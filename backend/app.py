# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, url_for, flash, redirect, session, jsonify, make_response
import json
import re
import requests
import base64
from sqlalchemy import *
from flask_cors import CORS
import re
import calendar;
import time;

from flask_jwt_extended import (
    JWTManager, jwt_required,jwt_optional, create_access_token,
    get_jwt_identity, decode_token
)



app = Flask(__name__)

# Setup the Flask-JWT-Extended extension
app.config['JWT_SECRET_KEY'] = 'Chave_123_Secreta'
app.secret_key = 'chave_secreta123'
jwt = JWTManager(app)

engine = create_engine('sqlite:////home/claralab/mysite/banco.db')
#engine = create_engine('sqlite:///./banco.db')
engine.echo = False
metadata = MetaData(engine)

CORS(app)



def getresult(sqlobj):
    d, a = {}, []
    for rowproxy in sqlobj:
        for column, value in rowproxy.items():
            d = {**d, **{column: value}}
        a.append(d)
    if(a == []):
        return None
    return a




#Tabelas que serão usadas pelo sqlalchemy
cursos = Table('cursos', metadata,
    Column('nome', String),
    Column('duracao', String),
    Column('descricao', String),
)

usuarios = Table('usuarios', metadata,
    Column('id', Integer),
    Column('usuario', String),
    Column('senha', String),
    Column('email', String),
)



@app.route('/api/v1/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Falta JSON na requisição"}), 400
    usuario = request.json.get('usuario', None)
    senha = request.json.get('senha', None)
    resultado = engine.execute('SELECT senha FROM usuarios WHERE usuario=\''+usuario+'\' and senha=\''+senha+'\';')
    resultado = getresult(resultado)
    print(resultado)
    if not usuario:
        return jsonify({"msg": "Falta parâmetro usuario"}), 400
    if not senha:
        return jsonify({"msg": "Falta parâmetro senha"}), 400
    if resultado is None:
        return jsonify({"msg": "Usuário ou senha incorretos"}), 401
    # Identity can be any data that is json serializable
    resultado = engine.execute('SELECT id FROM usuarios WHERE usuario=\''+usuario+'\';')
    a = getresult(resultado)
    session["userlogadoid"] = a[0]
    session["userlogadonome"] = usuario
    access_token = create_access_token(identity=usuario, fresh=True)
    return jsonify(access_token=access_token,id=a[0]['id']), 200


@app.route('/api/v1/asenha', methods=['POST','GET'])
@jwt_optional
def asenha():
    """type NovaSenha
{
  "novasenha1": string,
  "novasenha2": string,
}

    """
    current_user = get_jwt_identity()
    if current_user ==None:
      print("Usando o token pela URL")
      current_user = request.args.get('access_token', None)
      current_user = decode_token(current_user,allow_expired=False)
      print(current_user)
      current_user = current_user['identity']
    if current_user ==None:
      return jsonify({"msg": "Token não encontrado!"}), 400
    novasenha1 = request.json.get('novasenha1', None)
    novasenha2 = request.json.get('novasenha2', None)
    if novasenha1!=novasenha2:
      return jsonify({"msg": "Senhas divergem... Tente novamente"}), 401
    engine.execute('UPDATE usuarios SET senha='+novasenha1+' WHERE usuario=\''+current_user+'\';')
    return jsonify({"msg": "Senha alterada com sucesso!"}), 200

@app.route('/api/v1/esenha', methods=['POST'])
def esenha():
    """   type EsqueciSenha
    {
    "email": string
    }
    """
    email = request.json.get('email', None)
    resultado = engine.execute('SELECT email from usuarios WHERE email=\''+email+'\';')
    resultado = getresult(resultado)
    if resultado == None:
      return jsonify({"msg": "Este endereço de e-mail não existe!"}), 400
    else:
      return jsonify({"msg": "Verifique o e-mail "+str(resultado[0]['email'])+" para o link para a mudança de senha."}), 200


@app.route('/api/v1/adduser', methods=['POST'])
def adduser():
    """type AddUserTrilha {
  "idcurso": number
  "email": string
}
    """
    idcurso = request.json.get('idcurso', None)
    email = request.json.get('email', None)
    resultado = engine.execute('SELECT * from usuarios WHERE email=\''+email+'\';')
    resultado = getresult(resultado)
    if resultado == None:
      return jsonify({"msg": "Este endereço de e-mail não está cadastrado, porém será enviado um e-mail de ativação de cadastro."}), 200
    else:
      engine.execute('INSERT INTO desempenhousers (id_curso, id_user, pct_completado, numero_de_pontos) SELECT \''+str(idcurso)+'\', \''+str(resultado[0]['id'])+'\', \'0\', \'0\' WHERE NOT EXISTS ( SELECT 1 FROM desempenhousers WHERE id_curso = '+str(idcurso)+' AND id_user ='+str(resultado[0]['id'])+');')
      return jsonify({"msg": "Usuário adicionado com sucesso!"}), 200

@app.route('/api/v1/createuser', methods=['POST'])
def createuser():
    """type AddUserTrilha {
  "usuario": number
  "email": string,
  "senha": asda,
  "data_nasc": asdad,
  "nome" : asdasd
}
    """
    usuario = request.json.get('usuario', None)
    email = request.json.get('email', None)
    senha = request.json.get('senha', None)
    data_nasc = request.json.get('data_nasc', None)
    nome = request.json.get('nome', None)
    try:
      engine.execute('INSERT INTO usuarios (usuario, email, senha, data_nasc,nome)  VALUES (\''+usuario+'\',\''+email+'\',\''+senha+'\',\''+data_nasc+'\',\''+nome+'\')')
      return jsonify({"msg": "Cadastro realizado com sucesso!"}), 200
    except:
      return jsonify({"msg": "Usuário já existe. Login ou e-mail já cadastrados."}), 401



@app.route('/api/v1/getcurso', methods=['GET'])
def getcurso():
    id = request.args.get('id', None)
    if id is None:
        id=""
    else:
        id=' WHERE curso_id=\''+str(id)+'\';'
    resultado = engine.execute('SELECT * FROM cursos'+id)
    resultado = getresult(resultado)
    if id!="":
        resultado=resultado[0]
    return jsonify(resultado), 200


@app.route('/api/v1/getuser', methods=['GET'])
def getuser():
    '''
type ShowUser {
  "nome": string,
  "email": string,
  "data_nasc": string,
  "username": string
}
    '''
    id = request.args.get('id', None)
    if id is None:
        id=""
    else:
        id=' WHERE id=\''+str(id)+'\';'
    resultado = engine.execute('SELECT * FROM usuarios'+id)
    resultado = getresult(resultado)
    for x in resultado:
        x['username'] = x.pop('usuario')
        x.pop('senha')
    if id!="":
        resultado=resultado[0]
    return jsonify(resultado), 200


@app.route('/api/v1/gethome', methods=['GET'])
def gethome():
    '''
type RenderHome {
  "nickname_usuario": string,
  "id_user":number,
  "cargo": string,
  "numero_de_horas": number,
  "pontuacao_geral": number,
  "cursos": {
    "id_curso": number,
    "numero_de_cursos": number,
    "listacursos":[{
      "titulo_do_curso": string,
      "empresa": string,
      "numero_de_horas": number,
      "desempenho_usuario": {
        "pct_completado": number,
        "numero_de_pontos": number
      }
    },{
      "id_curso": number,
      "titulo_do_curso": string,
      "empresa": string,
      "numero_de_horas": number,
      "desempenho_usuario": {
        "pct_completado": number,
        "numero_de_pontos": number
      }
      ]
    }
  }
}
    '''
    id = request.args.get('id', None)
    id2=0
    if id is None:
        id=""
    else:
        id2=id
        id=' WHERE id=\''+str(id)+'\';'
    resultado = engine.execute('SELECT * FROM usuarios'+id)
    resultado = getresult(resultado)
    try:
        resultado[0].pop('senha')
    except:
        pass
    resultado2 = engine.execute('SELECT id_curso,pct_completado,numero_de_pontos FROM desempenhousers WHERE id_user=\''+id2+'\'')
    resultado2 = getresult(resultado2)
    try:
        for x in resultado2:
            dadoscurso=engine.execute('SELECT * FROM cursos WHERE curso_id=\''+str(x['id_curso'])+'\'')
            dadoscurso=getresult(dadoscurso)
            try:
                x.update(dadoscurso[0])
                x.pop('curso_id')
            except:
                pass
    except:
        pass
    return jsonify(dadosuser=resultado[0],cursos=resultado2), 200




@app.route('/api/v1/forumtopicos', methods=['GET','POST'])
def forumtopicos():
    '''
{"topicos":[{"titulo":"ola mundo","id":1,"userid":1,"criado_em":timestamp,"nposts":2,"lastpostid":15},
{"titulo":"ola mundo denovo","id":2,"userid":3,"criado_em":timestamp,"nposts":10, "lastpostid":50}]}
    '''
    curso_id = request.args.get('curso_id', None)
    if request.method == 'GET':
      dadosforum=engine.execute('SELECT * FROM forums WHERE curso_id=\''+str(curso_id)+'\'')
      dadosforum = getresult(dadosforum)
      dadostopicos=""
      dadosforum=json.loads(dadosforum[0]['topicos'])
      dadosforum2=[]
      for x in dadosforum:
        print("aaaa")
        print(x)
        dadostopicos=engine.execute('SELECT * FROM topicos WHERE id=\''+str(x)+'\'')
        dadostopicos=getresult(dadostopicos)
        print(dadostopicos)
        dadosforum2.append(dadostopicos[0])
      return jsonify(topicos=dadosforum2), 200
    if request.method == 'POST':
      titulo = request.json.get('titulo', None)
      userid = request.json.get('userid', None)
      criado_em = calendar.timegm(time.gmtime())
      result=engine.execute('SELECT topicos FROM forums WHERE id=\''+curso_id+'\'')
      result=getresult(result)
      result2 = eval(result[0]['topicos'])
      engine.execute('INSERT INTO topicos (titulo, user_id, criado_em,nposts,lastpostid)  VALUES (\''+titulo+'\',\''+userid+'\',\''+str(criado_em)+'\',\''+str(0)+'\',\''+str(0)+'\')')
      result=engine.execute('SELECT id FROM topicos WHERE titulo=\''+titulo+'\' AND user_id=\''+userid+'\' AND criado_em=\''+str(criado_em)+'\'')
      result=getresult(result)
      result = result[0]
      print("aqui")
      print(result2)
      print(result['id'])
      print(type(result2))
      result2.append(result['id'])
      print(result2)
      engine.execute('UPDATE forums SET topicos = \''+str(result2)+'\' WHERE id='+str(curso_id)+';')
      engine.execute('UPDATE topicos SET lastpostid = \''+str(result['id'])+'\' WHERE id='+str(result['id'])+';')
      return jsonify({"msg": "Tópico criado com sucesso."}), 200


@app.route('/api/v1/forumposts', methods=['GET','POST'])
def forumposts():
    '''
"mensagens":[{"userid":1,"postid":1,"mensagem":"Olá mundo, comovai?","criado_em":timestamp},{"userid":3,"postid":2,"mensagem":"Loremipsum","criado_em":timestamp}]
    '''
    if request.method == 'GET':
      topico_id = request.args.get('topico_id', None)
      dadostopico=engine.execute('SELECT * FROM posts WHERE topico_id=\''+str(topico_id)+'\'')
      dadostopico = getresult(dadostopico)
      return jsonify(mensagens=dadostopico), 200
    if request.method == 'POST':
      topico_id = request.args.get('topico_id', None)
      mensagem = request.json.get('mensagem', None)
      userid = request.json.get('userid', None)
      criado_em = calendar.timegm(time.gmtime())
      result = engine.execute('INSERT INTO posts (topico_id, userid, criado_em,mensagem)  VALUES (\''+topico_id+'\',\''+userid+'\',\''+str(criado_em)+'\',\''+mensagem+'\')')
      recid = result.lastrowid
      engine.execute('UPDATE topicos SET lastpostid = \''+str(recid)+'\' WHERE id='+str(topico_id)+';')
      return jsonify({"msg": "Post criado com sucesso."}), 200


@app.route('/api/v1/getcards', methods=['GET','DELETE'])
def getcards():
  deckid = request.args.get('id', None)
  if request.method == 'DELETE':
    engine.execute('DELETE FROM decks WHERE id='+deckid)
    resultado = engine.execute('SELECT * FROM tabuleiros')
    resultado = getresult(resultado)
    for x in resultado:
      y = json.loads(x['conteudo'])
      for z in y:
        if z['tipo']=='deck':
          if(z['id']==int(deckid)):
            y.remove(z)
            savey= str(y).replace("'",'"')
            engine.execute('UPDATE tabuleiros SET conteudo = \''+savey+'\' WHERE id='+str(x['id'])+';')
    return jsonify({"msg":"Conjunto de cards deletado com sucesso."}), 200
  resultado = engine.execute('SELECT * FROM decks WHERE id='+deckid)
  resultado = getresult(resultado)
  resultado[0]['cards']=resultado[0]['cards'].strip('][').split(',')
  resultado2=[]
  for x in resultado[0]['cards']:
    busca = engine.execute('SELECT id,tipo,titulo,corpo,numalt,pontos,filename FROM cards WHERE id='+x)
    busca = getresult(busca)
    busca[0]['corpo']=busca[0]['corpo'].strip('][').split(',')
    ite=0
    for y in busca[0]['corpo']:
      y=y.strip('\"')
      busca[0]['corpo'][ite]=y
      ite=ite+1
    if busca[0]['tipo']!='pobj':
      busca[0]['corpo']=busca[0]['corpo'][0]
    resultado2.append(busca[0])
  resultado[0].pop('cards')
  return jsonify(deck=resultado[0],cards=resultado2), 200
'''
type ShowCards {
  "numero_de_cards": number,
  "cards": [{
      "id" : number,
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string
      "favoritado":boolean
    },{"id" : number,
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string,
      "favoritado":boolean
    },{"id" : number,
      "tipo": "pobj",
      "titulo": string,
      "numalt": number,
      "corpo": {
        "alternativa1": string,
        "alternativa2": string,
        "alternativa3": string
      },
      "favoritado":boolean
    }
  ]
}

'''



@app.route('/api/v1/addcards', methods=['POST','GET'])
def addcards():
  cardids=[]
  tabuleiro = request.json.get('tabuleiro', None)
  cards = request.json.get('cards', None)
  deck = request.json.get('deck', None)
  atualizar = request.args.get('update', None)
  if atualizar!= None:
    resultados = engine.execute('SELECT cards FROM decks WHERE id='+atualizar+'')
    resposta=[]
    for row in resultados:
      resposta = row[0]
      break
    try:
      resposta = resposta.strip('][').split(', ')
    except:
      return jsonify({"msg": "Conjunto de cards inexistente!"}), 401
    for x in resposta:
      engine.execute('DELETE FROM cards WHERE id='+x+'')
    engine.execute('DELETE FROM decks WHERE id ='+atualizar+'')
  try:
    for x in cards:
      if(x['respcorretas'] == None):
        x['respcorretas']=""
      if(x['numalt'] == None):
        x['numalt']=0
      x['corpo']=str(x['corpo']).replace("'",'''"''')
      if(x['tipo']=="upload"):
        result = engine.execute('INSERT INTO cards (corpo,numalt,pontos,tipo,titulo,respcorretas,filename) VALUES (\''+str(x['corpo'])+'\','+str(x['numalt'])+','+str(x['pontos'])+',\''+x['tipo']+'\',\''+x['titulo']+'\',\''+str(x['respcorretas'])+'\',\''+x['filename']+'\')')
      else:
        result = engine.execute('INSERT INTO cards (corpo,numalt,pontos,tipo,titulo,respcorretas) VALUES (\''+str(x['corpo'])+'\','+str(x['numalt'])+','+str(x['pontos'])+',\''+x['tipo']+'\',\''+x['titulo']+'\',\''+str(x['respcorretas'])+'\')')
      recid = result.lastrowid
      cardids.append(recid)
    result = engine.execute('INSERT INTO decks (titulo,pontos,horas,cards,tabuleiro) VALUES (\''+deck['titulo']+'\','+str(deck['pontos'])+','+str(deck['horas'])+',\''+str(cardids)+'\',\''+str(tabuleiro)+'\')')
    recid = result.lastrowid
    if(atualizar==None):
      return jsonify({"msg": "Conjunto de cards adicionados com sucesso."}), 200
    else:
      engine.execute('UPDATE decks SET id = '+str(atualizar)+' WHERE id='+str(recid)+';')
      return jsonify({"msg": "Conjunto de cards atualizado com sucesso."}), 200
  except:
    return jsonify({"msg": "Erro ao criar ou atualizar o conjunto de cards."}), 401
'''
{
  "cards": [
    {
      "corpo": "Este é um card de informação criado para testes.",
      "id": 1,
      "numalt": null,
      "pontos": 0,
      "tipo": "informacao",
      "titulo": "teste 1"
    },
    {
      "corpo": [
        "alternativa 1",
        "alternativa 2",
        "alternativa 3",
        "alternativa 4"
      ],
      "id": 2,
      "numalt": 4,
      "pontos": 5,
      "tipo": "pobj",
      "titulo": "teste 2"
    },
    {
      "corpo": "Insira uma resposta subjetiva para este card!",
      "id": 3,
      "numalt": null,
      "pontos": 1,
      "tipo": "psubj",
      "titulo": "teste 3"
    }
  ],
  "deck": {
    "horas": 0,
    "id": 1,
    "pontos": 6,
    "titulo": "Deck de teste"
  }
}

'''


@app.route('/api/v1/editinfo', methods=['POST'])
def editinfo():
    """type EditarTrilha {
  "titulo": string,
  "horas": number,
  "descricao": string
}
    """
    return 0






@app.route('/api/v1/edituser', methods=['POST'])
def edituser():
    """type EditUser {
  "nome": string,
  "email": string,
  "data_nasc": string,
  "username": string
}
    """
    return 0


@app.route('/api/v1/addcardgeneric', methods=['POST'])
def addcardgeneric():
    """type AddCardTipoInformacaoESubjetiva {
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string
}

    """
    return 0


@app.route('/api/v1/addcardobj', methods=['POST'])
def addcardobj():
    """type AddCardTipoObjetiva {
      "tipo": "pobj",
      "titulo": string,
      "numalt": number,
      "corpo": [string,string,string,...]
      "respcorretas": [number,number,...]
}

    """
    return 0

@app.route('/api/v1/cardanswers', methods=['POST'])
def cardanswers():
    """type MandarRespostas {
  "numero_de_cards": number,
  "cards": [{"id" : number,
      "tipo": "psubj",
      "resposta": string
    },{"id" : number,
      "tipo": "pobj",
      "resposta": [number,number,...],
    },{"id" : number,
      "tipo": "upload",
      "file": string
    }
  ]
}
    """
    return 0


@app.route('/api/v1/addstage', methods=['POST'])
def addstage():
    """type AddStage {
  "titulo": string,
  "horas": number,
  "pontos": number,
  "numero_de_cards": number
}

    """
    return 0


@app.route('/api/v1/upload', methods=['GET','POST'])
def upload():
    try:
      image = request.files['file']
    except:
      image = None
    tipo = request.args.get('type', None)
    if tipo=="card":
      filename = request.json.get('filename', None)
      id = request.json.get('id', None)
      path=filename
      print("valor de verdade: "+str(image!=None))
      if image!= None:
        headers = {'Authorization': 'Client-ID 5ade086f212d00f'}
        encoded_string = base64.b64encode(image.read())
        payload = {'image': encoded_string}
        print(encoded_string)
        r = requests.post('https://api.imgur.com/3/image', headers=headers, data=payload)
        print(r.status_code)
        jso = r.json()
        path = jso['data']['link']
      result = engine.execute('INSERT into uploads (tipo,filename,ref_id) VALUES (\''+tipo+'\',\''+path+'\',\''+str(id)+'\')')
    elif tipo=="material":
      filename = request.json.get('filename', None)
      id = request.json.get('id', None)
      result = engine.execute('INSERT into uploads (tipo,filename,ref_id) VALUES (\''+tipo+'\',\''+filename+'\',\''+str(id)+'\')')
    elif tipo=="user":
      filename = request.json.get('filename', None)
      id = request.json.get('id', None)
      result = engine.execute('INSERT into uploads (tipo,filename,ref_id) VALUES (\''+tipo+'\',\''+filename+'\',\''+str(id)+'\')')
    elif tipo=="course":
      filename = request.json.get('filename', None)
      id = request.json.get('id', None)
      result = engine.execute('INSERT into uploads (tipo,filename,ref_id) VALUES (\''+tipo+'\',\''+filename+'\',\''+str(id)+'\')')
    else:
      filename = request.json.get('filename', None)
      id = request.json.get('id', None)
      tipo = request.json.get('tipo', None)
      result = engine.execute('INSERT into uploads (tipo,filename,ref_id) VALUES (\''+tipo+'\',\''+filename+'\',\''+str(id)+'\')')
    return jsonify({"msg": "Upload realizado com sucesso."}), 200




@app.route('/api/v1/respostas', methods=['POST'])
def respostas():
    if not request.is_json:
      return jsonify({"msg": "Falta JSON na requisição"}), 400
    respostas = request.json.get('respostas', None)
    for x in respostas:
      print(x['card_id'])
      print(x['user_id'])
      print(x['resposta'])
    return jsonify({"msg": "Respostas recebidas."}), 200


@app.route('/api/v1/getprofile', methods=['GET'])
def getprofile():
    '''
type ShowProfile {
  "nickname_usuario": string,
  "id_user":number,
  "cargo": string,
  "numero_de_horas": number,
  "pontuacao_geral": number
}

    '''

    return 0


@app.route('/api/v1/cadcliente', methods=['GET','POST'])
def cadcliente():
  '''{
    "nome":"Clara Idea",
  "login":"claraidea",
  "senha":"teste123",
  "fisicaoujuridica":"juridica",
  "cpfoucnpj": 4654565465,
  "cidade": "Salvador",
  "estado": "Bahia",
  "email" : "claraidea@gmail.com",
  "data_cadastro":"29/11/2020",
  "plano":"Premium",
  "validade":"01/01/2022",
  "acessos":50
  }'''
  nome = request.json.get('nome', None)
  login = request.json.get('login', None)
  senha = request.json.get('senha', None)
  fisicaoujuridica = request.json.get('fisicaoujuridica', None)
  cpfoucnpj = request.json.get('cpfoucnpj', None)
  cidade = request.json.get('cidade', None)
  estado = request.json.get('estado', None)
  email = request.json.get('email', None)
  data_cadastro = request.json.get('data_cadastro', None)
  plano = request.json.get('plano', None)
  validade = request.json.get('validade', None)
  acessos = request.json.get('acessos', None)
  result = engine.execute('INSERT into clientes (nome,login,senha,fisicaoujuridica,cpfoucnpj,cidade,estado,email,data_cadastro,plano,validade,acessos) VALUES (\''+nome+'\',\''+login+'\',\''+senha+'\',\''+fisicaoujuridica+'\',\''+str(cpfoucnpj)+'\',\''+cidade+'\',\''+estado+'\',\''+email+'\',\''+data_cadastro+'\',\''+plano+'\',\''+validade+'\',\''+str(acessos)+'\')')
  return jsonify("Cadastro realizado com sucesso"), 200


@app.route('/api/v1/pagamento', methods=['GET','POST'])
def pagamento():
  '''{
{"ncartao": 1111222233334444,"cvv": 123,"ccexp":10/22,"nomecartao":"FULANO DE TAL"}
  }'''
  tipo = request.args.get('tipo', None)
  if tipo=="cc":
    ncartao = request.json.get('ncartao', None)
    cvv = request.json.get('cvv', None)
    ccexp = request.json.get('ccexp', None)
    nomecartao = request.json.get('nomecartao', None)
  return jsonify("Pagamento realizado com sucesso"), 200



@app.route('/api/v1/tabuleiro', methods=['GET','POST'])
def tabuleiro():
    id = request.args.get('id', None)
    user_id = request.args.get('user_id', None)
    concat = ""
    cargo = ""
    statustabuleiro=""
    resultado = engine.execute('SELECT * FROM tabuleiros WHERE id='+id)
    resultado = getresult(resultado)
    print(resultado)
    for element in resultado:
      element['conteudo'] = element['conteudo'].replace('\\"', "")
      print(element['conteudo'])
    resultado = str(resultado).replace("conteudo': '","conteudo':")
    resultado = str(resultado).replace("]'","]")
    resultado = str(resultado).replace("'",'"')
    resultado = json.loads(resultado)
    print(resultado)
    resultado = resultado[0]
    if user_id!=None:
      cargo = engine.execute('SELECT cargo FROM usuarios WHERE id='+user_id)
      cargo = getresult(cargo)
      cargo = cargo[0]
      rst = engine.execute('SELECT statustabuleiro FROM desempenhousers WHERE id_user='+user_id)
      rst = getresult(rst)
      try:
        statustabuleiro=rst[0]
      except:
        statustabuleiro = {'statustabuleiro': "[{'id':"+str(id)+",'concluido':[]}]"}
      print("O STATUS DO TABULEIRO É: "+str(statustabuleiro['statustabuleiro']))
      stab = json.loads(statustabuleiro['statustabuleiro'].replace("'",'"'))
      for x in stab:
        id = int(id)
        if x['id']==id:
          ite=0
          for y in resultado['conteudo']:
            print(y)
            try:
              print(x['concluido'][ite])
              y.update({"concluido":x['concluido'][ite]})
            except:
              print(false)
              y.update({"concluido":False})
            ite=ite+1
    return jsonify(resultado), 200



@app.route('/api/v1/addtabuleiro', methods=["POST","GET"])
def addtabuleiro():
    tid = request.args.get('id', None)
    curso = request.json.get('curso', None)
    equipe = request.json.get('equipe', None)
    etapas = request.json.get('etapas', None)
    conteudo = request.json.get('conteudo', None)
    conteudo=str(conteudo).replace("'",'"')
    if(tid==None):
      engine.execute('INSERT into tabuleiros (curso,equipe,etapas,conteudo) VALUES (\''+str(curso)+'\',\''+str(equipe)+'\',\''+str(etapas)+'\',\''+str(conteudo)+'\')')
    else:
      engine.execute('UPDATE tabuleiros SET curso='+str(curso)+',equipe='+str(equipe)+',etapas='+str(etapas)+',conteudo=\''+str(conteudo)+'\' WHERE id=\''+str(tid)+'\';')
    return jsonify({"msg": "ok"}), 200



@app.route('/api/v1/getmembers', methods=['GET'])
def getmembers():
    '''
type ShowMembers {
  "curso_id": number,
  "participantes": {
    "numero_participantes": number,
    "listaparticipantes": [{
      "nickname": string,
      "pontuacao": number
    },{
      "nickname": string,
      "pontuacao": number
    }]
  }
}

    '''
    nome = request.args.get('id', None)
    return 0



@app.route('/api/v1/updateuser', methods=['GET','POST'])
def updateuser():
    id = request.args.get('id', None)
    usuario = request.json.get('usuario', None)
    email = request.json.get('email', None)
    data_nasc = request.json.get('data_nasc', None)
    nome = request.json.get('nome', None)
    avatar_url = request.json.get('avatar_url', None)
    engine.execute('UPDATE usuarios SET usuario=\''+str(usuario)+'\',email=\''+str(email)+'\',data_nasc=\''+str(data_nasc)+'\',nome=\''+str(nome)+'\',avatar_url=\''+str(avatar_url)+'\' WHERE id=\''+id+'\';')
    return jsonify({"msg": "Perfil atualizado."}), 200


@app.route('/api/v1/favoritarcard', methods=['GET'])
def favoritarcard():
    card_id = request.args.get('card_id', None)
    user_id = request.args.get('user_id', None)
    rst = engine.execute('SELECT cardsfavoritados FROM usuarios WHERE id=\''+user_id+'\';')
    rst = getresult(rst)
    rst = rst[0]['cardsfavoritados']
    rst = json.loads(rst)
    print(rst)
    if(int(card_id) not in rst):
      rst.append(int(card_id))
    engine.execute('UPDATE usuarios SET cardsfavoritados=\''+str(rst)+'\' WHERE id=\''+user_id+'\';')
    return jsonify({"msg": "Card favoritado com sucesso"}), 200

@app.route('/api/v1/criatrilha', methods=['POST','GET'])
def criatrilha():
    uid = request.args.get('id', None)
    if not request.is_json:
      return jsonify({"msg": "Falta JSON na requisição"}), 400
    nome = request.json.get('nome', None)
    horas = request.json.get('horas', None)
    descricao = request.json.get('descricao', None)
    pontos = request.json.get('pontos', None)
    empresa = request.json.get('empresa', None)
    professores = []
    professores.append(int(uid))
    engine.execute('UPDATE usuarios SET cargo=\'professor\' WHERE id='+str(uid))
    result = engine.execute('INSERT into cursos (nome,horas,descricao,pontos,empresa,professores) VALUES (\''+nome+'\',\''+horas+'\',\''+descricao+'\',\''+pontos+'\',\''+empresa+'\',\''+str(professores)+'\')')
    recid = result.lastrowid
    engine.execute('INSERT INTO desempenhousers (id_curso, id_user, pct_completado, numero_de_pontos) SELECT \''+str(recid)+'\', \''+str(uid)+'\', \'0\', \'0\' WHERE NOT EXISTS ( SELECT 1 FROM desempenhousers WHERE id_curso = '+str(recid)+' AND id_user ='+str(uid)+');')

    """nome, horas,descrição,pontos, empresa
    """
    return jsonify({"msg": "ok"}), 200


if __name__ == '__main__':
    app.run()
