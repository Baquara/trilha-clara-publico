const colors = {
  primary: "#3A1244",
  purple: "#60336B",
  pink: "#EE4B70",
  aqua: "#3CC0CC",
  blue: "#204C77",
  darkgray: "#393939",
  gray: "#C4C4C4",
  gray2: "#696969",
  gray3: "#76786A",
  gray4: "#e8e8e8",
  lightgray: "#E3E3E3",
  gold: "#D2A01D",
  lightyellow: "#F8FFCC",
  dark: "#1A1A1A",
  green: "#C1D731",
};

export default colors;
