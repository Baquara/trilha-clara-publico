import React, { useState } from "react";
import { Link } from "react-router-dom";
import { MdSettings, MdKeyboardBackspace, MdMenu } from "react-icons/md";
import { NavDiv } from "./styles";
import { useLocation } from "react-router-dom";
import logo from "../../assets/img/min-logo.png";

function NavBar(props) {
  let location = useLocation();
  let currentLocation = location.pathname;
  const [shouldHide, setHide] = useState(true);
  return (
    <NavDiv state={props.logged}>
      <div id="TopBar">
        {" "}
        {/*Mobile Only*/}
        <Link to={props.goBack}>
          <MdKeyboardBackspace id="MdBackspaceId" />
        </Link>
        {props.logged ? (
          <>
            {/* <Link to="/settings">
              <MdSettings id="MdSettingsId" />
            </Link> */}
            <img src={logo} />
            <MdMenu
              id="MdMenuId"
              onClick={() => setHide(shouldHide ? false : true)}
            />
          </>
        ) : (
          <>
            <img src={logo} />
          </>
        )}
      </div>

      <div className={shouldHide ? "HiddenMobile" : "List"}>
        <Link to={props.goBack}>
          <MdKeyboardBackspace id="MdBackspaceId" className="HiddenDesktop" />
        </Link>
        {props.logged ? (
          <>
            {props.admin ? (
              <Link
                className={
                  currentLocation === "/dashboard"
                    ? "CurrentRoute NavItem"
                    : "NavItem"
                }
                to={currentLocation === "/dashboard" ? "" : "/dashboard"}
              >
                Dashboard
              </Link>
            ) : (
              <></>
            )}
            <Link
              className={
                currentLocation === "/home" ? "CurrentRoute NavItem" : "NavItem"
              }
              to={currentLocation === "/home" ? "" : "/home"}
            >
              Home
            </Link>
            <Link
              className={
                currentLocation === "/members"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={currentLocation === "/members" ? "" : "/members"}
            >
              Participantes
            </Link>
            <Link
              className={
                currentLocation === "/materials"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={currentLocation === "/materials" ? "" : "/materials"}
            >
              Materiais
            </Link>
            <Link
              className={
                currentLocation === "/forum"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={currentLocation === "/forum" ? "" : "/forum"}
            >
              Fórum
            </Link>
            {/* <Link
              className={
                currentLocation === "/myprofile"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={currentLocation === "/myprofile" ? "" : "/myprofile"}
            >
              Perfil
            </Link> */}
            <Link
              className={
                currentLocation === "/favoritesCards"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={
                currentLocation === "/favoritesCards" ? "" : "/favoritesCards"
              }
            >
              Cards Favoritos
            </Link>
            {/* <Link
              className={
                currentLocation === "/stages"
                  ? "CurrentRoute NavItem"
                  : "NavItem"
              }
              to={currentLocation === "/stages" ? "" : "/stages"}
            >
              Fases
            </Link> */}

            <Link to="/settings">
              <MdSettings id="MdSettingsId" />
            </Link>

            <img src={logo} className="HiddenDesktop" />
          </>
        ) : (
          <>
            <img src={logo} className="HiddenDesktop" />
          </>
        )}
      </div>
    </NavDiv>
  );
}

export default NavBar;
