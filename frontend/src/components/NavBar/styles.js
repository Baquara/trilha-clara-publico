import styled from "styled-components";
import colors from "../../utils/colors";

export const NavDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: #3b1d40;
  width: 100%;
  font-size: 20px;
  padding-top: 5px;
  padding-bottom: 5px;
  margin: 0 auto;

  img {
    width: 40px;
    margin-left: 10px;
    filter: brightness(0) invert(1);
  }

  ${(props) =>
    props.state
      ? `

        `
      : `
        position: absolute;
        top: 0;
    `}

  #TopBar {
    display: flex;
    justify-content: space-between;
    font-size: 30px;
    padding: 10px;
    padding-bottom: 0px;
  }

  #MdBackspaceId,
  #MdMenuId {
    color: ${colors.green};
  }
  #MdSettingsId {
    color: white;
  }

  #MdMenuId:hover {
    cursor: pointer;
  }

  .List {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .NavItem {
    color: white;
    padding-bottom: 7.5px;
    padding-top: 7.5px;
    text-align: center;
    text-decoration: none;
    width: 100%;
  }
  .CurrentRoute {
    color: ${colors.primary};
    background-color: ${colors.green};
    font-weight: bold;
  }

  .HiddenMobile,
  .HiddenDesktop {
    @media (max-width: 1000px) {
      display: none;
    }
  }

  @media (min-width: 1000px) {
    .NavItem:hover {
      color: ${colors.primary};
      background-color: ${colors.aqua};
      font-weight: bold;
    }
    .CurrentRoute:hover {
      color: ${colors.primary};
      background-color: ${colors.green};
      font-weight: bold;
    }
    #TopBar {
      display: none;
    }
    #MdBackspaceId,
    #MdSettingsId {
      font-size: 30px;
    }
    #MdBackspaceId {
      margin-right: 10px;
    }
    #MdSettingsId {
      margin-left: 10px;
    }
    .HiddenMobile,
    .List {
      align-self: center;
      display: flex;
      flex-direction: row;
      width: 90vw;
      max-width: 1200px;
      justify-content: space-between;
      align-items: center;
      background-color: #3b1d40;
      font-size: 20px;
      margin: 10px;
    }

    .NavItem {
      margin: 0px;
      width: 100%;
      border-radius: 5px;
    }
  }
`;
