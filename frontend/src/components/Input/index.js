import React from 'react';
import {SimpleInput} from './styles';

function Input(props, ...rest) {
    return  <SimpleInput onChange={props.onChange} value={props.value} placeholder={props.placeholder} type={props.type} txtcolor={props.txtcolor} color={props.color} bdstyle={props.bdstyle} {...rest}/>;
    //Color e a cor do background e txtcolor e a cor do texto
}

export default Input;
