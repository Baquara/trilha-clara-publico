import styled from "styled-components";

export const SimpleInput = styled.input`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 250px;
  height: 35px;
  color: ${(props) => props.txtcolor};
  background: ${(props) => props.color};
  border-radius: 8px;
  border-style: ${(props) => props.bdstyle};
  border-color: #c1d731;
  margin-bottom: 20px;
`;
