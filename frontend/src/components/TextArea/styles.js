import styled from 'styled-components';

export const SimpleTextArea = styled.textarea`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 500px;
    height: 132px;
    color: ${props => props.color};
    background-color: ${props => props.backgroundColor};
    border-radius: 8px;
    border:none;
    padding-top: 19px;
`; 
