import React from 'react';
import { SimpleTextArea } from "./styles";


export default function TextArea(props) {
    return (
        <div>
            <SimpleTextArea color={props.color} placeholder={props.placeholder} 
                backgroundColor={props.backgroundColor}
            />
            
        </div>
    )
}
