import styled from "styled-components";
import colors from "../../../utils/colors";

export const Container = styled.div`
  float: right;
  position: relative;
  margin-top: -50%;
  background: ${colors.green};
  padding: 10px;
  padding: 3px;
  width: 40px;
  border-radius: 10px 0 0 10px;

  button {
    background: none;
    border: none;
    cursor: pointer;
    padding: 0;
    margin: 0;
  }

  .pencil {
    color: ${(props) => props.color};
    font-size: 20px;
    position: relative;
    top: -10;
  }
`;
