import React from "react";
import { BsPencil } from "react-icons/bs";
import { Container } from "./styles";

export default function Favorites(props) {
  return (
    <Container>
      <button>
        <BsPencil color={props.color} className="pencil" />
      </button>
    </Container>
  );
}
