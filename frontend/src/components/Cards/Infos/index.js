import React from 'react'
import { InfosContainer } from './styles'
import {AiFillHeart} from 'react-icons/ai'

export default function CardInfo(props){
  return(
    <InfosContainer>
      <div className="top">
        <img src="https://portal.conlicitacao.com.br/wp-content/uploads//2019/08/rotina-administrativa-eficiente-800x534.jpg" alt=""/>
        {/* <div className="favorite">
          <button>
            <AiFillHeart className='heart'/>
          </button>
        </div> */}
        {props.children}
      </div>

      <div className="content">
        <h3>{props.title}</h3>
        <hr/>
          
        <p>
          {props.content}
        </p>
        
      </div>
    </InfosContainer>
  )
}