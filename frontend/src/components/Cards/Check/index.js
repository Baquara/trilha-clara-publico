import React from 'react'
import { CheckContainer } from './styles'
// import {AiFillHeart} from 'react-icons/ai'

export default function CardCheck(props){
  return(
    <CheckContainer>
      <div className="top">
        <img src="https://www.casadaqualidade.com.br/wp-content/uploads/2018/11/fachada-1.png" alt=""/>
        {/* <div className="favorite">
          <button>
            <AiFillHeart className='heart'/>
          </button>
        </div> */}
        {props.children}
      </div>

      <div className="content">
        <h3>{props.title}</h3>
        <hr/>
          
        <p>{props.content}</p>
        <div id='check'>
          {props.options.map( (option, index) =>{         
            return (<><input type="checkbox" onChange={e => e.target.checked ? props.onChange.push(index) : props.onChange.push(null)} name="check" id="check"/> <span>{option}</span> <br/></>)
          })}
        </div>
      </div>
    </CheckContainer>
  )
}