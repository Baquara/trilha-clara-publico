import React from 'react'
import {AiFillHeart} from 'react-icons/ai'
import { Container } from './styles'

export default function Favorites(props){
  return(
    <Container>
      <button>
        <AiFillHeart color={props.color} className='heart'/>
      </button>
    </Container>
  )
}


