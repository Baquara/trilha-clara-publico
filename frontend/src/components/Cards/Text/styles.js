import styled from 'styled-components'
import colors from '../../../utils/colors'

export const TextContainer = styled.div`
  margin-top: 20px;
  display: block;
  margin: 0 auto;

  /* height: 350px; */
  background: ${colors.lightgray};
  width: 250px;
  margin-bottom: 20px;
  border-radius: 10px;

  .top{
    height: 40%;
  }

  .top img{
    width: 100%;
    max-height: 100%;
    border-radius: 10px 10px 0 0;
  }

  .content{
    padding: 0 10px;
    text-align: left;
    font-size: 15px
  }

  .content h3{
    color: ${colors.primary};
  }

  .content hr{
    margin-top: -10px;
    height: 2px;
    border: none;
    background: ${colors.primary};
    border-radius: 10px;
  }

  textarea{
    margin-bottom: 10px;
    width: 98%;
    padding: 20px 0;
  }

  p, span{
    color: ${colors.darkgray}
  }

`
