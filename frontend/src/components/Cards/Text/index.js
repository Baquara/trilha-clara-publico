import React from "react";
import { TextContainer } from "./styles";

export default function CardText(props) {
  return (
    <TextContainer>
      <div className="top">
        <img
          src="https://www.aceguarulhos.com.br/blog/wp-content/uploads/2017/12/rotinas-administrativas.jpg"
          alt=""
        />
        {/* <div className="favorite">
          <button>
            <AiFillHeart className='heart'/>
          </button>
        </div> */}
        {props.children}
      </div>

      <div className="content">
        <h3>{props.title}</h3>
        <hr />

        <p>{props.content}</p>
        <form onSubmit={props.onSubmit}>
          <textarea
            onChange={props.onChange}
            name="response"
            id=""
            cols="30"
            rows="4"
          ></textarea>
        </form>
      </div>
    </TextContainer>
  );
}
