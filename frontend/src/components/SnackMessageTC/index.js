import React from "react";
import { Snackbar, IconButton, Typography } from "@material-ui/core";
import { ContentContainer, Message } from "./styles";
import CloseIcon from "@material-ui/icons/Close";

function SnackMessageTC({ message, handleClose, type }) {
  // type: "success" ou "error" ou "info"
  return (
    <ContentContainer type={type}>
      <Message variant="p">{message}</Message>
      <IconButton
        className="closeButton"
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </ContentContainer>
  );
}

export default SnackMessageTC;
