import styled from "styled-components";
import { Paper, Typography } from "@material-ui/core";
import colors from "../../utils/colors";

const statusType = {
  success: colors.green,
  error: colors.pink,
  info: colors.purple,
};

export const ContentContainer = styled(Paper)`
  width: 200px;
  height: 60px;
  background-color: ${(p) => statusType[p.type] || colors.primary} !important;
  padding-left: 18px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  .closeButton {
    position: absolute;
    top: 5px;
    right: 5px;
  }

  @media (max-width: 700px) {
    width: 100%;
  }
`;

export const Message = styled(Typography)`
  font-family: "Roboto";
  font-size: 18px;
`;
