import { Typography } from "@material-ui/core";
import styled from "styled-components";

export const ErrorText = styled(Typography)`
  font-size: 0.8rem !important;
  color: red;
`;
