import React from 'react';
import {SimpleInput} from './styles';

function Input(props) {
    return  <SimpleInput placeholder={props.placeholder} type={props.type} txtcolor={props.txtcolor} color={props.color} bdstyle={props.bdstyle}/>;
    //Color e a cor do background e txtcolor e a cor do texto
}

export default Input;
