import styled from 'styled-components';

export const SimpleInput = styled.input`
    position:relative;
    justify-content: center;
    align-items: center;
    padding-left: 10px;
    width: 400px;
    height: 30px;
    color: ${props => props.txtcolor};
    background: ${props => props.color}; 
    border-radius: 6px;
    border-style: ${props => props.bdstyle};
    border-color: #fff;
    
`; 

export const SectionMobile = styled.input`
    position:relative;
    justify-content: center;
    align-items: center;
    padding-left: 10px;
    width: 400px;
    height: 30px;
    color: ${props => props.txtcolor};
    background: ${props => props.color}; 
    border-radius: 6px;
    border-style: ${props => props.bdstyle};
    border-color: #fff;
    
`; 

