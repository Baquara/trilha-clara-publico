import React from "react";
import styles from "./styles";
import PropTypes from "prop-types";
import { TextField, withStyles } from "@material-ui/core";

function InputTC(props) {
  const { classes } = props;
  return (
    <TextField
      variant="filled"
      color="secondary"
      placeholder={props.placeholder}
      rows={props.rows}
      label={props.label}
      multiline={props.multiline}
      className={classes.root}
      {...props}
      InputProps={{
        className: classes.input,
      }}
    />
  );
}

InputTC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InputTC);
