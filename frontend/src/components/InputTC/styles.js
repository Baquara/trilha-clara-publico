import colors from '../../utils/colors';

const styles = {
    root:{
        backgroundColor: '#FFFFFF',
        padding: '0px',
        width: '80%',
        minWidth: '250px', // ../Button/styles.js, InputTC's minWidth = Button's width 
        maxWidth: '500px',
        marginTop: '10px',
    },
    input:{
        fontFamily: 'Roboto',
        color: 'black',
    },
};

export default styles;