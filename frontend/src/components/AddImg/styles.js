import styled from "styled-components";

export const AddImage = styled.div`
  position: relative;
  padding: 120px;
  margin-top: 100px;

  img {
    cursor: pointer;
    border-radius: 50%;
    position: absolute;
  }
  #back {
    margin-top: -100px;
    margin-left: -100px;
  }
  #front {
    margin: 6px;
    margin-top: -94px;
    margin-left: -94px;
    height: 189px;
    width: 189px;
    object-fit: cover;
  }
  #small {
    margin-left: 33px;
    margin-top: 53px;
  }
  #icon {
    margin-left: 40px;
    margin-top: 60px;
  }
  #fileInput {
    display: none;
  }
`;
