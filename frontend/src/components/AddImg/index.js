import React, { useState } from 'react';
import LCircle from '../../assets/img/LCircle.svg';
import GrayImg from '../../assets/img/GrayImg.svg';
import SCircle from '../../assets/img/SCircle.svg';
import pencil from '../../assets/img/eva_edit-fill.svg';
import { AddImage } from './styles'


export default function AddImg() {
    const [imgData, setImgData] = useState(null);
    const onChangePicture = e => {
      if (e.target.files[0]) {
        const reader = new FileReader();
        reader.addEventListener("load", () => {
          setImgData(reader.result);
        });
        reader.readAsDataURL(e.target.files[0]);
      }
    };
    return(
        <AddImage>
            <label htmlFor="fileInput"> 
                <img src={LCircle} id="back" alt=""/>
                <img src={imgData === null ? GrayImg : imgData}  id="front" alt=""/>
                <img src={SCircle} id="small"  alt=""/>
                <img src={pencil} id="icon" alt=""/>
            </label>

            <input id="fileInput" type="file"  accept="image/*" name="image" onChange={onChangePicture}/>
        </AddImage>
    )
}