import styled from "styled-components";

import leftArrow from "../../assets/icons/arrow-left.svg";
import rightArrow from "../../assets/icons/arrow-right.svg";
import colors from "../../utils/colors";

const arrowIcons = {
  left: leftArrow,
  right: rightArrow,
};

export const Container = styled.div`
  width: 100%;
  overflow: hidden;
  position: relative;
  /* display: flex; */
  background: ${colors.primary};
  /* padding: 60px 4px; */

  ::before {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    background: blue;
    min-width: 36px;
    background: linear-gradient(to right, ${colors.primary}, #3a124400);
    z-index: 9;
  }

  ::after {
    content: "";
    position: absolute;
    right: 0;
    top: 0;
    height: 100%;
    background: blue;
    min-width: 36px;
    background: linear-gradient(to left, ${colors.primary}, #3a124400);
    z-index: 9;
  }
`;

export const ContentContainer = styled.div`
  /* position: absolute; */
  display: flex;
  transform: translateX(${(p) => -1 * p.translateX}px);
  transition: transform 0.3s ease-in-out;
  /* align-items: center; */
  /* justify-content: flex-start; */
`;
export const CardWrapper = styled.div`
  padding: 0 4px;
`;
export const SlideButton = styled.button`
  position: absolute;
  ${(p) => `${p.side}: 4px;`}
  background: url(${(p) => arrowIcons[p.side]}) #fafafa;
  background-size: 60%;
  background-repeat: no-repeat;
  background-position: center;
  top: 50%;
  box-shadow: 1px 2px 4px rgba(0, 0, 0, 0.3);
  border: none;
  border-radius: 50%;
  transform: translateY(-50%);
  height: 48px;
  width: 48px;
  z-index: 10;
  :focus {
    outline: 0;
    border: none;
  }
`;
