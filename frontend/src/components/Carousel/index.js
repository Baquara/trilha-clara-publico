import React, { useRef, useEffect, useState } from "react"
import { Container, ContentContainer, CardWrapper, SlideButton } from "./styles"

function Carousel({ children, numberOfCards }) {
  const ref = useRef(null)
  const step = 308
  const [px, setPx] = useState(-10)
  useEffect(() => {
    if (Array.isArray(children)) {
      // children.length
    }
  }, [ref, children])

  const cardWidth = 308
  const [maxPercent, setMaxPercent] = useState(120)
  useEffect(() => {
    if (ref) {
      const off =
        ref.current.offsetWidth < 308
          ? 1
          : parseInt(ref.current.offsetWidth / 308)
      setMaxPercent(
        // (((cardWidth - 100) * children.length) / ref.current.offsetWidth) * 100
        cardWidth * (children.length - off) + 20
      )
    }
  }, [ref])
  const minPercent = -20

  function next() {
    if (Array.isArray(children)) {
      // children.length
      console.log("oi")
      // const current = parseInt((percent / 100) * children.length)
      setPx(px + step > maxPercent ? maxPercent : px + step)
    }
  }

  function prev() {
    if (Array.isArray(children)) {
      setPx(px - step < minPercent ? minPercent : px - step)
    }
  }

  console.log(maxPercent)

  return (
    <Container>
      {px > step / 2 && <SlideButton side="left" onClick={prev} />}
      <ContentContainer ref={ref} translateX={px}>
        {children.map(child => (
          <CardWrapper>{child}</CardWrapper>
        ))}
      </ContentContainer>
      {px < maxPercent - step / 4 && (
        <SlideButton side="right" onClick={next} />
      )}
    </Container>
  )
}

export default Carousel
