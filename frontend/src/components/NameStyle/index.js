import React from "react";
import { NameStyled, HeaderCard } from "./styles";

export default function NameStyle(props) {
  return (
    <HeaderCard>
      <NameStyled>
        <h2>{props.name}</h2>
      </NameStyled>
    </HeaderCard>
  );
}
