import styled from "styled-components";
import colors from "../../utils/colors";

export const HeaderCard = styled.div`
  @media (max-width: 700px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 5px;
    /* height: 250px; */
    /* z-index: -100; */
    background: ${colors.green};
    border-radius: 0px 0px 10px 10px;
  }
`;

export const NameStyled = styled.div`
  background-color: ${colors.green};
  color: ${colors.primary};
  margin-bottom: 20px;
  padding: 3px;
  text-align: center;
  border-radius: 5px;

  @media (max-width: 700px) {
    background: none;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    color: ${colors.primary};
    font-size: 20px;
    margin-bottom: 0px;
  }
`;
