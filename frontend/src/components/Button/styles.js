import styled from "styled-components";

export const SimpleButton = styled.button`
  width: 250px;
  height: 40px;
  border-radius: 8px;
  border: none;
  background-color: ${(props) => props.color};
  color: ${(props) => props.txtcolor};
  cursor: pointer;
  transition: all 1s;
`;
