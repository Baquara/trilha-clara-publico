import React from "react";
import { SimpleButton } from "./styles";

export default function Button(props) {
  return (
    <div>
      <SimpleButton
        disabled={props.cond}
        color={props.color}
        txtcolor={props.txtcolor}
        {...props}
      >
        {props.name}
      </SimpleButton>
    </div>
    //Color e a cor do background e txtcolor e a cor do texto
  );
}

// export default Button;
