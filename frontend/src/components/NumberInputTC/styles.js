import styled from "styled-components";
import NumberFormat from "react-number-format";
import { FormControl } from "@material-ui/core";

export const NFInput = styled(NumberFormat)`
  width: 100%;
  box-sizing: border-box;
  padding: 27px 12px 10px;
  border: 0;
  background: none;
  outline: none;
  /* ::after {
    content: "";
    position: absolute;
    width: 100%;
    height: 2;
    background: #f50057;
  } */
`;

export const Container = styled(FormControl)`
  width: 80%;
  background: #fff;
  margin-top: 10px !important;
`;
