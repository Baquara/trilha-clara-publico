import { Alt, Container, MdDiv } from "./styles";
import colors from "../../utils/colors";
import { Link, useParams } from "react-router-dom";
import Button from "../../components/Button";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import { Grid, IconButton, TextField } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useUser } from "../../contexts/auth";
import api from "../../services/api";
import WaitingPage from "../WaitingPage";
import React, { useState, useEffect } from "react";
import Axios from "axios";


export default function AddUserTrilha(props) {
  const history = useHistory();
  const { user } = useUser();
  const [cursos, setCursos] = useState([]);
  const [curso, setCurso] = useState();
  const [email, setEmail] = useState();

  const [apiFetched, setapiFetched] = useState(false);
  
  const api = Axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  async function fetchCursos() {
    let arrayCursos = []
    let id = 0;
    let erros = 0;

    while(true){
      try{
        id++;
        const res = await api.get(`/api/v1/getcurso?id=${id}`)
        arrayCursos.push(res.data);
      }
      catch {
        erros++;
        if(erros > 4) {
          break;
        }
      };      
    }    
    setapiFetched(true);
    setCursos(arrayCursos);
  }

  const handleChange = (event) => {
    setCurso(event.target.value);
  };  

  async function handleSubmit(event) {
    event.preventDefault();

    try{
      const res = await api.post("/api/v1/adduser", {
        idcurso : curso,
        email
      });
      history.push(`/dashboard`);
    }
    catch {
        alert("Erro ao adicionar")
      
    }    
  }

  useEffect(() => {    
    fetchCursos();
  }, []);

  return (
    apiFetched ? 
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack={`/dashboard`}
      />
      <Grid
        container
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Adicionar Usuário a Trilha</h2>
          <form onSubmit={handleSubmit}>   

            <InputTC
              id="curso_id"
              name= "curso_id"
              select
              className="input"
              label="Selecione um curso"
              onChange={handleChange}
              value={curso}
              SelectProps={{
                native: true,
              }}
              variant="filled"
            >
              {cursos.map((option) => (
                <option key={option.curso_id} value={option.curso_id}>
                  {option.curso_id+" - "+ (option.descricao || "Curso sem nome")}
                </option>
              ))}

            </InputTC>
              
            <InputTC
              id= "email"
              name= "email"
              className= "input"
              label= "E-mail do Aluno"
              value={email}
              onChange={e => setEmail(e.target.value)}
            />


            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
              type="submit"
              onClick={handleSubmit}
            />
          </form>
        </Grid>
      </Grid>
    </Container> : <WaitingPage/>
  );
}
