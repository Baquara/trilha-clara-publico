import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: auto;
  overflow-x: hidden;
  background: ${colors.primary};

  a {
    text-decoration: none;
  }

  .options {
    display: flex;
    align-self: flex-end;

    button {
      margin-left: 20px;
    }
  }

  .carousel {
    display: flex;
  }

  .ContainerItems {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas: "user main";
    margin: 50px auto;
    max-width: 1100px;
  }

  @media (max-width: 980px) {
    background: ${colors.primary};
    overflow: auto;

    .carousel {
      width: 100%;
      margin-top: 20px;
    }

    .ContainerItems {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      grid-template-columns: 1fr;
      grid-template-areas: "main";
      margin-top: 0;
      width: 90%;
    }

    .options {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;

      button {
        margin-left: 0px;
        margin-bottom: 10px;
      }
    }
  }
`;

export const SectionUser = styled.div`
  grid-area: user;
  display: flex;
  width: fit-content;

  @media screen and (max-width: 980px) {
    display: none;
  }
`;

export const SectionMain = styled.div`
  grid-area: main;
  display: flex;
  flex-direction: column;
  width: 90%;
  max-width: 800px;

  .carousel {
    width: 110%;
  }

  @media (max-width: 980px) {
    display: flex;
    justify-content: center;
    /* margin-left: auto;
    margin-right: auto; */
  }
`;
