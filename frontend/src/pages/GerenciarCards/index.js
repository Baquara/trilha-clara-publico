import React, { useEffect, useState } from "react";
import { Container, SectionMain, SectionUser } from "./styles";
import NavBar from "../../components/NavBar";
import colors from "../../utils/colors";
import CardInfo from "../../components/Cards/Infos";
import Edit from "../../components/Cards/Edit";
import CardText from "../../components/Cards/Text";
import NameStyle from "../../components/NameStyle";
import Carousel from "../../components/Carousel";
import UserInfos from "../PageLogged/User";
import CardCheck from "../../components/Cards/Check";
import Button from "../../components/Button";
import { useWindowDimensions } from "../../utils/useWindowDimesion";
import { Link, useHistory } from "react-router-dom";
import { useUser } from "../../contexts/auth";

const cards = [
  {
    id: 1,
    title: "Metodologias ágeis",
    type: "Info",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae varius dui, a luctus metus. Sed non urnajusto. Proin at enim elementum quam.",
    favorite: false,
  },
  {
    id: 2,
    title: "Metodologias ágeis",
    type: "Text",
    content: "O que é o Scrum?",
    favorite: true,
  },
  {
    id: 3,
    title: "Metodologias ágeis",
    type: "Check",
    content: "Scrum  é uma metodologia ágil?",
    options: ["Sim", "Não", "Talvez"],
    favorite: true,
  },
  {
    id: 4,
    title: "Metodologias ágeis",
    type: "Info",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae varius dui, a luctus metus. Sed non urnajusto. Proin at enim elementum quam.",
    favorite: false,
  },
];

export default function GerenciarCards() {
  const windowDimension = useWindowDimensions();
  const history = useHistory();
  const { user } = useUser();
  const calcNumberOfSlides = () => {
    if (!windowDimension) {
      return 1;
    }
    if (windowDimension.width > 1075) {
      return 3;
    }
    const cardWidth = 358;
    return windowDimension.width >= cardWidth
      ? parseInt(windowDimension.width / cardWidth)
      : 1;
  };
  const [numberOfCards, setNumberOfCards] = useState(calcNumberOfSlides());
  useEffect(() => setNumberOfCards(calcNumberOfSlides()), [windowDimension]);
  console.log(numberOfCards);

  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/home"
        />
      </header>
      <div class="ContainerItems">
        <SectionUser>
          <UserInfos
            class="UserInfos"
            username="Winicius"
            hours="60"
            points="30"
          />
        </SectionUser>

        <SectionMain>
          <NameStyle name="Gerenciar Cards" />

          <div className="carousel">
            <Carousel numberOfCards={numberOfCards}>
              {cards.map((card) => {
                if (card.type === "Check") {
                  return (
                    <>
                      <CardCheck
                        title={card.title}
                        content={card.content}
                        options={card.options}
                      >
                        <span>
                          <Edit
                            onClick={() => history.push("/addcard")}
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardCheck>
                    </>
                  );
                } else if (card.type === "Info") {
                  return (
                    <>
                      <CardInfo title={card.title} content={card.content}>
                        <span>
                          <Edit
                            onClick={() => history.push("/addcard")}
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardInfo>
                    </>
                  );
                } else {
                  return (
                    <>
                      <CardText title={card.title} content={card.content}>
                        <span>
                          <Edit
                            onClick={() => history.push("/addcard")}
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardText>
                    </>
                  );
                }
              })}
            </Carousel>
          </div>
          <div className="options">
            <Link to="/addcard">
              <Button
                name="Criar card"
                txtcolor={colors.primary}
                color={colors.aqua}
                onClick=""
              />
            </Link>
            {/* TODO: passar o id do curso nessa rota (/edittrilha/:courseId) */}
            <Link to="/edittrilha">
              <Button
                name="Finalizar edição"
                txtcolor={colors.primary}
                color={colors.green}
                onClick=""
              />
            </Link>
          </div>
        </SectionMain>
      </div>
    </Container>
  );
}
