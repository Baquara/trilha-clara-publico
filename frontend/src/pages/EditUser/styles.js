import styled from "styled-components";
import colors from "../../utils/colors";
import circles from "../../assets/img/Group 37.svg";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  background-image: url("${circles}");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;

  Input::placeholder {
    color: ${colors.gray2};
  }
  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .mainGrid {
    height: auto;
    margin-top: 60px;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }
  button {
    margin: 10px;
  }
  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .date {
    width: 80%;
    min-width: 250px;
    background-color: ${colors.gray4};
    color: ${colors.gray2};
  }
`;
