import React, { useState } from "react";
import "date-fns";
import { Container } from "./styles";
import Button from "../../components/Button";
import AddImg from "../../components/AddImg";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { useHistory } from "react-router-dom";
import { useUser } from "../../contexts/auth";

export default function EditarUser() {
  const { user } = useUser();
  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [data_nasc, setData_nasc] = useState("");
  const [username, setUsernamec] = useState("");
  const history = useHistory();

  function handleSubmit(event) {
    //   event.preventDefault();
    //   api
    //     .post(`${api}/edituser/${id}`, {
    //       idcurso,
    //       email,
    //
    //     })
    //     .then(() => {
    //       alert("usuariodit")
    //       history.push("/create")
    //     });
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={false}
          goBack="/settings"
        />
        <Grid
          container
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <AddImg />
            <h2>Editar Usuário</h2>
            <form onSubmit={handleSubmit}>
              <InputTC
                value={nome}
                onChange={(e) => setNome(e.target.value)}
                className="mainInput"
                label="Nome"
              />
              <InputTC
                onChange={(e) => setEmail(e.target.value)}
                className="mainInput"
                label="E-mail"
              />
              <KeyboardDatePicker
                onChange={(e) => setData_nasc(e.target.value)}
                margin="normal"
                id="date-picker-dialog"
                className="date"
                label="Data de nascimento"
                format="dd/MM/yyyy"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
              />
              <InputTC
                onChange={(e) => setUsernamec(e.target.value)}
                className="mainInput"
                label="Username"
              />
              <Button
                name="Confirmar"
                txtcolor={colors.primary}
                color={colors.aqua}
                onClick=""
              />
            </form>
          </Grid>
        </Grid>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
