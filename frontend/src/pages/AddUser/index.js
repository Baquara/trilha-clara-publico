import React, { useState } from "react";
import { Alt, Container, MdDiv } from "./styles";
import colors from "../../utils/colors";
import { Link, useParams } from "react-router-dom";
import Button from "../../components/Button";
import AddImg from "../../components/AddImg";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import { Grid, IconButton, TextField } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useUser } from "../../contexts/auth";
import { AiFillMinusCircle, AiFillPlusCircle } from "react-icons/ai";
import api from "../../services/api";

export default function AdicionarUser() {
  //Adicionado
  const [idcurso, setidCurso] = useState("");
  const [email, setEmail] = useState([]);
  const [array, setArray] = useState([
    {
      numb: 0,
      placeholder: "",
    },
  ]);
  const [corpo, setCorpo] = useState("" || []);
  const history = useHistory();
  const { user } = useUser();

  const [counter, setCounter] = useState(1);
  const { courseId } = useParams();

  function handleAddAlt() {
    setArray((old) => old.concat([{ numb: counter, placeholder: "" }]));
    setCounter(counter + 1);
  }
  function handleDelAlt(numb) {
    setArray((old) => old.filter((e) => e.numb !== numb));
    corpo[numb] = undefined;
  }

  async function handleSubmit(event) {
    event.preventDefault();

    const emails = [email, corpo.filter(Boolean)].flat();
    for (const email of emails) {
      const res = await api.post(
        "https://claralab.pythonanywhere.com/api/v1/adduser",
        {
          email,
          idcurso: Number(courseId),
        }
      );
    }
    history.push(`/editTrilha/${courseId}`);
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack={`/edittrilha/${courseId}`}
      />
      <Grid
        container
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Adicionar Usuários</h2>
          <form onSubmit={handleSubmit}>
            <ls>
              <Alt>
                <InputTC
                  id="pergunta"
                  // label={alt.placeholder}
                  placeholder="E-mail"
                  multiline
                  margin="normal"
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  variant="outlined"
                />
                <IconButton className="lados" onClick={handleAddAlt}>
                  <AiFillPlusCircle />
                </IconButton>
              </Alt>
              {array.map((alt, index) => (
                <Alt key={alt.numb}>
                  <InputTC
                    id="pergunta"
                    label={alt.placeholder}
                    className={`input${index}`}
                    placeholder="E-mail"
                    multiline
                    margin="normal"
                    onChange={(e) => (corpo[index] = e.target.value)}
                    // value={corpo}
                    variant="outlined"
                  />

                  <IconButton
                    className="lados"
                    onClick={() => handleDelAlt(alt.numb)}
                  >
                    <AiFillMinusCircle />
                  </IconButton>
                </Alt>
              ))}
            </ls>
            {/* <InputTC
              value={idcurso}
              onChange={(e) => setidCurso(e.target.value)}
              className="mainInput"
              label="E-mail"
            /> */}
            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
              type="submit"
              onClick={handleSubmit}
            />
          </form>
        </Grid>
      </Grid>
    </Container>
  );
}
