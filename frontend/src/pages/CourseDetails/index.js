import React, { useEffect, useState } from "react";
import { Container } from "./styles";
import Button from "../../components/Button";
import NavBar from "../../components/NavBar";
import Input from "../../components/Input";
import WaitingPage from "../WaitingPage";
import { Link, useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import colors from "../../utils/colors";
import Pic from "../../assets/img/Ellipse 3.png";
import { BsOctagonFill } from "react-icons/bs";
import { AiFillClockCircle } from "react-icons/ai";
import ProfilePic from "../../assets/img/Ellipse 2.png";
// import api from "../../services/api";
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function Index(props) {
  const { user } = useUser();
  const history = useHistory();
  const { courseId } = useParams();
  const cursoId = courseId;
  const [curso, setCurso] = useState("");
  // const cursoId = props.location.pathname.replaceAll(/\D/g, "");
  let userName = "";

  const api = axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  useEffect(() => {
    try {
      setCurso(props.location.state.curso);
      userName = props.location.state.userName;
    } catch {
      if (cursoId) {
        api.get(`/api/v1/getcurso?id=${cursoId}`).then((response) => {
          setCurso(response.data);
        });
      } else {
        history.push("/home");
        alert("Algo deu errado.");
      }
    }
  }, [cursoId]);

  return curso ? (
    <Container>
      <NavBar admin={user?.cargo == "professor"} logged={true} goBack="/home" />
      <div id="content">
        <div>
          <img id="coursePic" src={Pic} />
          <h2 id="title">{curso.nome}</h2>
          <p id="courseDesc">{curso.descricao}</p>
          <div>
            <a id="courseHours">
              <AiFillClockCircle /> {curso.horas} horas
            </a>
            <a id="coursePoints">
              <BsOctagonFill /> {curso.pontos} pontos
            </a>
          </div>
          {user?.cargo == "professor" ? (
            <Link to={`/edittrilha/${courseId}`}>
              <Button name="Trilha Digital" color={colors.green} />
            </Link>
          ) : (
            <Link to="/trilha">
              <Button name="Trilha Digital" color={colors.green} />
            </Link>
          )}
        </div>
      </div>
    </Container>
  ) : (
    <WaitingPage />
  );
}
