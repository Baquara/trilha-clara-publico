import styled from "styled-components";
import circles from "../../assets/img/Group 37.png";
import circlesM from "../../assets/img/Group 136.png";
import colors from "../../utils/colors";

export const Container = styled.div`
  background-image: url("${circles}"),
    linear-gradient(108.94deg, ${colors.primary} 34.76%, ${colors.pink} 90.77%);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  overflow: auto;
  color: #fff;
  display: flex;
  align-items: center;
  flex-direction: column;
  position: relative;

  #content {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    position: relative;
    justify-content: center;
    height: 100vh;
    Button {
      width: 180px;
      cursor: pointer;
      font-size: 16px;
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      margin-bottom: 50px;
      margin-top: 10px;
    }
    a {
      color: ${colors.green};
      text-decoration: none;
      margin: 10px;
    }
    #header {
      position: absolute;
      top: 8px;
      left: 16px;
      font-size: 18px;
    }
    #header img {
      width: 25px;
      margin-right: 5px;
    }
    #header a {
      color: #fff;
      font-weight: bold;
    }
    Button:hover {
      transition: 0.3s;
      background: ${colors.aqua};
      color: ${colors.primary};
    }
    #coursePic {
      width: 200px;
      margin-top: 5%;
    }
    #courseDesc {
      width: 300px;
      align-content: center;
    }
    .footerInfo:hover {
      font-weight: bold;
      color: ${colors.green};
      transition: 0.1s;
    }
    @media (max-width: 700px) {
      button {
        margin-bottom: 60px;
      }
      #coursePic {
        width: 150px;
        margin-top: 10%;
      }
      #courseDesc {
        width: 300px;
      }
      #non {
        display: none;
      }
    }
  }
`;
