import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Container } from "./styles";
import { SectionInput } from "./styles";
import { SectionImage } from "./styles";
import { SectionHeaderLeft } from "./styles";
import { SectionHeaderRight } from "./styles";
import Button from "../../components/Button";
import Input from "../../components/Input";
import NavBar from "../../components/NavBar";
import { Link } from "react-router-dom";
import colors from "../../utils/colors";
import Pic from "../../assets/img/photos.svg";
import bgPic from "../../assets/img/Group 136.png";
import logo from "../../assets/img/min-logo.png";

import api from "../../services/api";
import axios from "axios";
import { useAuth, useUser } from "../../contexts/auth";

export default function Index() {
  const { auth, setAuth } = useAuth();
  const { user, setUser } = useUser();
  const [usuario, setUsuario] = useState("");
  const [senha, setSenha] = useState("");
  // const [success, setSuccess] = useState(undefined);
  const history = useHistory();

  // useEffect(() => {
  //   if (auth) {
  //     history.push(user?.cargo === "membro" ? "/home" : "/dashboard");
  //   }
  // }, []);

  const api = axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const response = await api.post("/api/v1/login", {
        usuario,
        senha,
      });

      const profile = await api.get(`/api/v1/getuser?id=${response.data.id}`);
      console.log(profile.data);
      setAuth(response.data);
      setUser(profile.data);
      history.push(profile.data.cargo === "membro" ? "/home" : "/dashboard");
    } catch (error) {
      alert(error.response.data.msg);
      // alert('erro no cadastro')
    }
  }

  return (
    <Container>
      <SectionImage>
        <img id="desktop" src={Pic} />
        <img id="mobile" src={bgPic} />
      </SectionImage>

      <SectionHeaderLeft></SectionHeaderLeft>

      <SectionInput>
        <img className="logo" src={logo} />
        <h1>Login</h1>

        <form onSubmit={handleSubmit}>
          <Input
            className="loginInput"
            placeholder="Usuário"
            name="usuario"
            value={usuario}
            color="transparent"
            txtcolor={colors.green}
            bdstyle="solid"
            onChange={(e) => setUsuario(e.target.value)}
          />
          <Input
            className="loginInput"
            placeholder="Senha"
            type="password"
            value={senha}
            name="senha"
            color="transparent"
            txtcolor={colors.green}
            bdstyle="solid"
            onChange={(e) => setSenha(e.target.value)}
          />
          <div className="options">
            <Link to="/forgotpassword" id="forgot">
              <span>Esqueci minha senha</span>
            </Link>
            <div className="label">
              <div className="remember-container">
                <label for="remember" className="remind">
                  Lembrar de mim
                </label>
                <input type="checkbox" id="remember" className="remind" />
              </div>
            </div>
          </div>
          <Button
            id="loginButton"
            name="Login"
            color={colors.primary}
            txtcolor={colors.green}
            type="submit"
          />
        </form>

        {/* <div className="footer">
          <Link to="/">| CLARA IDEIA |</Link>
          <a> | </a>
          <a className="info" href="#">
            {" "}
            Parceiros e Depoimentos{" "}
          </a>
          <a> | </a>
          <a href="#"> Perguntas Frequentes (FAQ)</a>
        </div> */}
      </SectionInput>

      <SectionHeaderRight>
        <p>Ainda não tem uma conta?</p>
        <Link className="regiterButton" to="/registerAutentication">
          <Button name="Cadastre-se" color={colors.green} />
        </Link>
      </SectionHeaderRight>
    </Container>
  );
}
