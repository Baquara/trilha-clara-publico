import React, { useState } from "react";
import { Container } from "./styles";
import { SectionInput } from "./styles";
import { SectionImage } from "./styles";
import { SectionHeaderLeft } from "./styles";
import { SectionHeaderRight } from "./styles";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { Link, useHistory } from "react-router-dom";
import colors from "../../utils/colors";
import Pic from "../../assets/img/photos.svg";
import bgPic from "../../assets/img/Group 136.png";
// import api from "../../services/api";
import axios from "axios";

export default function EsqueciSenha() {
  const [email, setEmail] = useState("");

  const history = useHistory();

  function handleSubmitEmail(e) {
    e.preventDefault();

    axios
      .post(`https://claralab.pythonanywhere.com/api/v1/esenha`, {
        email,
      })
      .then((res) => {
        // alert("Enviado para email")

        console.log(res.data);

        setTimeout(() => history.push("/login"), 2000);
      })
      .catch(() => {
        alert("erro no cadastro");
      });
  }

  return (
    <Container>
      <SectionImage>
        <img id="desktop" src={Pic} />
        <img id="mobile" src={bgPic} />
      </SectionImage>

      <SectionHeaderLeft>
        {/* Deixar vazio para ajustar o header */}
      </SectionHeaderLeft>

      <SectionInput>
        <h1>
          Esqueci
          <br /> Minha Senha
        </h1>

        <form onSubmit={handleSubmitEmail}>
          <Input
            className="loginInput"
            placeholder="Insira seu e-mail"
            value={email}
            name="UserInput"
            color="transparent"
            txtcolor={colors.green}
            bdstyle="solid"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />

          <Button
            id="loginButton"
            name="Enviar"
            color={colors.primary}
            txtcolor="#fff"
            onclick=""
            type="submit"
          />
        </form>
        {/* <div className="footer">
          <a href="#">Sobre a CLARA IDEIA </a>
          <a> | </a>
          <a className="info" href="#">
            {" "}
            Parceiros e Depoimentos{" "}
          </a>
          <a> | </a>
          <a href="#"> Perguntas Frequentes (FAQ)</a>
        </div> */}
      </SectionInput>

      <SectionHeaderRight>
        <p>Ainda não tem uma conta?</p>
        <Link to="/register">
          <Button name="Cadastre-se" color={colors.green} onclick="" />
        </Link>
      </SectionHeaderRight>
    </Container>
  );
}
