import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  color: ${colors.green};
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas:
    "HeaderLeft HeaderRight"
    "Image  Input";
  background: linear-gradient(
      108.94deg,
      ${colors.primary} 34.76%,
      ${colors.pink} 90.77%
    ),
    ${colors.primary};
  height: 100vh;
  overflow: auto;
  Button {
    cursor: pointer;
    /* width: 150px; */
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  }
  a {
    color: white;
    text-decoration: none;
    margin: 10px;
  }

  a:hover {
    font-weight: bold;
    color: ${colors.green};
    transition: 0.1s;
  }

  button {
    margin-bottom: 20px;
  }

  #mobile {
    display: none;
  }
  @media (max-width: 700px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    #mobile {
      display: flex;
      width: 350px;
    }
    #desktop {
      display: none;
    }
    background: linear-gradient(
        180deg,
        ${colors.primary} 31.09%,
        ${colors.pink} 100%
      ),
      ${colors.primary};
  }
`;

export const SectionImage = styled.div`
  grid-area: Image;
  justify-content: center;
  align-items: center;
  img {
    width: 500px;
  }
`;

export const SectionInput = styled.div`
  grid-area: Input;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  Input::placeholder {
    color: ${colors.green};
    font-weight: bold;
  }

  Input {
    padding-left: 5px;
  }

  .loginInput {
    margin-bottom: 10px;
  }
  .label {
    input {
      margin-right: 10px;
    }
    Button {
      font-weight: bold;
      color: #fff;
      font-size: 16px;
    }
    Button:hover {
      transition: 0.3s;
      background: ${colors.pink};
      color: ${colors.primary};
    }
  }
  .footer {
    margin-top: 150px;
    display: inline-flex;
  }
  .remind {
    flex-direction: row;
    margin: 10px;
    margin-bottom: 20px;
  }
  #forgot {
    margin-top: -20px;
    margin-left: 110px;
    color: ${colors.green};
  }
  @media (max-width: 700px) {
    .footer {
      display: none;
    }
    .label {
      Button {
        width: 100%;
        font-weight: bold;
        color: ${colors.green};
        font-size: 16px;
      }
    }
  }
`;

export const SectionHeaderRight = styled.div`
  grid-area: HeaderRight;
  height: 80px;
  display: inline-flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: rgba(57, 57, 57, 0.3);

  @media (max-width: 700px) {
    background-color: transparent;
    flex-direction: column;
  }
  Button {
    /* width: 180px; */
    font-weight: bold;
    cursor: pointer;
    font-size: 16px;
  }
  Button:hover {
    transition: 0.3s;
    background: ${colors.aqua};
    color: ${colors.primary};
  }
`;
export const SectionHeaderLeft = styled.div`
  grid-area: HeaderLeft;
  background: rgba(57, 57, 57, 0.3);
  height: 80px;
`;
