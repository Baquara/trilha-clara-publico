import styled from 'styled-components'
import colors from '../../utils/colors'

export const WaitingContainer = styled.div `

width: 100%;
height: 100vh;
display: flex;
flex-direction: column;
align-items: center;
background: #3A1244;

.Waiting {
  margin-top: 40vh;
}

`

