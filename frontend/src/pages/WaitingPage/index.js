 import React from "react";
 import { PuffLoader } from "react-spinners";
 import { WaitingContainer } from "./style";

 export default function WaitingPage() {
   return (
     <WaitingContainer>
       <div className="Waiting">
         <PuffLoader size={150} color="#ffd700" loading />
       </div>
     </WaitingContainer>
   );
}
