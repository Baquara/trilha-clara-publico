import React, { Component, useState } from "react";
import { Container } from "./styles";
import Button from "../../components/Button";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid } from "@material-ui/core";
import Axios from "axios";
import api from "../../services/api";
import { useHistory, useParams } from "react-router-dom";
import { useUser } from "../../contexts/auth";

export default function AdicionarFase() {
  //Adicionado
  const { user } = useUser();
  const [titulo, setTitulo] = useState("");
  const [horas, setHoras] = useState("");
  const [pontos, setpontos] = useState("");
  const [numero_de_cards, setCards] = useState("");
  const history = useHistory();
  const { courseId } = useParams();

  function handleSubmit(event) {
    //   event.preventDefault();
    //   api
    //     .post(`${api}/addstage/${id}`, {
    //       titulo,
    //       horas,
    //       pontos,
    //       numero_de_cards,
    //     })
    //     .then(() => {
    //       alert("fasecadastrada")
    //       history.push("/home")
    //     });
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack={`/edittrilha/${courseId}`}
      />
      <Grid
        container
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Adicionar Etapa</h2>

          <form onSubmit={handleSubmit}>
            <InputTC
              value={titulo}
              onChange={(e) => setTitulo(e.target.value)}
              className="mainInput"
              label="Título"
            />
            <InputTC
              value={horas}
              onChange={(e) => setHoras(e.target.value)}
              className="mainInput"
              label="Horas"
            />
            <InputTC
              value={pontos}
              onChange={(e) => setpontos(e.target.value)}
              className="mainInput"
              label="Pontos"
            />
            {/* <InputTC
              value={numero_de_cards}
              onChange={(e) => setCards(e.target.value)}
              className="mainInput"
              label="Número de Cards"
            /> */}

            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
              onClick=""
            />
          </form>
        </Grid>
      </Grid>
    </Container>
  );
}
