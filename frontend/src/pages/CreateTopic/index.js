import React, { useState } from "react";
import "date-fns";
import { Container } from "./styles";
import Button from "../../components/Button";
import AddImg from "../../components/AddImg";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid } from "@material-ui/core";
import { useUser } from "../../contexts/auth";
import api from "../../services/api";
import { useHistory } from "react-router-dom";

export default function CreateTopic() {
  const history = useHistory()
  const [titulo, setTitulo] = useState("");
  const [categorias, setCategorias] = useState([]);
  const [texto, setTexto] = useState("");
  const { user } = useUser();

  async function onSubmit() {
    await api.post('forumtopicos?curso_id=1', {
      titulo,
      userid: String(user.id)
    })
    history.push('/forum')
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack="/forum"
      />
      <Grid
        container
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Criar Tópico</h2>

          <InputTC
            className="mainInput"
            label="Título"
            value={titulo}
            onChange={(e) => setTitulo(e.target.value)}
          />
          <InputTC
            className="mainInput"
            label="Categorias"
            value={categorias}
            onChange={(e) => setCategorias(e.target.value)}
          />
          <InputTC
            value={texto}
            onChange={(e) => setTexto(e.target.value)}
            className="mainInput largeInput"
            label="Texto"
            multiline={true}
            rows={4}
          />
          <Button
            name="Publicar"
            txtcolor={colors.primary}
            color={colors.aqua}
            onClick={onSubmit}
          />
        </Grid>
      </Grid>
    </Container>
  );
}
