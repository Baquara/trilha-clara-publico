import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: auto;
  background: ${colors.primary};

  a {
    text-decoration: none;
  }

  .main-content {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas: "user main";
    max-width: 1100px;
    margin: 50px auto;
  }

  @media screen and (max-width: 767px) {
    background: ${colors.primary};
    overflow: auto;
    width: 100vw;
    display: flex;
    flex-direction: column;

    .main-content {
      margin: 0;
      grid-template-columns: 1fr;
      grid-template-areas: "main";
    }
  }
`;

export const SectionUser = styled.div`
  grid-area: user;
  display: flex;
  width: fit-content;

  @media screen and (max-width: 767px) {
    display: none;
  }
`;

export const SectionMain = styled.div`
  grid-area: main;
  display: flex;
  flex-direction: column;
  width: 90%;

  @media screen and (max-width: 767px) {
    display: flex;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const SectionSearch = styled.div`
  display: flex;
  height: 135px;
  padding-top: 50px;

  .SearchBox {
    display: flex;
    width: 100%;
    height: 120px;
    background-color: ${colors.green};
    border-radius: 8px;
    justify-content: space-between;
    align-items: center;
  }

  h2 {
    color: ${colors.primary};
    font-weight: bold;
    font-size: 28px;
    margin-left: 30px;
    padding-bottom: 10px;
  }

  Input {
    margin-right: 30px;
  }

  @media screen and (max-width: 767px) {
    display: none;
  }
`;

export const SectionList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  .ListBox {
    width: 100%;
    display: flex;

    background-color: ${colors.gray};
    border-radius: 8px;
    flex-direction: column;
    margin-top: 10px;
  }

  .ListItems {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 20px;

    .checkbox {
      width: 22px;
      height: 22px;

      cursor: pointer;
    }

    h4 {
      font-size: 22px;
      font-weight: bold;
    }

    p {
      color: ${colors.primary};
      font-size: 22px;
      font-weight: bold;
    }
  }

  @media screen and (max-width: 767px) {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 0px;

    .ListBox {
      z-index: 2;
      display: flex;
      flex-wrap: wrap;
    }

    .ListItems {
      display: flex;
      flex-wrap: wrap;
      .checkbox {
        width: 15px;
        height: 15px;
        cursor: pointer;
      }

      h4 {
        font-size: 12px;
        font-weight: bold;
      }

      p {
        color: ${colors.primary};
        font-size: 12px;
        font-weight: bold;
        margin-right: 20px;
      }
    }
  }
`;

export const SectionButtons = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  /* width: 450px; */
  /* flex: auto; */
  /* margin-top: 17%; */

  input {
    width: 250px;
    margin-bottom: 10px;
  }

  .main-buttons {
    display: flex;
  }

  Button {
    color: ${colors.primary};
    font-weight: bold;
    font-size: 16px;
    margin-bottom: 10px;
    cursor: pointer;
    margin-left: 10px;
  }

  .Download:hover {
    transition: 0.3s;
    background: ${colors.pink};
    color: ${colors.primary};
  }

  .Upload:hover {
    transition: 0.3s;
    background: ${colors.green};
    color: ${colors.primary};
  }

  input {
    cursor: pointer;
    margin-top: 20px;
    display: flex;
    justify-content: center;
    color: white;
  }

  @media screen and (max-width: 767px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: 20px;

    Button {
      margin-left: 0px;
    }
  }
`;
