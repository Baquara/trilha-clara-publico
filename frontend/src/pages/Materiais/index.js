import React, { useState } from "react";
import { Container } from "./styles";
import { Link } from "react-router-dom";
import { SectionNav } from "./styles";
import { SectionSearch } from "./styles";
import { SectionUser } from "./styles";
import { SectionList } from "./styles";
import { SectionMain } from "./styles";
import { SectionButtons } from "./styles";
import { BsArrowLeftShort, BsSearch } from "react-icons/bs";
import Button from "../../components/Button";
import Input from "../../components/Input";
import UserInfos from "../PageLogged/User/index";
import NavBar from "../../components/NavBar";
import api from "../../services/api";
import NameStyle from "../../components/NameStyle";
import { useUser } from "../../contexts/auth";

export default function Materiais() {
  const [file, setFile] = useState();
  const { user } = useUser();

  const array = [
    {
      name: "introducao_jornada.pdf",
      tam: "5mb",
      uploader: "Nome.Tutor",
    },
    {
      name: "introducao_jornada.pdf",
      tam: "5mb",
      uploader: "Nome.Tutor",
    },
  ];

  function submitFile(e) {
    e.preventDefault();
    console.log(file);
    api.post("https://claralab.pythonanywhere.com/api/v1/upload", file);
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={true}
        goBack="/course"
      />

      <div class="ContainerItems">
        <div class="main-content">
          <SectionUser>
            <UserInfos
              username={user.nome}
              hours={user.numero_de_horas ?? 0}
              points={user.pontuacao_geral ?? 0}
            />
          </SectionUser>

          <SectionMain>
            {/* <SectionSearch>
              <div class="SearchBox">
                <h2>Materiais Disponíveis</h2>
                <Input
                  placeholder="Pesquisar documento"
                  type="name"
                  bdstyle="solid"
                />
              </div>
            </SectionSearch> */}
            <NameStyle name="Materiais" />

            <SectionList>
              {array.map((materials) => {
                return (
                  <div class="ListBox">
                    <div class="ListItems">
                      <label class="Checkcontainer">
                        <input
                          class="checkbox"
                          type="checkbox"
                          value={materials.uploader}
                        />
                      </label>
                      <h4>{materials.name}</h4>
                      <p>{materials.tam}</p>
                      <p>{materials.uploader}</p>
                    </div>
                  </div>
                ); //return
              })}
            </SectionList>

            <SectionButtons>
              <Button
                class="Download"
                name="Fazer download"
                color="#C1D731"
                txtcolor="#fff"
                onclick=""
              />
              <form onSubmit={submitFile}>
                <input
                  type="file"
                  accept="application/pdf"
                  onChange={(e) => setFile(e.target.files[0])}
                />
                <br />
                <Button
                  class="Upload"
                  name="Fazer upload"
                  color="#3CC0CC"
                  txtcolor="#fff"
                  onclick=""
                  type="submit"
                />
              </form>
            </SectionButtons>
          </SectionMain>
        </div>
      </div>
    </Container>
  );
}
