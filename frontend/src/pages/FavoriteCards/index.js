import React from "react";
import { Container, SectionMain, SectionUser } from "./styles";
import NavBar from "../../components/NavBar";
import colors from "../../utils/colors";
import CardInfo from "../../components/Cards/Infos";
import Favorites from "../../components/Cards/Favorite";
import CardText from "../../components/Cards/Text";
import NameStyle from "../../components/NameStyle";
import Carousel from "react-material-ui-carousel";
import UserInfos from "../PageLogged/User";
import CardCheck from "../../components/Cards/Check";
import { useUser } from "../../contexts/auth";

const favorites = [
  {
    id: 1,
    title: "Metodologias ágeis",
    type: "Info",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae varius dui, a luctus metus. Sed non urnajusto. Proin at enim elementum quam.",
    favorite: false,
  },
  {
    id: 2,
    title: "Metodologias ágeis",
    type: "Text",
    content: "O que é o Scrum?",
    favorite: true,
  },
  {
    id: 3,
    title: "Metodologias ágeis",
    type: "Check",
    content: "Scrum  é uma metodologia ágil?",
    options: ["Sim", "Não", "Talvez"],
    favorite: true,
  },
  {
    id: 4,
    title: "Metodologias ágeis",
    type: "Info",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae varius dui, a luctus metus. Sed non urnajusto. Proin at enim elementum quam.",
    favorite: false,
  },
];

export default function FavoriteCards() {
  const { user } = useUser();
  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/home"
        />
      </header>
      <div class="ContainerItems">
        <SectionUser>
          <UserInfos
            class="UserInfos"
            username="Winicius"
            hours="60"
            points="30"
          />
        </SectionUser>

        <SectionMain>
          <NameStyle name="Cards Favoritos " />

          <div className="carousel">
            <Carousel
              autoPlay={false}
              animation="slide"
              fullHeightHover={false}
            >
              {favorites.map((card) => {
                if (card.type === "Check") {
                  return (
                    <>
                      <CardCheck
                        title={card.title}
                        content={card.content}
                        options={card.options}
                      >
                        <span>
                          <Favorites
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardCheck>
                    </>
                  );
                } else if (card.type === "Info") {
                  return (
                    <>
                      <CardInfo title={card.title} content={card.content}>
                        <span>
                          <Favorites
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardInfo>
                    </>
                  );
                } else {
                  return (
                    <>
                      <CardText title={card.title} content={card.content}>
                        <span>
                          <Favorites
                            color={card.favorite ? colors.primary : "white"}
                          />
                        </span>
                      </CardText>
                    </>
                  );
                }
              })}
            </Carousel>
          </div>
        </SectionMain>
      </div>
    </Container>
  );
}
