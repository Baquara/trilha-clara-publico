import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: auto;
  overflow-x: hidden;
  background: ${colors.primary};

  a {
    text-decoration: none;
  }

  .ContainerItems {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas: "user main";
    margin: 50px auto;
    max-width: 1100px;
  }

  @media screen and (max-width: 767px) {
    background: ${colors.primary};
    overflow: auto;
    width: 100vw;
    display: flex;
    flex-direction: column;

    .ContainerItems {
      grid-template-columns: 1fr;
      grid-template-areas: "main";
      margin: 0;
    }
  }
`;

export const SectionUser = styled.div`
  grid-area: user;
  display: flex;
  width: fit-content;

  @media screen and (max-width: 767px) {
    display: none;
  }
`;

export const SectionMain = styled.div`
  grid-area: main;
  display: flex;
  flex-direction: column;
  width: 90%;

  @media screen and (max-width: 767px) {
    display: flex;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;

    .carousel {
      margin-top: 10px;
    }
  }
`;
