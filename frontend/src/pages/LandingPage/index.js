import React from "react";
import { Container, Logo } from "./styles";
import logo from "../../assets/img/logo.svg";
import banner from "../../assets/img/LPAsset.svg";
import Button from "../../components/Button";
import colors from "../../utils/colors";
import { Link } from "react-router-dom";
import { AiOutlineArrowRight } from "react-icons/ai";

export default function LandingPage() {
  return (
    <Container>
      <img className="banner" src={banner} />
      <div className="main-content">
        <Logo src={logo} />
        <p>
          Formas lúdicas de analisar e <br /> solucionar problemas{" "}
          <AiOutlineArrowRight className="logo" />
        </p>
      </div>
      <Link to="login">
        <Button
          name="Testar Gratuitamente"
          txtcolor={colors.primary}
          color={colors.green}
        />
      </Link>
    </Container>
  );
}
