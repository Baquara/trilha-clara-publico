import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  background: linear-gradient(
      165deg,
      ${colors.primary} 34.76%,
      ${colors.pink} 90.77%
    ),
    ${colors.primary};
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;
  overflow: auto;
  flex-direction: column;

  .banner {
    width: 300px;
    position: absolute;
    top: 50px;
    right: 0;
  }

  .main-content {
    display: flex;
    flex-direction: column;
    width: 635px;
    margin-top: 100px;

    p {
      color: #fff;
      align-self: flex-end;
      font-size: 30px;
      opacity: 0;
      transform: translateY(-45px);
      letter-spacing: 3px;
      animation: appear 2s ease-in-out forwards;
      animation-delay: 6s;

      svg {
        color: ${colors.green};
        transform: translateY(180px);
      }

      .logo {
        animation: bounce-horizontal 2s infinite;
      }
    }
  }

  @keyframes appear {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }

  @keyframes bounce-horizontal {
    0% {
      transform: translateX(5px);
    }

    50% {
      transform: translateX(-5px);
    }

    100% {
      transform: translateX(5px);
    }
  }

  @media (max-width: 980px) {
    background: linear-gradient(
        180deg,
        ${colors.primary} 31.09%,
        ${colors.pink} 100%
      ),
      ${colors.primary};

    .banner {
      width: 200px;
      top: 0px;
    }

    .main-content {
      width: auto;
      justify-content: center;
      align-items: center;

      p {
        align-self: center;
        font-size: 18px;
        transform: translateY(0px);
        letter-spacing: 0px;
        animation: appear 2s ease-in-out forwards;
        animation-delay: 2s;
      }
    }

    @keyframes appear-mobile {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }
  }
`;

export const Logo = styled.img`
  /* width: 60%; */
  width: 165px;
  height: 164px;
  object-fit: cover;
  position: relative;
  z-index: 2;
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-top: ${(props) => props.marginTop};
  animation: grow-left 4s ease-in-out forwards;
  animation-delay: 1.5s;
  transition: transform margin-left 2s ease-in-out;
  @media (max-width: 980px) {
    width: 200px;
    height: auto;
    animation: grow-left-mobile 1s ease-in-out forwards;
  }
  object-position: left 50%;

  @keyframes grow-left {
    0% {
      width: 165px;
      transform: scale(1.1, 1.1);
      opacity: 0.4;
      margin-left: 235px;
    }

    25% {
      width: 165px;
      transform: scale(1.1, 1.1);
      opacity: 1;
      margin-left: 235px;
    }

    75% {
      width: 165px;
      transform: scale(1.1, 1.1);
      opacity: 0.4;
      margin-left: 235px;
    }

    100% {
      width: 430px;
      transform: scale(1.1, 1.1);
      opacity: 1;
      margin-left: 0px;
    }
  }

  @keyframes grow-left-mobile {
    0% {
      width: 50px;
      transform: scale(1.1, 1.1);
      opacity: 0.4;
    }

    25% {
      transform: scale(1.1, 1.1);
      opacity: 1;
    }

    75% {
      transform: scale(1.1, 1.1);
      opacity: 0.4;
    }

    100% {
      transform: scale(1.1, 1.1);
      opacity: 1;
    }
  }
`;
