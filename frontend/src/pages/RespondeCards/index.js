import React, { useState, useEffect } from "react";
import { Container, Header, Main } from "./styles";
import CardInfo from "../../components/Cards/Infos";
import Button from "../../components/Button";
import colors from "../../utils/colors";
import CardCheck from "../../components/Cards/Check";
import CardText from "../../components/Cards/Text";
import Carousel from "react-material-ui-carousel";
import NavBar from "../../components/NavBar";
import Favorites from "../../components/Cards/Favorite";
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function ResCards() {
  const { user } = useUser();
  function handleSubmit(event) {
    //   event.preventDefault();
    //   api
    //     .post(`${api}/cardanswers/${id}`, {
    //       id,
    //       tipo,
    //       resposta,
    //
    //     })
    //     .then(() => {
    //       alert("Respondendocomsucesso")
    //       history.push("/myprofile")
    //     });
  }

  const [data, setData] = useState([]);
  const [render, setRender] = useState(false);

  const [resPsubj, setResPsubj] = useState();
  const [resposta, setResposta] = useState();

  // function submitResCards(){
  //   axios.post('https://',)
  // }

  useEffect(() => {
    async function fetchData() {
      await axios
        .get(`https://claralab.pythonanywhere.com/api/v1/getcards?id=1`)
        .then((response) => {
          console.log(response.data.cards);
          setData(response.data.cards);
          setRender(true);
        })
        .catch((err) => console.log(err));
    }

    fetchData();
  }, []);

  let card_id = 0;

  function submitResCards(e) {
    e.preventDefault();
    axios
      .post("https://claralab.pythonanywhere.com/api/v1/respostas", {
        respostas: [
          {
            user_id: 2,
            card_id,
            resposta,
          },
        ],
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  console.log(data);

  const [favorites, setFavorites] = useState(data);

  function addFavorite(id) {
    if (data[id].favorite) {
      data[id].favorite = false;
    } else {
      data[id].favorite = true;
    }
  }

  // array[id].favorite ? array[id].favorite = false && color = colors.lightgray : array[id].favorite = true

  console.log(favorites);

  return (
    <Container>
      <Header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/trilha"
        />
      </Header>
      <Main>
        <h1>Fase 1</h1>

        <form onSubmit={submitResCards}>
          <div className="carousel">
            {render && (
              <Carousel
                autoPlay={false}
                strictIndexing={false}
                animation="fade"
                fullHeightHover={false}
              >
                {data.map((card, index) => {
                  if (card.tipo === "pobj") {
                    return (
                      <>
                        <CardCheck title={card.titulo} options={card.corpo}>
                          {/* <span onClick={() => addFavorite(index)} >
                        </span> */}
                          <Favorites color={colors.primary} />
                        </CardCheck>
                        <h2>
                          {index + 1}/{data.length}
                        </h2>
                      </>
                    );
                  } else if (card.tipo === "informacao") {
                    return (
                      <>
                        <CardInfo title={card.titulo} content={card.corpo}>
                          {/* <span onClick={() => addFavorite(index)} >
                        </span>                     */}
                          <Favorites color={colors.primary} />
                        </CardInfo>
                        <h2>
                          {index + 1}/{data.length}
                        </h2>
                      </>
                    );
                  } else if (card.tipo === "psubj") {
                    card_id = card.id;
                    return (
                      <>
                        <CardText
                          onChange={(e) => setResposta(e.target.value)}
                          title={card.titulo}
                          content={card.corpo}
                        >
                          {/* <span onClick={() => addFavorite(index)} >
                        </span> */}
                          <Favorites color={colors.primary} />
                        </CardText>
                        <h2>
                          {index + 1}/{data.length}
                        </h2>
                      </>
                    );
                  }
                })}
              </Carousel>
            )}
          </div>
          <Button
            name="Concluir"
            color={colors.green}
            txtcolor={colors.primary}
            onclick=""
            type="submit"
          />
        </form>
      </Main>
    </Container>
  );
}
