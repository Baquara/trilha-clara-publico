import styled from 'styled-components'
import colors from '../../utils/colors'


export const Container = styled.div`
  margin: 0 auto;
  background: linear-gradient(108.94deg, ${colors.primary} 34.76%, ${colors.pink} 90.77%), ${colors.primary};
  height: 100vh;
  width: 100vw;
  overflow: auto;

  button{
    font-weight: bold;
  }
`

export const Header = styled.div`
  grid-area: topo;
  width: 100vw;
  margin-bottom: 20px;
  background: ${colors.primary};
`

export const Main = styled.div`
  margin: 0 auto;
  text-align: center;
  justify-content: center;
  

  h1{
    margin: 20px;
    color: ${colors.lightgray};
  }

  button{
    margin-top: 20px;
  }

  .carousel{
    display: flex;
    justify-content: center;
  }

  .carousel h2{
    color: ${colors.green};
    margin-top: -10px;
  }


`
