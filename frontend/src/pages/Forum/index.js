import { capitalize } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import KeyboardArrowDownRoundedIcon from "@material-ui/icons/KeyboardArrowDownRounded";
import SubdirectoryArrowRightIcon from "@material-ui/icons/SubdirectoryArrowRight";
import ThumbUpAltOutlinedIcon from "@material-ui/icons/ThumbUpAltOutlined";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import NameStyle from "../../components/NameStyle";
import NavBar from "../../components/NavBar";
import { useUser } from "../../contexts/auth";
import api from "../../services/api";
import UserInfos from "../PageLogged/User";
import { Comment, Container, Post } from "./styles";

const useStyles = makeStyles({
  root: {
    background: "white",
    borderRadius: 10,
    border: 0,
    width: "auto",
    color: "white",
    height: 48,
    marginTop: 10,
  },
  input: {
    paddingLeft: 10,
    width: 275,
  },
  icon: {
    marginTop: 2000,
    color: "white",
  },
  iconButton: {
    color: "black",
  },
});

export default function Forum() {
  const classes = useStyles();
  const [commentId, setCommentId] = useState("");
  const [commentOpen, setCommentOpen] = useState();
  const [postOpen, setPostOpen] = useState();
  const [postId, setPostId] = useState(0);
  const [categoriesOpen, setCategoriesOpen] = useState();
  const [chevronId, setChevronId] = useState();
  const { user } = useUser();
  const [posts, setPosts] = useState([])

  const forum = [
    {
      id: 1,
      pfp: require("../../assets/img/Ellipse 29.svg"), //ajustar import de imagens pelo vetor
      user: "Adalberto Fortunato",
      role: "Tutor",
      title:
        "Sugestão sobre os critérios de inovação propostos na primeira etapa.",
      likes: 0,
      comments: 0,
      content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales sit ipsum dictum vulputate imperdiet turpis hendrerit elit amet. Sed suspendisse scelerisque ipsum elit pulvinar purus. Sed vulputate nunc diam augue volutpat, etiam ac, malesuada. Id libero adipiscing consectetur netus odio dignissim neque. Habitant fusce faucibus senectus nisl. Velit nulla tempor, nulla sit felis viverra. Mauris sit eget nunc ornare ut pellentesque nibh mi libero. Ut dictumst dictum risus lectus mauris, egestas a faucibus arcu. Quis fringilla pharetra non pulvinar porttitor quis. Massa, quam ipsum est viverra fermentum. Tellus tortor fermentum vitae in sed. Sit quis ultrices elementum elementum. Eu vitae est rhoncus sit molestie lorem est, eros eget. Venenatis ultrices justo ultricies convallis sem. Ipsum leo neque adipiscing vel semper. Sit suspendisse malesuada ipsum quam arcu neque, blandit amet, fermentum. Elementum platea eget vivamus posuere eros, tristique enim nunc ut. Adipiscing convallis iaculis sed iaculis elementum ipsum congue aliquam feugiat. At sed leo quis convallis nulla at iaculis vitae. Nibh volutpat, id habitant sit. Tortor habitasse in amet eu massa suscipit posuere hendrerit venenatis.`,
      category: [
        {
          cat: "Dicas",
        },
        {
          cat: "Dúvidas",
        },
      ],
      postImg: require("../../assets/img/Rectangle 94.svg"),
      date: "01/01/2020",
      time: "11h32m",
    },
    {
      id: 2,
      pfp: require("../../assets/img/Ellipse 291.svg"), //ajustar import de imagens pelo vetor
      user: "Adalberto Fortunato",
      role: "Tutor",
      title: "Título do Tópico",
      likes: 0,
      comments: 0,
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque ac libero nascetur et enim sed diam pretium tempus. Placerat diam auctor velit at. Nulla ut ac eget ullamcorper ultricies. Felis, nulla amet sem pharetra, arcu mi quis. A nisl elementum laoreet sit. Felis, faucibus urna nunc, facilisis turpis viverra. In et tellus mi urna. At libero diam purus quis semper quis. Diam feugiat augue urna tristique. Placerat placerat varius ut erat lacinia vulputate. Sed in integer vitae ac. Vel, mi turpis lobortis justo, eu pellentesque fermentum...",
      category: [
        {
          cat: "Dúvidas",
        },
        {
          cat: "Discussão",
        },
      ],
      postImg: require("../../assets/img/Rectangle 94.svg"),
      date: "",
      time: "",
    },
    {
      id: 3,
      pfp: require("../../assets/img/Ellipse 30.svg"), //ajustar import de imagens pelo vetor
      user: "Adalberto Fortunato",
      role: "Tutor",
      title: "Título do Tópico",
      likes: 0,
      comments: 0,
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque ac libero nascetur et enim sed diam pretium tempus. Placerat diam auctor velit at. Nulla ut ac eget ullamcorper ultricies. Felis, nulla amet sem pharetra, arcu mi quis. A nisl elementum laoreet sit. Felis, faucibus urna nunc, facilisis turpis viverra. In et tellus mi urna. At libero diam purus quis semper quis. Diam feugiat augue urna tristique. Placerat placerat varius ut erat lacinia vulputate. Sed in integer vitae ac. Vel, mi turpis lobortis justo, eu pellentesque fermentum...",
      category: [
        {
          cat: "Dicas",
        },
        {
          cat: "Inovação",
        },
        {
          cat: "Gestão",
        },
      ],
      postImg: require("../../assets/img/Rectangle 94.svg"),
      date: "",
      time: "",
    },
  ];
  const comments = [
    {
      user: "Nikita Laureano",
      role: "Trainee",
      pfp: require("../../assets/img/Ellipse 292.svg"),
      content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ultrices aliquet semper lectus sagittis urna. Eget elit ornare id imperdiet vestibulum lacus. Amet, purus id fusce ipsum sed. Est nunc elit, id in volutpat ut egestas morbi! ❤️`,
    },
  ];

  React.useEffect(() => {
    const getPosts = async () => {
      const response = await api.get('forumtopicos?curso_id=1')
      if (response) {
        const { topicos } = response.data
        const novosTopicos = await Promise.all(
          topicos.map(async topico => {
            const user = await api
              .get(`getuser?id=${topico.user_id}`)
              .then(res => res.data)
              .catch(e => null)
            return {
              ...topico,
              user
            }
          })
        )
        setPosts(novosTopicos)
      }
    }
    getPosts()
  }, [])

  function expandPost(key) {
    return postId === key && postOpen ? `100%` : "90px"; //verifica se teste e verdadeiro ou falso e define o valor da funcao expandPost
  }
  function expandComment(key) {
    return commentId === key && commentOpen ? "flex " : "none";
  }
  function expandCategories() {
    return categoriesOpen ? "none" : "flex";
  }
  function rotateChevron(key) {
    return chevronId === key && postOpen ? "rotate(270deg)" : "rotate(0deg)";
  }

  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/course"
        />
      </header>
      <div class="ContainerItems">
        <Grid id="user" item xs>
          <UserInfos
            username={user.nome}
            hours={user.numero_de_horas ?? 0}
            points={user.pontuacao_geral ?? 0}
          />
          <Link to="/createTopic">
            <Button className="button" variant="contained">
              Escrever tópico
            </Button>
          </Link>
          {/* <div id="categories">
            <ListIcon fontSize="medium" /> <b> Categorias </b>
            <div>
              <a>Dicas</a>
              <a>Dúvidas</a>
              <a>Discussão</a>
              <a>Criatividade</a>
              <a>Inovação</a>
              <a>Gestão</a>
            </div>
          </div> */}
        </Grid>
        <div id="stuff">
          <NameStyle name="Fórum" />
          <Link className="mobile-button" to="/createTopic">
            <Button className="button" variant="contained">
              Escrever tópico
            </Button>
          </Link>
          {/* <Grid id="search">
            <div id="title">
              <h1>Forum</h1>
              <div id="mobileCategories">
                <IconButton
                  onClick={() => {
                    setCategoriesOpen(!categoriesOpen);
                  }}
                  size="small"
                >
                  <ListIcon />
                  <h4>Categorias</h4>
                </IconButton>
                <Collapse in={!categoriesOpen}>
                  <div id="mobile" style={{ display: `${expandCategories()}` }}>
                    <div>
                      <a href="#">Dicas</a>
                      <a href="#">Dúvidas</a>
                      <a href="#">Discussão</a>
                    </div>
                    <div>
                      <a href="#">Criatividade</a>
                      <a href="#">Inovação</a>
                      <a href="#">Gestão</a>
                    </div>
                  </div>
                </Collapse>
              </div>
            </div>
            <div id="searchbox">
              <Paper
                classes={{
                  root: classes.root,
                }}
              >
                <InputBase
                  className={classes.input}
                  placeholder="Pesquisar..."
                  inputProps={{ "aria-label": "naked" }}
                />
                <IconButton type="submit" aria-label="search">
                  <SearchIcon />
                </IconButton>
              </Paper>
            </div>
          </Grid> */}
          <Grid id="content">
            {posts && posts?.map((post, index) => {
              return (
                <Post id="post" key={post.titulo}>
                  <div id="postBody">
                    <postContent
                      onClick={() => {
                        setPostId(post.titulo);
                        setPostOpen(!postOpen);
                      }}
                    >
                      <Grid xs id="postInfo">
                        <div id="title">
                          <div id="titleText">
                            <h4>{post.user?.nome ?? forum[0].user}</h4>
                            <a>{capitalize(post.user?.cargo ?? forum[0].role)}</a>
                            <div>
                              <h2>{post.titulo}</h2>{" "}
                              <IconButton
                                style={{ transform: `${rotateChevron()}` }}
                                key={post.titulo}
                              >
                                <KeyboardArrowDownRoundedIcon />
                              </IconButton>
                            </div>
                          </div>
                          <img src={forum[0].pfp} />
                        </div>
                        <div id="cat">
                          {forum[0].category.map((cat) => (
                            <div>
                              <p>{cat.cat}</p>
                            </div>
                          ))}
                        </div>
                      </Grid>
                      {/* <Collapse in={postOpen} collapsedHeight={95}> */}
                      <div
                        style={{ maxHeight: `${expandPost(index)}` }}
                        id="postContent"
                      >
                        <Grid>
                          <p>{forum[0].content}</p>
                          <img src={forum[0].postImg} />
                        </Grid>
                      </div>
                      {/* </Collapse> */}
                    </postContent>
                    <postFooter>
                      <Grid justify="space-around" id="contentInfo">
                        <Grid>
                          <a id="date">
                            Publicado em {forum[0].date} as {forum[0].time}
                          </a>
                        </Grid>
                        <Grid direction="row">
                          <IconButton
                            className={classes.IconButton}
                            size="small"
                          >
                            {forum[0].likes}
                            <ThumbUpAltOutlinedIcon fontSize="inherit" />
                          </IconButton>
                          <IconButton
                            className={classes.IconButton}
                            onClick={() => {
                              setCommentId(index);
                              setCommentOpen(!commentOpen);
                              console.log(expandComment(index));
                            }}
                            size="small"
                          >
                            {forum[0].comments}
                            <SubdirectoryArrowRightIcon fontSize="inherit" />
                            <a>Comentários</a>
                          </IconButton>
                        </Grid>
                      </Grid>
                    </postFooter>
                  </div>
                  <Collapse in={commentOpen}>
                    <div
                      style={{ display: `${expandComment(index)}` }}
                      key={index}
                      id="comments"
                    >
                      <div>
                        {comments.map((comment) => {
                          return (
                            <Comment>
                              <div id="commentInfo">
                                <img src={comment.pfp} />
                                <div>
                                  <h3>{comment.user}</h3>
                                  <a>{comment.role}</a>
                                </div>
                              </div>
                              <div id="commentContent">
                                <p>{comment.content}</p>
                              </div>
                            </Comment>
                          );
                        })}
                      </div>
                    </div>
                  </Collapse>
                </Post>
              );
            })}
          </Grid>
        </div>
      </div>
    </Container>
  );
}
