import styled from "styled-components";
import colors from "../../../utils/colors";

export const Accordion = styled.div`
  display: none;
  color: ${colors.green};
  margin-left: 50px;
  display: flex;
  align-items: center;

  div {
    /* margin-top: 10px; */
    margin-left: 20px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    text-align: left;
    width: 10px;
  }
  svg {
    /* margin-bottom: -6px; */
  }

  a:hover {
    font-weight: bold;
    color: ${colors.green};
    transition: 0.1s;
  }

  .accordionContent {
    overflow: scroll; //temporario
    display: flex;
    flex-direction: column;
    text-align: left;
    /* margin-left: 10px; */
  }
  .accordionTitle {
    display: flex;
    align-self: flex-start;
    justify-self: flex-start;
    /* margin-left: 10px; */
  }

  @media (max-width: 700px) {
    display: block;
    margin-left: 0px;
  }
`;
