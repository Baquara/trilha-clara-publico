import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  overflow: auto;
  background: ${colors.primary};
  height: 100vh;
  font-family: "Roboto", sans-serif;

  a {
    text-decoration: none;
  }

  .button {
    margin: 20px;
    background: ${colors.green};
    font-weight: bold;
    width: 200px;
  }

  .mobile-button {
    display: none;
  }

  .ContainerItems {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas: "user main";
    max-width: 1100px;
    margin: 50px auto;

    #user {
      grid: user;
      display: flex;
      align-items: center;
      flex-direction: column;

      img {
        /* width: 50%; */
        justify-self: center;
        align-self: center;
      }
      h2,
      h3 {
        color: white;
        margin: 0px;
        justify-self: center;
        align-self: center;
      }
      #userinfo {
        display: flex;
        flex-direction: column;
        margin-top: 10px;
        margin-bottom: 10px;
        color: ${colors.green};
        justify-self: center;
        align-self: center;
      }
    }
    #stuff {
      grid: main;
      display: flex;
      flex-direction: column;
      width: 90%;

      #search {
        background: ${colors.green};
        border-radius: 10px;
        margin: 10px;
        display: flex;
        flex-direction: row;
        h1 {
          margin-left: 10px;
          color: ${colors.primary};
        }
        #searchbox {
          margin-top: 10px;
          margin-left: 50%;
        }
        #mobileCategories {
          display: none;
        }
      }
    }
  }

  @media (max-width: 700px) {
    .mobile-button {
      display: block;
      .button {
        width: 100%;
        margin: 0px;
        margin-top: 20px;
      }
    }

    .ContainerItems {
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-top: 20px;
      grid-template-columns: 1fr;
      grid-template-areas: "main";
      margin: 0;

      #user {
        display: none;
      }
      #stuff {
        #search {
          background: ${colors.primary};
          flex-direction: column;
          h1 {
            text-align: center;
          }
          #title {
            background: ${colors.green};
            border-radius: 10px;
            #mobileCategories {
              display: block;
              #mobile {
                flex-direction: row;
                div {
                  margin: 10px;
                  a {
                    text-decoration: none;
                    margin: 5px;
                  }
                  display: flex;
                  flex-direction: column;
                }
              }
            }
          }
          #searchbox {
            margin: 0px;
          }
        }
      }
    }
  }
`;
export const Post = styled.div`
  #postBody {
    background: #fff;
    border-radius: 10px;
    margin-top: 10px;
    #postInfo {
      display: flex;
      flex-direction: column;

      img {
        align-self: flex-start;
        margin: 10px;
      }
      #title {
        display: flex;
        flex-direction: row-reverse;
        align-content: flex-end;
        justify-content: flex-end;
        #titleText {
          #mobileContentInfo {
            display: none;
          }
          div {
            display: flex;
            flex-direction: row;
          }
        }

        a {
          margin: 5px;
        }
        h4 {
          margin: 5px;
          margin-top: 25px;
        }
      }
      #cat {
        display: flex;
        flex-direction: row;
        margin-left: 10px;
        div {
          border-radius: 5px;
          color: white;
          margin: 5px;
          background: ${colors.primary};
        }
        p {
          margin: 5px;
        }
      }
    }
    #postContent {
      transition: height 2s linear;
      overflow: hidden;
      margin: 25px;
      img {
        width: 100%;
      }
    }
    #contentInfo {
      display: flex;
      flex-direction: row;
      color: ${colors.primary};
      margin: 10px;
    }
    @media (max-width: 700px) {
      #title {
        #mobileContentInfo {
          display: flex;
        }

        h2 {
          font-size: 18px;
        }
      }
      #content {
        padding: 10px;
      }
      #contentInfo {
        a {
          display: none;
        }
        #date {
          display: flex;
          align-self: flex-start;
          justify-self: flex-start;
          font-size: 12px;
        }
      }
    }
  }
`;
export const Comment = styled.div`
  background: #fff;
  margin-left: 15%;
  width: 84.3%;
  border-radius: 10px;
  #commentInfo {
    display: flex;
    flex-direction: row;
    img {
      margin: 10px;
    }
  }
  #commentContent {
    padding: 20px;
  }
`;
