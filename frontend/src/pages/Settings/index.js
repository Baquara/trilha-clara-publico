import React, { useState, useEffect } from "react";
import { Container } from "./styles";
// import {Link} from 'react-router-dom';
// import { SectionNav } from './styles';
import { SectionSearch } from "./styles";
import { SectionUser } from "./styles";
import { SectionList } from "./styles";
import { SectionMain } from "./styles";
import { BsPencil } from "react-icons/bs";
import { BiLogOutCircle } from "react-icons/bi";
import Input from "../../components/Input";
import UserInfos from "../PageLogged/User/index";
import NavBar from "../../components/NavBar";
import ProfilePic from "../../assets/img/Ellipse 2.png";
import NameStyle from "../../components/NameStyle";
import { Link, useHistory } from "react-router-dom";
import { useAuth, useUser } from "../../contexts/auth";

export default function Settings() {
  const [search, setSearch] = useState("");
  const history = useHistory();
  const { setAuth } = useAuth();
  const { setUser } = useUser();
  const { user } = useUser();
  // const [data, setData] = useState();
  // const [courseId, setCourseId] = useState();

  // useEffect(() => {
  //     async function fetchData() {
  //         const result = await axios
  //         .get(`${api}/getmemberse/${id}`)
  //         .then((response) => {
  //             console.log(response.data);
  //             setData(result.data);
  //         })
  //         .catch((err) => console.log(err));
  //     };

  //     fetchData();
  // }, [id]);

  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/home"
        />
      </header>
      <div class="ContainerItems">
        <div className="main-content">
          <SectionUser>
            <UserInfos
              class="UserInfos"
              username="Winicius"
              hours="60"
              points="30"
            />
          </SectionUser>

          <SectionMain>
            <NameStyle name="Opções" />

            <SectionList>
              <div class="ListBox">
                <div class="ListItems">
                  <Link to="/newpassword">
                    <h4>Alterar senha</h4>
                  </Link>                  
                  <i>
                    <BsPencil class="ChatIcon" />
                  </i>
                </div>
              </div>
              <div class="ListBox">
                <Link to="/edituser">
                  <div class="ListItems">
                      <h4>Editar informações cadastrais</h4>
                    <i>
                      <BsPencil class="ChatIcon" />
                    </i>
                  </div>
                </Link>
              </div>

              <div
                class="ListBox danger"
                to="/login"
                onClick={() => {
                  localStorage.clear();
                  setUser({});
                  setAuth(undefined);
                  history.push("/login");
                }}
              >
                <div class="ListItems">
                  <h4>Logout</h4>
                  <i>
                    <BiLogOutCircle class="ChatIcon" />
                  </i>
                </div>
              </div>
            </SectionList>
          </SectionMain>
        </div>
      </div>
    </Container>
  );
}
