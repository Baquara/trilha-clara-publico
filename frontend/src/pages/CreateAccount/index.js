import React, { useState } from "react";
import "date-fns";
import { Container } from "./styles";
import Button from "../../components/Button";
import AddImg from "../../components/AddImg";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { setDate } from "date-fns";
import { useUser } from "../../contexts/auth";

export default function CreatAccount() {
  const { user } = useUser();
  const [selectedDate, setSelectedDate] = useState();
  const [usuario, setUser] = useState("");
  const [senha, setSenha] = useState("");
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const history = useHistory();

  const api = axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  async function handleSubmit(e) {
    e.preventDefault();

    if (usuario && senha && nome && email && selectedDate) {
      const data_nasc = selectedDate;

      await api
        .post("/api/v1/createuser", {
          usuario,
          email,
          senha,
          data_nasc,
          nome,
        })
        .then((response) => {
          alert(response.data.msg);
          history.push({
            pathname: "/login",
          });
        })
        .catch((error) => {
          alert(error.response.data.msg);
        });
    } else {
      alert("Favor preencher todos os campos!");
    }
  }

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/home"
        />
        <Grid
          container
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <form onSubmit={handleSubmit}>
              <AddImg />
              <h2>Criar Usuário</h2>

              <InputTC
                className="mainInput"
                label="Usuário"
                value={usuario}
                name="usuario"
                onChange={(e) => setUser(e.target.value)}
              />
              <InputTC
                className="mainInput"
                type="password"
                label="Senha"
                value={senha}
                name="senha"
                onChange={(e) => setSenha(e.target.value)}
              />
              <InputTC
                className="mainInput"
                label="Nome"
                value={nome}
                name="nome"
                onChange={(e) => setNome(e.target.value)}
              />
              <InputTC
                className="mainInput"
                label="E-mail"
                value={email}
                name="email"
                onChange={(e) => setEmail(e.target.value)}
              />
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                className="date"
                label="Data de nascimento"
                format="dd/MM/yyyy"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
              />
              <Button
                id="btnCadastrar"
                name="Cadastrar"
                type="submit"
                txtcolor={colors.primary}
                color={colors.green}
              />
            </form>
          </Grid>
        </Grid>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
