import React, { useState } from "react";
import { Container } from "./styles";
import Button from "../../components/Button";
import AddImg from "../../components/AddImg";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid, Snackbar } from "@material-ui/core";

import NumberInputTC from "../../components/NumberInputTC";
import api from "../../services/api";
import SnackMessageTC from "../../components/SnackMessageTC";
import { useLocation, useHistory } from "react-router-dom";
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function CriarTrilha() {
  const { user } = useUser();
  const [nome, setNome] = useState("");
  const [horas, setHoras] = useState("");
  const [descricao, setDescricao] = useState("");
  const [pontos, setPontos] = useState("");
  const [empresa, setEmpresa] = useState("");
  const [success, setSuccess] = useState(undefined);

  const history = useHistory();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  async function handleSubmit(e) {
    e.preventDefault();
    await axios
      .post("https://claralab.pythonanywhere.com/api/v1/criatrilha", {
        nome,
        pontos,
        horas,
        empresa,
        descricao,
      })
      .then((res) => {
        console.log(res.data);
        setSuccess(true);
      })
      .catch((error) => {
        setSuccess(false);
        setNome("");
        setHoras("");
        setDescricao("");
        setPontos("");
        setEmpresa("");
        setTimeout(() => history.push("/home"), 2500);
      });
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack="/dashboard"
      />
      <form onSubmit={handleSubmit}>
        <Grid
          container
          component="form"
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
          onSubmit={handleSubmit}
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <AddImg />
            <h2>Criar Trilha</h2>
            <InputTC
              className="mainInput"
              label="Título"
              value={nome}
              onChange={(e) => setNome(e.target.value)}
            />
            <InputTC
              className="mainInput"
              label="Pontos"
              value={pontos}
              onChange={(e) => setPontos(e.target.value)}
            />
            <InputTC
              label="Horas"
              value={horas}
              onChange={(e) => setHoras(e.target.value)}
            />
            <InputTC
              label="Empresa"
              value={empresa}
              onChange={(e) => setEmpresa(e.target.value)}
            />
            <InputTC
              className="mainInput largeInput"
              label="Descrição"
              multiline={true}
              rows={4}
              value={descricao}
              onChange={(e) => setDescricao(e.target.value)}
            />
            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
              type="submit"
            />
          </Grid>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={success !== undefined}
            autoHideDuration={1600}
            onClose={handleClose}
          >
            {success !== undefined && (
              <SnackMessageTC
                message={success ? "Erro" : "Sucesso"}
                handleClose={handleClose}
                type={success ? "error" : "success"}
              />
            )}
          </Snackbar>
        </Grid>
      </form>
    </Container>
  );
}
