import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  background: ${colors.primary};
  color: white;
  height: 100vh;
  overflow: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;

  .content {
    display: block;
    color: black;
    background: #fafafa;
    border-radius: 25px;
    width: 300px;
    min-height: 380px;
    padding: 10px;
    border: 10px solid ${colors.pink};
  }
  Button {
    background: ${colors.purple};
    font-weight: bold;
    width: 90%;
    height: 50px;
    margin: 10px;
    color: white;
  }
  Button:hover {
    background-color: ${colors.pink};
  }

  #text {
    max-height: 200px;
    overflow-y: scroll;
  }

  @media (max-width: 700px) {
    margin-top: 60px;
    .content {
      width: 80%;
      padding: 10px;
      border: 10px solid ${colors.pink};
      margin-right: 0;
    }
  }
`;

export const Alt = styled.div`
  flex-direction: row;
  align-content: initial;

  margin-top: 20px;
`;
