import React, { useState } from "react";
import { Container } from "./styles";
import { Alt } from "./styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import colors from "../../utils/colors";
import NavBar from "../../components/NavBar";
import { AiFillPlusCircle, AiFillMinusCircle } from "react-icons/ai";
import IconButton from "@material-ui/core/IconButton";
import Checkbox from "@material-ui/core/Checkbox";
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function Index() {
  // function createCards(){

  //   axios.post('claralab.pythonanywhere.com/api/v1/addcards', {
  //     cards: [{
  //       numalt,
  //       pontos,
  //       tipo: type,
  //       titulo,
  //       respcorretas
  //     }]
  //   }).then(response =>{
  //     console.log(response)
  //   }).catch(error =>{
  //     console.log(error)
  //   })

  //   console.log(`titulo ${titulo}, corpo ${corpo} `)
  // }
  const { user } = useUser();

  function SwitchCase() {
    // const [type, setType] = useState("")
    let type = "";
    let numalt = null;
    let pontos = 0;
    const [corpo, setCorpo] = useState("" || []);
    const [titulo, setTitulo] = useState("");
    const [respcorretas, setRespCorretas] = useState(null || []);

    function createCardsUpload(e) {
      e.preventDefault();
      axios.post("https://claralab.pythonanywhere.com/api/v1/upload", {
        cards: [
          {
            corpo,
            numalt,
            pontos,
            tipo: type,
            titulo,
            respcorretas: respcorretas.sort((a, b) => a - b),
          },
        ],
      });
    }

    function createCards(e) {
      e.preventDefault();
      axios
        .post("https://claralab.pythonanywhere.com/api/v1/addcards", {
          cards: [
            {
              corpo,
              numalt,
              pontos,
              tipo: type,
              titulo,
              respcorretas: respcorretas.sort((a, b) => a - b),
            },
          ],
          deck: {
            horas: 0,
            pontos: 10,
            titulo: "Segundo deck de teste",
          },
        })
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });

      console.log(`titulo ${titulo}, corpo ${corpo} `);
    }

    const [array, setArray] = useState([
      {
        numb: 0,
        placeholder: "",
        // checkbox:setChecked(false)
      },
    ]);
    function handleAddAlt() {
      setArray((old) =>
        old.concat([{ numb: old.length + 1, placeholder: "" }])
      );
      // console.log(array)
    }
    function handleDelAlt(numb) {
      setArray((old) => old.filter((e) => e.numb !== numb));
    }
    const [tipo, setTipo] = useState(0);
    switch (tipo) {
      case 1:
        return (
          <>
            <h1>Tipo 1</h1>
            <div className="content">
              <form onSubmit={createCards}>
                {/* {(type = "psubj")}
                {console.log(type)} */}
                <div>
                  <h2>Escreva aqui a pergunta</h2>
                  <div>
                    <TextField
                      label="Título"
                      placeholder="Escreva aqui..."
                      fullWidth
                      margin="dense"
                      variant="filled"
                      onChange={(e) => setTitulo(e.target.value)}
                      value={titulo}
                    />
                    <TextField
                      id="pergunta"
                      label="Pergunta"
                      placeholder="Escreva aqui..."
                      onChange={(e) => {
                        setCorpo(e.target.value);
                      }}
                      value={corpo}
                      multiline
                      fullWidth
                      variant="outlined"
                    />
                  </div>
                  <Button
                    variant="contained"
                    size="large"
                    startIcon={<SaveIcon />}
                    type="submit"
                  >
                    Salvar card
                  </Button>
                </div>
              </form>
            </div>
          </>
        );
      case 2:
        type = "upload";
        return (
          <>
            <h1>Tipo 2</h1>
            <div className="content">
              <form onSubmit={createCardsUpload}>
                <div>
                  <h2>Escreva aqui a pergunta</h2>
                  <div>
                    <TextField
                      id="pergunta"
                      label="Título"
                      placeholder="Escreva aqui..."
                      multiline
                      onChange={(e) => setTitulo(e.target.value)}
                      fullWidth
                      margin="dense"
                      variant="filled"
                    />
                    <TextField
                      id="pergunta"
                      label="Pergunta e nome do arquivo"
                      placeholder="Escreva aqui..."
                      multiline
                      fullWidth
                      onChange={(e) => setCorpo(e.target.value)}
                      margin="normal"
                      variant="outlined"
                      helperText="O arquivo é necessário."
                    />
                  </div>
                  <Button
                    variant="contained"
                    size="large"
                    startIcon={<SaveIcon />}
                    type="submit"
                  >
                    Salvar card
                  </Button>
                </div>
              </form>
            </div>
          </>
        );
      case 3:
        type = "pobj";
        return (
          <>
            {/* {console.log(type)} */}
            <h1>Tipo 3</h1>
            <div className="content">
              <form onSubmit={createCards}>
                <div>
                  <h2>Escreva aqui as alternativas e marque a(s) correta(s)</h2>
                  <div id="text">
                    <TextField
                      id="pergunta"
                      label="Título"
                      placeholder="Escreva aqui..."
                      multiline
                      fullWidth
                      onChange={(e) => {
                        setTitulo(e.target.value);
                      }}
                      value={titulo}
                      margin="dense"
                      variant="filled"
                    />
                    <ls>
                      {array.map((alt, index) => {
                        return (
                          <Alt>
                            <Checkbox
                              label="Alternativa correta?"
                              className="lados"
                              inputProps={{ "aria-label": "primary checkbox" }}
                              onChange={(e) => {
                                e.target.checked
                                  ? respcorretas.push(index)
                                  : respcorretas.push(null);
                              }}
                            />
                            {/* {console.log(respcorretas.sort())} */}
                            <TextField
                              id="pergunta"
                              label={alt.placeholder}
                              className={`input${index}`}
                              placeholder="Escreva o título aqui..."
                              multiline
                              margin="normal"
                              onChange={(e) => (corpo[index] = e.target.value)}
                              // value={corpo}
                              variant="outlined"
                            />
                            {console.log(corpo)}
                            <IconButton
                              className="lados"
                              onClick={() => handleDelAlt(alt.numb)}
                            >
                              <AiFillMinusCircle />
                            </IconButton>
                          </Alt>
                        );
                      })}
                    </ls>
                  </div>
                  <IconButton onClick={handleAddAlt}>
                    <AiFillPlusCircle />
                  </IconButton>
                  <Button
                    id="save"
                    variant="contained"
                    size="large"
                    startIcon={<SaveIcon />}
                    type="submit"
                  >
                    Salvar card
                  </Button>
                </div>
              </form>
            </div>
          </>
        );
      case 4:
        type = "informacao";
        return (
          <>
            <h1>Tipo 4</h1>
            <div className="content">
              <form onSubmit={createCards}>
                <div>
                  <h2>
                    Escreva aqui a informação que gostaria de transmitir para o
                    usuário
                  </h2>
                  <div id="text">
                    <TextField
                      label="Título"
                      placeholder="Escreva aqui..."
                      multiline
                      fullWidth
                      margin="dense"
                      onChange={(e) => setTitulo(e.target.value)}
                      // value={titulo}
                      variant="filled"
                    />
                    <TextField
                      label="Texto"
                      placeholder="Escreva aqui..."
                      multiline
                      fullWidth
                      onChange={(e) => setCorpo(e.target.value)}
                      value={corpo}
                      margin="normal"
                      variant="outlined"
                    />
                  </div>
                  <Button
                    variant="contained"
                    size="large"
                    startIcon={<SaveIcon />}
                    type="submit"
                  >
                    Salvar card
                  </Button>
                </div>
              </form>
            </div>
          </>
        );
      default:
        return (
          <>
            <h1>Criação de Cards</h1>
            <div className="content">
              <h2>Selecione o tipo de carta</h2>
              <div id="buttons">
                <Button
                  variant="contained"
                  color={colors.green}
                  onClick={() => setTipo(1)}
                >
                  Tipo 1: Questão Aberta
                </Button>
                <Button
                  variant="contained"
                  color={colors.primary}
                  onClick={() => setTipo(2)}
                >
                  Tipo 2: Upload de Arquivo
                </Button>
                <Button
                  variant="contained"
                  color={colors.primary}
                  onClick={() => setTipo(3)}
                >
                  Tipo 3: Questão Múltipla Escolha
                </Button>
                <Button
                  variant="contained"
                  color={colors.primary}
                  onClick={() => setTipo(4)}
                >
                  Tipo 4: Leitura
                </Button>
              </div>
            </div>
          </>
        );
    }
  }
  return (
    <Container>
      <NavBar admin={user?.cargo == "professor"} goBack="/dashboard" />
      <SwitchCase />
    </Container>
  );
}
