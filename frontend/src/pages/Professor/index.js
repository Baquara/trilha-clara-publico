import React, { useState, useEffect } from "react";
import { Container } from "./styles";
// import {Link} from 'react-router-dom';
// import { SectionNav } from './styles';
import { SectionSearch } from "./styles";
import { SectionUser } from "./styles";
import { SectionList } from "./styles";
import { SectionMain } from "./styles";
import { BsPlusCircle, BsPencil, BsCardList } from "react-icons/bs";
// import Button from '../../components/Button';
import Input from "../../components/Input";
import UserInfos from "../PageLogged/User/index";
import NavBar from "../../components/NavBar";
import ProfilePic from "../../assets/img/Ellipse 2.png";
import NameStyle from "../../components/NameStyle";
import { Link } from "react-router-dom";
import { useUser } from "../../contexts/auth";
// import api from '../../services/api';
// import axios from 'axios'

export default function Professor() {
  const { user } = useUser();
  // const [data, setData] = useState();
  // const [courseId, setCourseId] = useState();

  // useEffect(() => {
  //     async function fetchData() {
  //         const result = await axios
  //         .get(`${api}/getmemberse/${id}`)
  //         .then((response) => {
  //             console.log(response.data);
  //             setData(result.data);
  //         })
  //         .catch((err) => console.log(err));
  //     };

  //     fetchData();
  // }, [id]);

  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/dashboard"
        />
      </header>
      <div class="ContainerItems">
        <div className="main-content">
          <SectionUser>
            <UserInfos
              class="UserInfos"
              username={user.nome}
              hours={user.numero_de_horas ?? 0}
              points={user.pontuacao_geral ?? 0}
            />
          </SectionUser>

          <SectionMain>
            {/* <SectionSearch>
              <div class="SearchBox">
                <h2>Participantes</h2>
                <Input
                  placeholder="Pesquisar participante"
                  type="name"
                  bdstyle="solid"
                  onChange={(e) => setSearch(e.target.value)}
                /> 
              </div>
            </SectionSearch> */}
            <NameStyle name="Professor" />

            <SectionList>
              <div class="ListBox">
                <Link to="/create">
                  <div class="ListItems">
                      <h4>Criar Curso</h4>
                    <i>
                      <BsPlusCircle class="ChatIcon" />
                    </i>
                  </div>
                </Link>
              </div>
              <div class="ListBox">
                <Link to="/home">
                  <div class="ListItems">
                      <h4>Gerenciar Cursos</h4>
                    <i>
                      <BsPencil class="ChatIcon" />
                    </i>
                  </div>
                </Link>
              </div>
              <Link to="/addusertrilha">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Adicionar usuário a uma trilha</h4>
                    <i>
                      <BsPlusCircle class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link>
              { /*
              <Link to="/addstage">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Adicionar estágio</h4>
                    <i>
                      <BsPlusCircle class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link>
              <Link to="/addcard">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Adicionar cards</h4>
                    <i>
                      <BsCardList class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link>
              <Link to="/gerenciarcards">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Gerenciar cards</h4>
                    <i>
                      <BsCardList class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link>
              <Link to="/trilha">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Editar trilha</h4>
                    <i>
                      <BsPencil class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link>
              <Link to="/edit">
                <div class="ListBox">
                  <div class="ListItems">
                    <h4>Editar informações da trilha</h4>
                    <i>
                      <BsPencil class="ChatIcon" />
                    </i>
                  </div>
                </div>
              </Link> */}
            </SectionList>
          </SectionMain>
        </div>
      </div>
    </Container>
  );
}
