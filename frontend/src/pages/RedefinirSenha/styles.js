import styled from "styled-components";

export const Container = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative; /*Remove um scroll e a região branca*/
    height: 100vh;
    overflow: auto;
    background: linear-gradient(108.94deg, #3A1244 34.76%, #EE4B70 90.77%), #3A1244;
    color: #C1D731;

    @media screen and ( max-width: 767px ) {
        background: linear-gradient(180deg, #3A1244 31.09%, #EE4B70 100%), #3A1244;
        max-width: 1100px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;

    }
`;

export const SectionNewPass = styled.div `
    display: flex;
    position: absolute;
    justify-content: center;
    flex-direction: column;
    margin-right: auto;
    margin-left: auto;
    font-size: 20px;
    margin-bottom: 100px;
    margin-top: 70px;
    
    @media screen and ( max-width: 767px ) {
        h1 {
            font-size: 30px;
        }
    }
`;

export const SectionInput = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    Input{
        padding-left: 5px;
    }

    Input::placeholder{
        color: #C1D731;
        font-weight: bold;

}

`;

export const SectionButton = styled.div `
    display: flex;
    justify-content: center;
    align-items: center;

    Button {
        font-weight: bold;
        color: #C1D731;
        cursor: pointer;
        font-size: 16px;
    }

    Button:hover {
        transition: 0.3s;
        background: #EE4B70;
        color: #3A1244;
    }
`;

export const SectionImg = styled.div `
    display: flex;
    justify-content: center;
    align-items: center;

    .Group38 {
        width: 720px;
        height: 678.4px;
        padding-bottom: 70px;
    }

    .Group136 {
        display: none;
    }

    @media screen and ( max-width: 767px ) {
        display: flex;
        justify-content: center;
        align-items: center;
        
        .Group38 {
            display: none;
        }

        .Group136 {
            display: flex;
            margin-bottom: 380px;
        }
    }

`;

export const SectionFooter = styled.div `
    display: flex;
    width: 100%;
    height: 50px;
    position: fixed;
    bottom: 0;
    left: 0;
    justify-content: space-evenly;
    
    margin-right: auto;
    margin-left: auto;

    a {
        color: white;
        text-decoration: none;
    }

    a:hover {
        font-weight: bold;
        color:  #C1D731;
        transition: 0.1s;
    }

    @media screen and ( max-width: 767px ) {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin-bottom: 50px;
        
        a {
        color: white;
        text-decoration: none;
        font-weight: lighter;
        font-size: 12px;
        margin: 8px;
    }
 }

`;

