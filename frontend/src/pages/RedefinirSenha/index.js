import React, { useState, useEffect } from "react";
import { Container } from "./styles";
import { SectionNewPass } from "./styles";
import { SectionFooter } from "./styles";
import { SectionButton } from "./styles";
import { SectionInput } from "./styles";
import { SectionImg } from "./styles";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { Link, useHistory, useLocation } from "react-router-dom";
import Group38 from "../../assets/img/Group 38.png";
import Group136 from "../../assets/img/Group 136.png";
import api from "../../services/api";
import { MdCheck } from "react-icons/md";
import Axios from "axios";
import { useAuth } from "../../contexts/auth";

export default function RedefinirSenha() {
  const [novasenha1, setNovasenha1] = useState("");
  const [novasenha2, setNovasenha2] = useState("");
  const history = useHistory();
  const location = useLocation();
  const token = location.search.replace("?access_token=", "");
  const { auth } = useAuth();
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  console.log(location.search);
  console.log(token);
  async function handleSubmitPassword() {
    console.log(auth)
    // e.preventDefault();
    Axios.post(
      `http://claralab.pythonanywhere.com/api/v1/asenha?access_token=${auth.access_token}`,
      {
        novasenha1,
        novasenha2,
      },
      config
    ).then(() => history.push("/dashboard"));
  }

  function checkPass(e) {
    e.preventDefault();
    novasenha1 === novasenha2
      ? handleSubmitPassword()
      : alert("Senhas divergem");
  }
  return (
    <div>
      <Container>
        <SectionNewPass>
          <h1>Redefinir Senha</h1>

          <form onSubmit={checkPass}>
            <SectionInput>
              <Input
                placeholder="Insira nova senha"
                type="password"
                color="transparent"
                txtcolor="#C1D731"
                bdstyle="solid"
                value={novasenha1}
                onChange={(e) => {
                  setNovasenha1(e.target.value);
                }}
              />

              <Input
                placeholder="Confirme a senha"
                type="password"
                color="transparent"
                txtcolor="#C1D731"
                bdstyle="solid"
                value={novasenha2}
                onChange={(e) => {
                  setNovasenha2(e.target.value);
                }}
              />
            </SectionInput>

            <SectionButton>
              <Button
                class="button"
                name="Redefinir"
                color="#3A1244"
                txtcolor="#fff"
                type="submit"
              />
            </SectionButton>
          </form>
        </SectionNewPass>

        <SectionImg>
          <img class="Group38" name="Group38" src={Group38} alt="" />
          <img class="Group136" name="Group136" src={Group136} alt="" />
        </SectionImg>

          {/* <SectionFooter>
          <a href="#">Sobre a CLARA IDEA</a>
          <a href="#">Parceiros e Depoimentos</a>
          <a href="#">Perguntas Frequentes(FAQ)</a>
        </SectionFooter> */}
      </Container>
    </div>
  );
}
