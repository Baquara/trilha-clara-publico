import React from "react";
import { AiFillClockCircle, AiFillPlusCircle } from "react-icons/ai";
import UserStyled from "./styles";
import { ProgressContainer, Progress } from "../styles";
import ProfilePic from "../../../assets/img/Ellipse 2.png";
import Button from "../../../components/Button";
import colors from "../../../utils/colors";
import { Link } from "react-router-dom";

const percentage = 50;

export default function UserInfos(props) {
  return (
    <UserStyled>
      <img src={ProfilePic} alt="" />

      <h4>
        Capacitação em <br></br>
        Gestão Corporativa
      </h4>
      <h3>{props.title}</h3>
      {/* <Link to="/stages">
        <Button name="Etapas" txtcolor={colors.primary} color={colors.aqua} />
      </Link>
      <Link to="/addstage">
        <Button
          name="Adicionar Etapa"
          txtcolor={colors.primary}
          color={colors.green}
        />
      </Link> */}
      <p>
        <AiFillClockCircle /> {props.hours} <AiFillPlusCircle />
        {props.points}/{props.maxpoints} pontos
      </p>
      <ProgressContainer>
        <div className="container">{props.info}</div>

        <Progress color="secondary" variant="determinate" value={percentage} />
      </ProgressContainer>
    </UserStyled>
  );
}
