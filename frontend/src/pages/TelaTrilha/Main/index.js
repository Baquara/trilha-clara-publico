import React, { useState, useEffect } from "react";
import colors from "../../../utils/colors";
import { Diagram, Stack } from "grommet";
import { MainStyled, Buttons } from "./styles";
import { HiLightBulb } from "react-icons/hi";
import { GiFlyingFlag } from "react-icons/gi";
import axios from "axios";
import { Button, Menu, MenuItem } from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";

export default function MainInfos() {
  const array = [
    {
      id: 1,
      completed: true,
    },
    {
      id: 2,
      completed: true,
    },
    {
      id: 3,
      completed: true,
    },
    {
      id: 4,
      completed: false,
    },
    {
      id: 5,
      completed: false,
    },
    {
      id: 6,
      completed: false,
    },
    {
      id: 7,
      completed: false,
    },
    {
      id: 8,
      completed: false,
    },
    {
      id: 9,
      completed: false,
    },
    {
      id: 10,
      completed: false,
    },
    {
      id: 11,
      completed: false,
    },
    {
      id: 12,
      completed: false,
    },
    {
      id: 13,
      completed: false,
    },
    {
      id: 14,
      completed: false,
    },
    {
      id: 15,
      completed: false,
    },
    {
      id: 16,
      completed: false,
    },
    {
      id: 17,
      completed: false,
    },
    {
      id: 18,
      completed: false,
    },
    {
      id: 19,
      completed: false,
    },
    {
      id: 20,
      completed: false,
    },
    {
      id: 21,
      completed: false,
    },
    {
      id: 22,
      completed: false,
    },
    {
      id: 23,
      completed: false,
    },
    {
      id: 24,
      completed: false,
    },
    {
      id: 25,
      completed: false,
    },
  ];

  const [fase, setFase] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [render, setRender] = useState(false)
  const history = useHistory();

  const handleClick = (event) => {
    history.push("/cards");
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    axios
      .get("https://claralab.pythonanywhere.com/api/v1/tabuleiro?id=1")
      .then((response) => {
        setFase(response.data.conteudo);
        console.log(response.data.conteudo)
        setRender(true)
    });
  }, []);

  let connect = [];
  for (let i = 0; i < fase.length; i++) {
    // if (fase[i].completed) {
    //   connect.push({
    //     fromTarget: `${i}`,
    //     toTarget: `${i + 1}`,
    //     thickness: "xsmall",
    //     color: colors.green,
    //     type: "rectilinear",
    // });

    // // fase.push(fase[i].id + 1);
    // } else 
    if (i === fase.length - 1) {
      connect.push(
        {
          fromTarget: `${i}`,
          toTarget: `${i + 1}`,
          thickness: "xsmall",
          color: colors.aqua,
          type: "rectilinear",
        },
        {
          fromTarget: `${i + 1}`,
          toTarget: `${i + 2}`,
          thickness: "xsmall",
          color: colors.aqua,
          type: "rectilinear",
        }
      );
    } else {
      connect.push({
        fromTarget: `${i}`,
        toTarget: `${i + 1}`,
        thickness: "xsmall",
        color: colors.aqua,
        type: "rectilinear",
      });
    }
  }

  // console.log(fase.length)
  console.log(connect);

  return (
    render && (<MainStyled>
      <Stack guidingChild={1}>
        <Diagram connections={connect} />
        <div>
          {/* <Buttons
              disabled
              margin="small"
              pad="medium"
              background="light-4"
            /> */}
          {fase.map((count, index) => {
            if (index+1 === 1) {
              return (
                <>
                  <GiFlyingFlag className="flag" />
                  <Buttons
                    onClick={handleClick}
                    // style={{marginLeft: '160px'}}
                    className="settings"
                    primary
                    id={index+1}
                    margin="small"
                    pad="medium"
                    style={{ border: `4px solid ${colors.pink}` }}
                    color={colors.aqua}
                    background={colors.aqua}
                  >
                    <h2>{count.id}</h2>
                  </Buttons>
                </>
              );
            }
            if (index+1 === 1 && count.completed) {
              return (
                <>
                  <GiFlyingFlag className="flag" />
                  <Buttons
                    onClick={handleClick}
                    className="settings"
                    primary
                    id={index+1}
                    margin="small"
                    pad="medium"
                    color={colors.green}
                    background={colors.aqua}
                  >
                    <h2>{index+1}</h2>
                  </Buttons>
                </>
              );
            }
            if (index + 1 === 2) {
              if (count.completed) {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.green}
                      background={colors.green}
                    >
                      <h2>{count.id}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              } else if (index + 1 === fase[fase.length - 1]) {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.aqua}
                      style={{ border: `4px solid ${colors.pink}` }}
                      background={colors.aqua}
                    >
                      <h2>{index+1}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              } else {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.aqua}
                      background={colors.aqua}
                    >
                      <h2>{count.id}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              }
            }

            if ((index + 1) % 3 === 2) {
              if (count.completed) {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.green}
                      background={colors.green}
                    >
                      <h2>{count.id}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              } else if (index + 1 === fase[fase.length - 1]) {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.aqua}
                      style={{ border: `4px solid ${colors.pink}` }}
                      background={colors.aqua}
                    >
                      <h2>{index+1}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              } else {
                return (
                  <>
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+1}
                      margin="small"
                      pad="medium"
                      color={colors.aqua}
                      background={colors.aqua}
                    >
                      <h2>{index+1}</h2>
                    </Buttons>
                    <br />
                  </>
                );
              }
            } else if (count.completed) {
              return (
                <Buttons
                  onClick={handleClick}
                  primary
                  id={index+1}
                  margin="small"
                  pad="medium"
                  color={colors.green}
                  background={colors.green}
                >
                  <h2>{index+1}</h2>
                </Buttons>
              );
            } else if (index + 1 === fase[fase.length - 1]) {
              return (
                <>
                  <Buttons
                    onClick={handleClick}
                    primary
                    id={index+1}
                    margin="small"
                    pad="medium"
                    color={colors.aqua}
                    style={{ border: `4px solid ${colors.pink}` }}
                    background={colors.aqua}
                  >
                    <h2>{index+1}</h2>
                  </Buttons>
                </>
              );
            }
            if (index === fase.length - 1) {
              return (
                <>
                  <Buttons
                    onClick={handleClick}
                    primary
                    id={index+1}
                    margin="small"
                    pad="medium"
                    color={colors.aqua}
                    background={colors.aqua}
                  >
                    <h2>{index+1}</h2>
                  </Buttons>

                  <br />

                  <div className="flagFinal">
                    <GiFlyingFlag
                      style={{ color: colors.green }}
                      className="flag"
                    />
                    <Buttons
                      onClick={handleClick}
                      primary
                      id={index+2}
                      className="buttonFinal"
                      margin="small"
                      pad="medium"
                      color={colors.aqua}
                      style={{ border: `4px solid ${colors.pink}` }}
                      background={colors.aqua}
                    >
                      <h2>
                        <HiLightBulb className="lamp" />
                      </h2>
                    </Buttons>
                  </div>
                </>
              );
            } else {
              return (
                <Buttons
                  onClick={handleClick}
                  primary
                  id={index+1}
                  margin="small"
                  pad="medium"
                  color={colors.aqua}
                  background={colors.aqua}
                >
                  <h2>{index+1}</h2>
                </Buttons>
              );
            }
          })}
        </div>
      </Stack>
      {/* <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <Link to="/gerenciarcards">Gerenciar Cards</Link>
        </MenuItem>
        <MenuItem onClick={handleClose}>Excluir Etapa</MenuItem>
      </Menu> */}
    </MainStyled>
  ));
}
