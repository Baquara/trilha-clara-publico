import React, { useState, useEffect } from "react";
import { Container } from "./styles";
// import {Link} from 'react-router-dom';
// import { SectionNav } from './styles';
import { SectionSearch } from "./styles";
import { SectionUser } from "./styles";
import { SectionList } from "./styles";
import { SectionMain } from "./styles";
import { BsFillChatFill } from "react-icons/bs";
// import Button from '../../components/Button';
import Input from "../../components/Input";
import UserInfos from "../PageLogged/User/index";
import NavBar from "../../components/NavBar";
import ProfilePic from "../../assets/img/Ellipse 2.png";
import NameStyle from "../../components/NameStyle";
// import api from '../../services/api';
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function Participantes() {
  const { user } = useUser();
  const [search, setSearch] = useState("");
  const [data, setData] = useState([]);
  // const [courseId, setCourseId] = useState();

  useEffect(() => {
    async function fetchData() {
      const result = await axios
        .get(`https://claralab.pythonanywhere.com/api/v1/getuser`)
        .then((response) => {
          console.log(response.data);
          setData(response.data);
        })
        .catch((err) => console.log(err));
    }

    fetchData();
  }, []);

  //     fetchData();
  // }, [id]);

  return (
    <Container>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/course"
        />
      </header>
      <div class="ContainerItems">
        <div className="main-content">
          <SectionUser>
            <UserInfos
              class="UserInfos"
              username={user.nome}
              hours={user.numero_de_horas ?? 0}
              points={user.pontuacao_geral ?? 0}
            />
          </SectionUser>
          <SectionMain>
            {/* <SectionSearch>
              <div class="SearchBox">
                <h2>Participantes</h2>
                <Input
                  placeholder="Pesquisar participante"
                  type="name"
                  bdstyle="solid"
                  onChange={(e) => setSearch(e.target.value)}
                /> 
              </div>
            </SectionSearch> */}
            <NameStyle name="Participantes" />

            <SectionList>
              {data.map((members) => {
                return (
                  <div class="ListBox">
                    {console.log("engenharia")}
                    <div class="ListItems">
                      {/* <img src={members.image}/> */}
                      <img src={ProfilePic} alt="" />
                      <h4>{members.nome}</h4>
                      <p>{members.pontuacao_geral} pontos</p>
                      {/* <i>
                        <BsFillChatFill class="ChatIcon" />
                      </i> */}
                    </div>
                  </div>
                );
              })}
            </SectionList>
          </SectionMain>
        </div>
      </div>
    </Container>
  );
}
