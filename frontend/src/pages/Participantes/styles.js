import styled from "styled-components";
import colors from "../../utils/colors";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: auto;
  overflow-x: hidden;
  background: ${colors.primary};

  a {
    text-decoration: none;
  }

  .main-content {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas: "user main";
    margin: 50px auto;
    max-width: 1100px;
  }

  @media screen and (max-width: 767px) {
    background: ${colors.primary};
    overflow: auto;
    display: flex;
    flex-direction: column;

    .main-content {
      margin: 0;
      grid-template-columns: 1fr;
      grid-template-areas: "main";
    }
  }
`;

export const SectionUser = styled.div`
  grid-area: user;
  display: flex;
  width: fit-content;

  @media (max-width: 500px) {
    display: none;
  }
`;

export const SectionMain = styled.div`
  grid-area: main;
  display: flex;
  flex-direction: column;
  width: 90%;

  @media screen and (max-width: 767px) {
    display: flex;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const SectionSearch = styled.div`
  display: flex;
  height: 135px;
  padding-top: 50px;

  .SearchBox {
    display: flex;
    width: 100%;
    height: 120px;
    background-color: ${colors.green};
    border-radius: 8px;
    justify-content: space-between;
    align-items: center;
  }

  h2 {
    color: ${colors.primary};
    font-weight: bold;
    font-size: 28px;
    margin-left: 30px;
    padding-bottom: 10px;
  }

  Input {
    margin-right: 30px;
  }

  Input::-webkit-input-placeholder {
    text-align: center;
  }

  @media screen and (max-width: 767px) {
    display: none;
  }
`;

export const SectionList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  .ListBox {
    display: flex;
    width: 100%;
    background-color: ${colors.gray};
    border-radius: 8px;
    flex-direction: column;
    margin-top: 10px;
  }

  .ListItems {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 20px;

    img {
      width: 60px;
      height: 60px;
      border-radius: 50%;
    }

    h4 {
      font-size: 22px;
      font-weight: bold;
    }

    p {
      color: ${colors.primary};
      font-size: 22px;
      font-weight: bold;
    }

    i {
      color: ${colors.primary};
      padding-right: 40px;

      .ChatIcon {
        width: 25px;
        height: 23.33px;
      }

      .ChatIcon:hover {
        color: ${colors.pink};
        transition: 0.3s;
      }
    }
  }

  @media screen and (max-width: 767px) {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 0px;

    .ListBox {
      z-index: 2;
      display: flex;
    }

    .ListItems {
      padding: 10px;
      img {
        width: 30px;
        height: 30px;
        border-radius: 50%;
      }

      h4 {
        font-size: 18px;
      }

      p {
        font-size: 18px;
      }
    }
  }
`;
