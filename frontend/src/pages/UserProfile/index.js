import React, { useState, useEffect } from "react";
import { Container } from "./styles";
import Button from "../../components/Button";
import NavBar from "../../components/NavBar";
import { Link } from "react-router-dom";
import colors from "../../utils/colors";
import Pic from "../../assets/img/Ellipse 2.png";
import { AiFillClockCircle } from "react-icons/ai";
import { MdPerson } from "react-icons/md";
import { IconContext } from "react-icons";
import { BsOctagonFill } from "react-icons/bs";
import { AiFillMessage } from "react-icons/ai";
import api from "../../services/api";
import axios from "axios";
import { useUser } from "../../contexts/auth";

export default function UserProfile() {
  const [data, setData] = useState();
  const [id, setId] = useState();
  const { user } = useUser();

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios
        .get(`${api}/getprofile/${id}`)
        .then((response) => {
          console.log(response.data);
          setData(result.data);
        })
        .catch((err) => console.log(err));
    };

    fetchData();
  }, [id]);

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack="/home"
      />
      <div id="content">
        <img id="pfp" src={Pic} />
        <h2>{user?.username}</h2>
        <p id="userRole">{data?.cargo}</p>
        <p>
          <div id="textEdit">
            <MdPerson />
            <Link to="/edituser">Editar informações</Link>
          </div>
          {/* <div>
            <button className="talk">
              <IconContext.Provider value={{ className: "talk" }}>
                <AiFillMessage />
              </IconContext.Provider>
            </button>
          </div> */}
        </p>
      </div>
    </Container>
  );
}
