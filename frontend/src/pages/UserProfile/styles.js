import styled from "styled-components";
import circlesD from "../../assets/img/Group 37.svg";
import circlesM from "../../assets/img/Group 136.png";
import colors from "../../utils/colors";

export const Container = styled.div`
  height: 100vh;
  background-position: center;
  background-image: url("${circlesD}"),
    linear-gradient(108.94deg, ${colors.primary} 34.76%, ${colors.pink} 90.77%);
  background-repeat: no-repeat;
  background-size: cover;
  color: #fff;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  #content {
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;

    Button {
      cursor: pointer;
      font-size: 16px;
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      margin-top: 20px;
    }
    Button #trilha:hover {
      transition: 0.3s;
      background: ${colors.lightyellow};
      color: ${colors.primary};
    }
    a {
      color: white;
      text-decoration: none;
      margin: 10px;
    }
    p {
      display: flex;
      flex-direction: row;
    }
    p #textEdit {
      justify-content: center;
    }
    #pfp {
      margin-top: 5px;
      width: 200px;
    }
    #userRole {
      margin: 10px;
      margin-bottom: 50px;
    }
    #info {
      color: ${colors.green};
    }
    p a:hover {
      font-weight: bold;
      color: ${colors.green};
      transition: 0.1s;
    }
    .talk {
      color: ${colors.green};
      font-size: 30px;
      border: none;
      border-radius: 60px;
      background: ${colors.purple};
      padding: 5px;
      padding-bottom: 2px;
      cursor: pointer;
    }
    @media (max-width: 700px) {
      p #textEdit {
        justify-content: center;
      }
      #pfp {
        margin-top: 15%;
        width: 150px;
      }
    }
  }
`;
