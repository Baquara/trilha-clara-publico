import styled from 'styled-components'
import colors from '../../../utils/colors'

const FooterStyled = styled.div`

   
    grid-area: footer;
    position: fixed;
    margin-top: 70vh;
    margin-left: 60vw;

    button{
        border: none;
        border-radius: 60px;
        background: ${colors.purple};
        padding: 10px;
        cursor: pointer;
        margin-right: 10px;
    }

    button .talk{
        color: ${colors.green};
        font-size: 30px;
    }

    button .plus{
        color: ${colors.green};
        font-size: 30px;
    }

    @media(max-width: 700px){
        margin-top: 70vh;
    }
`

export default FooterStyled