import React from 'react'
import { AiFillClockCircle, AiFillPlusCircle } from 'react-icons/ai'
import UserStyled from './style'
import ProfilePic from "../../../assets/img/Ellipse 2.png";

export default function UserInfos(props){
    return(
        <UserStyled>
            <img src={ProfilePic} alt=""/>
            <h3>{props.username}</h3>
            <h4>Tutor</h4>

            <p>
                <AiFillClockCircle/>  {props.hours}
            </p>

            <p>
                <AiFillPlusCircle/>  {props.points}
            </p>
        </UserStyled>
    )
}
