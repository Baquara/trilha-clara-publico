import styled from "styled-components";
import colors from "../../../utils/colors";

const UserStyled = styled.div`
  grid-area: user;
  text-align: center;
  position: fixed;

  img {
    height: 120px;
    border-radius: 50%;
    margin-top: 10px;
    margin-bottom: 20px;
  }

  h3,
  h4 {
    color: ${colors.lightgray};
    margin: 0;
    font-weight: bold;
    font-size: 20px;
    text-align: center;
  }

  h4 {
    margin-bottom: 20px;
  }

  p {
    color: ${colors.green};
    margin: 0px;
    font-weight: bold;
    font-size: 15px;
  }

  @media (max-width: 700px) {
    display: none;
  }
`;

export default UserStyled;
