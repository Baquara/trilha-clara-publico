import styled from "styled-components";
import colors from "../../utils/colors";

export const LoggedContainer = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0 auto;
  overflow: auto;
  overflow-x: hidden;

  background: #3a1244;

  .page-logged-container {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas:
      "user main"
      "footer footer";
    margin: 50px auto;
    max-width: 1100px;
  }

  header {
    grid-area: topo;
    width: 100vw;
    /* height: 70px; */
    background: ${colors.primary};
  }

  .back {
    justify-content: space-between;
  }

  .back a {
    display: flex;
    font-size: 30px;
    margin: 0 10px;
    float: right;
    margin-left: 30px;
    color: ${colors.green};
  }

  @media (max-width: 700px) {
    /* background: rgb(193,215,49);
        background: linear-gradient(180deg, rgba(193,215,49,1) 50%, rgba(58,18,68,1) 50%); */
    /* background: 50vh; */

    .page-logged-container {
      display: grid;
      grid-template-columns: 1fr 3fr;
      grid-template-areas:
        "user user"
        "main main"
        "footer footer";
      margin: 0 auto;
      max-width: 600px;
    }
  }
`;
