import React, { useState } from "react";
import { AiFillClockCircle, AiFillPlusCircle } from "react-icons/ai";
import { MainStyled, CardsStyled } from "./styles";
import { useEffect } from "react";
import NameStyled from "../../../components/NameStyle";
import colors from "../../../utils/colors";

export default function MainInfos(props) {
  const array = [
    {
      id: 1,
      title: "Introdução a inovação",
      image:
        "https://www.aceguarulhos.com.br/blog/wp-content/uploads/2017/12/rotinas-administrativas.jpg",
      time: 5,
      points: 20,
      progress: 100,
      infos: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      ],
    },
    {
      id: 2,
      title: "Introdução ao sistemas de Inovação do Contratante",
      image:
        "https://www.casadaqualidade.com.br/wp-content/uploads/2018/11/fachada-1.png",
      time: 1,
      points: 45,
      progress: 50,
      infos: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      ],
    },

    {
      id: 3,
      title: "Capacitação em React",
      image:
        "https://portal.conlicitacao.com.br/wp-content/uploads//2019/08/rotina-administrativa-eficiente-800x534.jpg",
      time: 10,
      points: 80,
      progress: 87,
      infos: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      ],
    },
    {
      id: 4,
      title: "Introdução a inovação",
      image:
        "https://www.aceguarulhos.com.br/blog/wp-content/uploads/2017/12/rotinas-administrativas.jpg",
      time: 5,
      points: 20,
      progress: 100,
      infos: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      ],
    },
  ];

  function handlediv(index) {
    let display = document.getElementById(`display${index}`).style.display;

    if (display === "block") {
      document.getElementById(`display${index}`).style.display = "none";
    } else {
      document.getElementById(`display${index}`).style.display = "block";
    }
  }

  function mudaCor(id) {
    let color = document.getElementById(`colors${id}`).style.background;

    if (color === "") {
      document.getElementById(`colors${id}`).style.background = colors.green;
    } else {
      document.getElementById(`colors${id}`).style.background = "";
    }
  }

  return (
    <MainStyled>
      <div id="green">
        <NameStyled name="Introdução ao Design Thinking" />
      </div>

      {array.map((card, index) => {
        return (
          <CardsStyled onClick={() => handlediv(index)}>
            <div className="top">
              <img src={card.image} alt="" />
              <div className="id">
                <b>{card.id}</b>
              </div>
            </div>

            <div className="content">
              <b>{card.title}</b>

              <p>
                <b>
                  <AiFillClockCircle /> {card.time} horas <AiFillPlusCircle />{" "}
                  {card.points} pontos
                </b>
              </p>
            </div>

            {/* {display == true ?  */}
            <div style={{ display: "none" }} id={`display${index}`}>
              <div className="phases">
                <div className="infos">
                  {card.infos.map((info, index) => {
                    return (
                      <div className="container">
                        <div className="infos">
                          <div
                            onClick={() => mudaCor(index)}
                            id={`colors${index}`}
                            className="check"
                          />
                          <span>{info}</span>
                          {index !== card.infos.length - 1 ? <hr /> : ""}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="progress">
                {card.progress !== 100 ? (
                  <div
                    style={{ width: `${card.progress}%` }}
                    className="incompleted-progress"
                  >
                    <strong> {card.progress} % </strong>
                  </div>
                ) : (
                  <div
                    style={{ width: `${card.progress}%` }}
                    className="completed-progress"
                  >
                    <strong> {card.progress} % </strong>
                  </div>
                )}
              </div>
            </div>
            {/* }   */}
          </CardsStyled>
        );
      })}
    </MainStyled>
  );
}
