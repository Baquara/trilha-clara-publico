import styled from "styled-components";
import colors from "../../../utils/colors";

export const MainStyled = styled.div`
  grid-area: main;
  margin-bottom: 20px;

  /* @media (max-width: 700px) {
    #green {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      padding: 5px;
      height: 250px;
      z-index: -100;
      background: ${colors.green};
      border-radius: 0px 0px 10px 10px;
    }
  } */
`;

export const CardsStyled = styled.div`
  width: 100%;
  background: ${colors.lightgray};
  margin: 0 auto;
  border-radius: 5px;
  padding-bottom: 20px;
  margin-bottom: 20px;
  cursor: pointer;

  .top {
    display: flex;
    justify-content: space-between;
    margin: 0;
    height: 50%;
    display: block;
  }

  .id {
    width: 10px;
    background: ${colors.aqua};
    border-radius: 100%;
    padding: 5px 10px;
    font-size: 17px;
    color: ${colors.lightgray};
    position: relative;
    top: -60px;
    margin-left: 10px;
  }

  .top img {
    width: 100%;
    height: 70px;
    border-radius: 5px 5px 0 0;
    object-fit: cover;
  }

  .content {
    display: flex;
    justify-content: space-between;
    margin-top: -20px;
    padding: 0 10px;
  }

  .content p {
    color: ${colors.primary};
    margin-top: 0;
  }

  .content b {
    cursor: pointer;
  }

  .infos {
    box-sizing: border-box;
  }

  .infos p {
    color: ${colors.gray2};
  }

  .infos hr {
    margin-top: -2px;
    margin-left: 5px;
    width: 1px;
    height: 100%;
    float: left;
    background: ${colors.green};
    border: dotted 2px ${colors.green};
  }

  .progress {
    justify-content: center;
    text-align: center;
    margin: 0 auto;
    margin-top: 20px;
    width: 95%;
    border-radius: 20px;
    background: ${colors.gray};
  }

  .completed-progress {
    background: ${colors.green};
    border-radius: 20px;
    text-align: center;
  }

  .incompleted-progress {
    background: ${colors.pink};
    border-radius: 20px;
    text-align: center;
  }

  .container {
    display: flex;
    margin-left: 20px;
    justify-content: space-between;
    margin-top: 10px;
  }

  .check {
    border-radius: 50%;
    height: 10px;
    width: 10px;
    border: black;
    background-color: ${colors.lightgray};
    border: 3px solid ${colors.green};
    display: flex;
    margin-bottom: -20px;
    cursor: pointer;
  }

  .infos {
    margin-top: 8px;
  }

  .infos span {
    margin-left: 30px;
    display: flex;
    text-align: left;
    justify-items: center;
    color: ${colors.gray2};
  }

  @media (max-width: 500px) {
    width: 90%;

    header {
      margin: 0;
    }

    .content {
      display: block;
    }

    .content p {
      margin-top: 10px;
      display: none;
    }

    .content b {
      background: none;
    }

    .check {
      border: 4px solid ${colors.green};
    }

    .infos hr {
      margin-top: -6.8%;
      margin-left: 5px;
      width: 1px;
      height: calc(100% + 10px);
      float: left;
      margin-left: 7px;
      background: ${colors.green};
      border: solid 1.5px ${colors.green};
    }
  }
`;
