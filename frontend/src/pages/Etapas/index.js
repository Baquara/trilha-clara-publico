import React from "react";
import { LoggedContainer } from "./styles";

import Footer from "./Footer";
import UserInfos from "./User";
import MainInfos from "./Main";
import NavBar from "../../components/NavBar";
import { useParams } from "react-router-dom";
import { useUser } from "../../contexts/auth";

export default function Etapas() {
  const { user } = useUser();
  const { title } = useParams();

  return (
    <LoggedContainer>
      <header>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/trilha"
        />
      </header>

      <div className="page-logged-container">
        <UserInfos username="Wxavier08" hours="6 horas" points="50 pontos" />
        <MainInfos title={title} />
        <Footer />
      </div>
    </LoggedContainer>
  );
}
