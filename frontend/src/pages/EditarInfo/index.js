import React, { Component, useState } from "react";
import { Container } from "./styles";
import Button from "../../components/Button";
import NavBar from "../../components/NavBar";
import InputTC from "../../components/InputTC";
import colors from "../../utils/colors";
import { Grid } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import { useUser } from "../../contexts/auth";

export default function EditarInfo() {
  const [titulo, setTitulo] = useState("");
  const [horas, setHoras] = useState("");
  const [pontos, setPontos] = useState("");
  const [descricao, setdescricao] = useState("");
  const history = useHistory();
  const { courseId } = useParams();
  const { user } = useUser();

  function handleSubmit(event) {
    //   event.preventDefault();
    //   api
    //     .post(`${api}/editinfo/${id}`, {
    //       titulo,
    //       horas,
    //       pontos,
    //       descricao,
    //
    //     })
    //     .then(() => {
    //       alert("editadocomsucesso")
    //       history.push("/myprofile")
    //     });
  }

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack={`/edittrilha/${courseId}`}
      />
      <Grid
        container
        className="mainGrid"
        justify="center"
        alignContent="center"
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignContent="center"
        >
          <h2>Editar Informações</h2>
          <form onSubmit={handleSubmit}>
            <InputTC
              value={titulo}
              onChange={(e) => setTitulo(e.target.value)}
              className="mainInput"
              label="Título"
            />
            <InputTC
              value={horas}
              onChange={(e) => setHoras(e.target.value)}
              className="mainInput"
              label="Horas"
            />
            <InputTC
              value={pontos}
              onChange={(e) => setPontos(e.target.value)}
              className="mainInput"
              label="Pontos"
            />
            <InputTC
              value={descricao}
              onChange={(e) => setdescricao(e.target.value)}
              className="mainInput largeInput"
              label="Descrição"
              multiline={true}
              rows={4}
            />
            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
              onClick=""
            />
          </form>
        </Grid>
      </Grid>
    </Container>
  );
}
