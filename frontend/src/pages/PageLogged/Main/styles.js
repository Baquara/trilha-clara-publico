import styled from "styled-components";
import colors from "../../../utils/colors";

export const MainStyled = styled.div`
  grid-area: main;
  margin-bottom: 20px;

  a {
    text-decoration: none;
    color: black;
  }
`;
export const CardsStyled = styled.div`
  width: 90%;
  background: ${colors.lightgray};
  margin: 0 auto;
  border-radius: 5px;
  padding: 15px;
  margin-bottom: 20px;

  .top {
    display: flex;
    justify-content: space-between;
    margin: 0;
  }

  .top p {
    margin: 0;
    color: ${colors.primary};
    font-weight: bold;
  }

  .top h3 {
    margin: 0;
    color: ${colors.primary};
  }

  .progress {
    justify-content: center;
    text-align: center;
    margin: 0 auto;
    margin-top: 30px;
    width: 100%;
    border-radius: 20px;
    background: ${colors.gray};
  }

  @media (max-width: 700px) {
    width: 80%;

    .top p {
      display: none;
    }
  }
`;
