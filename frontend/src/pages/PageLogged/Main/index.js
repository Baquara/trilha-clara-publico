import React, { useEffect, useState } from "react";
import { AiFillClockCircle, AiFillPlusCircle } from "react-icons/ai";
import { MainStyled, CardsStyled } from "./styles";
import colors from "../../../utils/colors";
import { Link } from "react-router-dom";

export default function MainInfos(props) {
  return (
    <MainStyled>
      {props.cursos === null && (
        <CardsStyled>
          <h3>Você ainda não tem cursos</h3>
        </CardsStyled>
      )}
      {(props.cursos ?? []).map((card) => {
        return (
          <Link
            to={{
              pathname: `/course/${card.id_curso}`,
              state: { curso: card },
            }}
          >
            <CardsStyled>
              <div className="top">
                <h3>{card.nome}</h3>

                <p>
                  <AiFillClockCircle /> {card.horas} horas <AiFillPlusCircle />{" "}
                  {card.pontos} pontos
                </p>
              </div>
              {card.empresa}

              <div className="progress">
                {card.pct_completado !== 100 ? (
                  <div
                    style={{
                      background: colors.pink,
                      width: `${card.pct_completado}%`,
                      borderRadius: "20px",
                      textAlign: "center",
                    }}
                  >
                    {card.pct_completado !== 0 ? (
                      <strong> {card.pct_completado} % </strong>
                    ) : (
                      <strong>0%</strong>
                    )}
                  </div>
                ) : (
                  <div
                    style={{
                      background: colors.green,
                      width: `${card.progress}%`,
                      borderRadius: "20px",
                      textAlign: "center",
                    }}
                  >
                    <strong> {card.progress} % </strong>
                  </div>
                )}
              </div>
            </CardsStyled>
          </Link>
        );
      })}
    </MainStyled>
  );
}
