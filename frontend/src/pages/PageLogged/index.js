import React, { useEffect, useState } from "react";
import { LoggedContainer } from "./styles";
import { MdSettings, MdKeyboardBackspace } from "react-icons/md";
import ProfilePic from "../../assets/img/Ellipse 2.png";
import Footer from "./Footer";
import UserInfos from "./User";
import MainInfos from "./Main";
import { useHistory } from "react-router-dom";
import { Link, useParams } from "react-router-dom";
import WaitingPage from "../WaitingPage";
import NavBar from "../../components/NavBar";
// import api from '../../services/api'
import axios from "axios";
import { useAuth, useUser } from "../../contexts/auth";

export default function PageLogged(props) {
  const { user } = useUser();
  const { auth } = useAuth();
  const [data, setData] = useState("");

  const api = axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  useEffect(() => {
    api.get(`/api/v1/gethome?id=${auth.id}`).then((response) => {
      setData(response.data);
    });
  }, []);

  return data ? (
    <LoggedContainer>
      {/* <header>
                <div>
                    <p className='name-user'>
                        Bem vindo, <b>{data.dadosuser.usuario}</b>                       
                        <img src={ProfilePic} alt=""/>
                    </p>

                
                    <p className='back'>
                        <Link to='/login'><MdKeyboardBackspace/></Link> 
                        <Link><MdSettings/></Link>
                    </p>
                </div>
            </header> */}
      <NavBar admin={user?.cargo == "professor"} logged={true} goBack="/home" />

      <div className="page-logged-container">
        <UserInfos
          username={data.dadosuser.nome}
          hours={data.dadosuser.numero_de_horas ?? 0}
          points={data.dadosuser.pontuacao_geral ?? 0}
          cargo={data.dadosuser?.cargo}
        />
        <MainInfos cursos={data.cursos} />
        <Footer />
      </div>
    </LoggedContainer>
  ) : (
    <WaitingPage />
  );
}
