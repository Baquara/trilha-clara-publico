import React from 'react'
import { AiFillClockCircle, AiFillPlusCircle } from 'react-icons/ai'
import UserStyled from './style'
import ProfilePic from "../../../assets/img/Ellipse 2.png";


export default function UserInfos(props){
    return(
        <UserStyled>
            <img src={ProfilePic} alt=""/>
            <h3>{props.username}</h3>
            <h4>{props.cargo}</h4>

            <p>
                <AiFillClockCircle/>  {`${props.hours} horas`}
            </p>

            <p>
                <AiFillPlusCircle/>  {`${props.points} pontos`}
            </p>
        </UserStyled>
    )
}
