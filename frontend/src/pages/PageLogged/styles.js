import styled from "styled-components";
import colors from "../../utils/colors";

export const LoggedContainer = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0 auto;
  overflow: auto;
  overflow-x: hidden;

  .name-user b {
    margin-left: 5px;
  }

  background: #3a1244;

  .page-logged-container {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-template-areas:
      "user main"
      "footer footer";
    margin: 50px auto;
    max-width: 1100px;
  }

  header {
    grid-area: topo;
    width: 100vw;
    height: 70px;

    background: ${colors.darkgray};
    opacity: 30%;
  }

  /* header div {
    display: flex;
    justify-content: space-between;
    max-width: 1000px;
    margin: 0 auto;
  }

  header p,
  a {
    display: flex;
    padding-top: 20px;
    margin-left: 20px;
    color: ${colors.lightgray};
  }

  header p img {
    height: 30px;
    border-radius: 50%;
    display: flex;
    margin-left: 20px;
  } */

  .name-user {
    display: none;
  }

  .back {
    margin-left: 90%;
  }

  .back a {
    display: flex;
    font-size: 25px;
    margin: 0 10px;
    margin-top: -30px;
    float: right;
  }

  @media (max-width: 700px) {
    .page-logged-container {
      display: grid;
      grid-template-columns: 1fr 3fr;
      grid-template-areas:
        "user user"
        "main main"
        "footer footer";
      margin: 50px auto;
      max-width: 600px;
    }

    header {
      background: none;
      opacity: 100%;
      margin: 0 auto;
      font-size: 15px;
    }

    header .name-user {
      display: flex;
      margin-top: 6px;
    }

    /* .name-user{
        display: none;
        opacity: 0;
    } */

    .name-user img {
      margin-top: -3px;
      margin-left: 10px;
    }

    header .back {
      margin-right: 20px;
    }

    .icons {
      margin: 0 5px;
    }

    .back {
      margin-left: 0;
    }
  }
`;
