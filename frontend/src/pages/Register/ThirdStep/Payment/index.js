import React, { useState } from "react";
import { Bill, Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
  TextField,
} from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link, Route } from "react-router-dom";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { add, format } from "date-fns";
import api from "../../../../services/api";
import { BeatLoader } from "react-spinners";
import { useAuth, useUser } from "../../../../contexts/auth";

export default function Payment() {
  const [name, setName] = useState("");
  const [usersLimit, setUsersLimit] = useState(1);
  const [usageLimit, setUsageLimit] = useState(1);
  const [billValue, setBillValue] = useState(0.0);
  const [paymentMethodDialog, setPatmentMethodDialog] = useState(false);
  const [success, setSuccess] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [loadingType, setLoadingType] = useState(undefined);
  const history = useHistory();
  const location = useLocation();
  const { user } = useUser();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerConditions");
  }

  const handleChangeProduct = (event) => {
    setProducts(event);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  const { setAuth } = useAuth();

  async function handleSubmit(e) {
    if (e) {
      e.preventDefault();
    }
    try {
      await api.post("/cadcliente", {
        nome: routeState.name,
        login: routeState.login,
        senha: routeState.password,
        fisicaoujuridica: routeState.type === "PF" ? "fisica" : "juridica",
        cpfoucnpj:
          routeState.type === "PF"
            ? Number(routeState.cpf)
            : Number(routeState.cnpj),
        cidade: routeState.city,
        estado: routeState.state.nome,
        email: routeState.email,
        // data_cadastro:"29/11/2020", <- não deveria ser enviado pelo front
        data_cadastro: format(new Date(), "dd/MM/yyyy"),
        plano: routeState.plan === 1 ? "Free" : "Premium",
        // validade:"01/01/2022", <- deveria ser a quantidade de meses
        // faltou mandar o nome do curso e CEP <-
        validade: format(add(new Date(), { months: usageLimit }), "dd/MM/yyyy"),
        acessos: usersLimit,
      });
      console.log(res.data);

      const res = await api.post("/api/v1/login", {
        login: routeState.login,
        senha: routeState.password,
      });
      setAuth(res.data);
      setLoading(false);
      setSuccess(true);

      // setEmail("");
      // setLogin("");
      // setPassword("");
      // setTimeout(() => history.push("/home"), 2500);
    } catch {
      setLoading(false);
      setLoadingType(undefined);
      setSuccess(false);
    }
  }

  const produtos = [];

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar
          admin={user?.cargo == "professor"}
          logged={false}
          goBack="/registerGeneral"
        />
        <Grid
          container
          // component="form"
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
          // onSubmit={handleSubmit}
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <h2 className="title">Cadastro</h2>

            <TextField
              id="filled-select-currency-native"
              select
              label="Produto"
              value={products}
              onChange={handleChangeProduct}
              SelectProps={{
                native: true,
              }}
              variant="filled"
              className="input"
            >
              {products?.map((option) => (
                <option key={option.id} value={option.id}>
                  {option.nome}
                </option>
              ))}
            </TextField>
            <InputTC
              className="input"
              label="Número de acessos"
              value={usersLimit}
              onChange={(e) => setUsersLimit(Number(e.target.value))}
              onInput={(e) => {
                if (e.target.value) {
                  e.target.value = Math.max(
                    0,
                    parseInt(e.target.value) !== NaN
                      ? parseInt(e.target.value)
                      : 0
                  ).toString();
                }
              }}
            />
            <InputTC
              className="input"
              label="Período de uso (em meses)"
              value={usageLimit}
              onChange={(e) => setUsageLimit(Number(e.target.value))}
              onInput={(e) => {
                if (e.target.value) {
                  e.target.value = Math.max(
                    0,
                    parseInt(e.target.value) !== NaN
                      ? parseInt(e.target.value)
                      : 0
                  ).toString();
                }
              }}
            />
            <Bill
              active={paymentMethodDialog}
              onClick={(e) => {
                if (paymentMethodDialog) {
                  if (e.target === e.currentTarget)
                    setPatmentMethodDialog(false);
                } else {
                  setPatmentMethodDialog(!paymentMethodDialog);
                }
              }}
            >
              <h2>{routeState?.plan === 1 ? "FREE" : "PREMIUM"}</h2>
              <p>{usersLimit} Acessos</p>
              <p>{usageLimit} Meses</p>
              <h3>TOTAL R${billValue}</h3>
              {paymentMethodDialog ? (
                <>
                  {routeState?.plan === 1 ? (
                    <Button
                      name={
                        loading && loadingType === "Free" ? (
                          <BeatLoader size={13} color={colors.lightyellow} />
                        ) : (
                          "Continuar"
                        )
                      }
                      txtcolor={colors.lightyellow}
                      color={colors.purple}
                      className="opBtn"
                      onClick={async () => {
                        setLoading(true);
                        setLoadingType("Free");
                        await handleSubmit();
                        // if (success) {
                        history.push("/login", {
                          data: {
                            ...routeState,
                            usersLimit,
                            usageLimit,
                            billValue,
                            courseName: name,
                          },
                        });
                        // }
                      }}
                    />
                  ) : (
                    <>
                      {/* <Link to="/creditCard"> */}
                      <Button
                        // name={"Cartão de crédito"}
                        name={
                          loading && loadingType === "Cartão de crédito" ? (
                            <BeatLoader size={13} color={colors.lightyellow} />
                          ) : (
                            "Cartão de crédito"
                          )
                        }
                        child
                        txtcolor={colors.lightyellow}
                        color={colors.purple}
                        className="opBtn"
                        onClick={async () => {
                          setLoading(true);
                          setLoadingType("Cartão de crédito");
                          await handleSubmit();
                          // if (success) {
                          history.push("/creditCard", {
                            data: {
                              ...routeState,
                              usersLimit,
                              usageLimit,
                              billValue,
                              courseName: name,
                            },
                          });
                          // }
                        }}
                      />
                      {/* </Link> */}
                      <Button
                        name="Paypal"
                        txtcolor={colors.lightyellow}
                        color={colors.purple}
                        className="opBtn"
                      />
                      <Button
                        name="Boleto bancário"
                        txtcolor={colors.lightyellow}
                        color={colors.purple}
                        className="opBtn"
                      />
                      {/* <Button
                    name="Prosseguir para compra"
                    txtcolor={colors.blue}
                    color={colors.aqua}
                    className="mainBtn"
                  /> */}
                    </>
                  )}
                </>
              ) : (
                <>
                  {usersLimit && usageLimit ? (
                    <p className="action">Clique aqui para prosseguir</p>
                  ) : (
                    <p className="action">Escolha seu plano!</p>
                  )}
                </>
              )}
            </Bill>
          </Grid>
        </Grid>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={success !== undefined}
          autoHideDuration={1600}
          onClose={handleClose}
        >
          {success !== undefined && (
            <SnackMessageTC
              message={success ? "Sucesso" : "Erro"}
              handleClose={handleClose}
              type={success ? "success" : "error"}
            />
          )}
        </Snackbar>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
