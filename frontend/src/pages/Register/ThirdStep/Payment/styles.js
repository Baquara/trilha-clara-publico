import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  Input::placeholder {
    color: ${colors.gray2};
  }

  .mainGrid {
    height: auto;
    position: relative;
    padding-bottom: 140px;
    margin-top: 60px;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }

  .title {
    margin-top: 80px;
  }

  /* button{
        margin-top: 100px;
    } */

  .opBtn {
    margin: 5px;
  }

  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .input {
    width: 80%;
    min-width: 250px;
    margin-bottom: 10px;
    background-color: white;
    color: ${colors.gray2};
  }
`;

export const Bill = styled.div`
  background-color: ${colors.lightyellow};
  padding: 5px;
  width: 90%;
  height: 100px;
  position: absolute;
  bottom: 0px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 8px 8px 0px 0px;
  transition: all 0.25s;
  cursor: pointer;

  h2 {
    color: ${colors.dark};
    font-size: 18px;
  }

  h3 {
    color: ${colors.primary};
    font-size: 18px;
    margin: 0;
  }

  p {
    color: ${colors.gray3};
    font-size: 14px;
    margin: 0;
  }

  .action {
    color: ${colors.aqua};
    font-weight: bold;
  }

  ${(props) =>
    props.active
      ? `
            height: 350px;
            z-index: 2;
            button {
                margin: 5px;
            }
        `
      : `
            height 120px;

            :hover {
                height: 140px;  
            }
        `}
`;
