import React, { useState } from "react";
import { Bill, Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import { Grid, Snackbar } from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Axios from "axios";

export default function CreditCard() {
  const [nomecartao, setNomeCartao] = useState("");
  const [ncartao, setNCartao] = useState("");
  const [ccexp, setCCExp] = useState(new Date());
  const [cvv, setCvv] = useState("");
  const [success, setSuccess] = useState(undefined);
  // const [selectedDate, setCCExp] = useState(new Date());
  const history = useHistory();
  const location = useLocation();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerPayment");
  }

  const handleDateChange = (date) => {
    setCCExp(date);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  const api = Axios.create({
    baseURL: "https://claralab.pythonanywhere.com",
  });

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const res = await api.post("/api/v1/pagamento?tipo=cc", {
        ncartao,
        cvv,
        ccexp,
        nomecartao,
      });
      console.log(res.data);
      setSuccess(res.data.sucesso === "true");
      setNCartao("");
      setCCExp("");
      setCvv("");
      setNomeCartao("");
      setTimeout(() => history.push("/login"), 2500);
    } catch {
      setSuccess(false);
      setTimeout(() => history.push("/login"), 2500);
    }
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar admin={false} logged={false} goBack="/registerGeneral" />
        <Grid
          container
          component="form"
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
          onSubmit={handleSubmit}
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <h2>Cadastro</h2>

            <InputTC
              className="input"
              label="Número do cartão"
              value={ncartao}
              onChange={(e) => setNCartao(e.target.value)}
            />
            <InputTC
              className="input"
              label="CVV"
              value={cvv}
              onChange={(e) => setCvv(e.target.value)}
            />
            <KeyboardDatePicker
              margin="normal"
              id="date-picker-dialog"
              className="input date"
              label="Data de nascimento"
              format="MM/yyyy"
              value={ccexp}
              onChange={handleDateChange}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
            />
            <InputTC
              className="input"
              label="Nome no cartão"
              value={nomecartao}
              onChange={(e) => setNomeCartao(e.target.value)}
            />
            <Bill>
              <h2>PREMIUM</h2>
              <p>{routeState.usersLimit} Acessos</p>
              <p>{routeState.usageLimit} Meses</p>
              <h3>TOTAL R$</h3>
            </Bill>

            <Button
              name="Confirmar"
              txtcolor={colors.blue}
              color={colors.aqua}
              className="mainBtn"
              type="submit"
            />
          </Grid>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={success !== undefined}
            autoHideDuration={1600}
            onClose={handleClose}
          >
            {success !== undefined && (
              <SnackMessageTC
                message={success ? "Sucesso" : "Erro"}
                handleClose={handleClose}
                type={success ? "success" : "error"}
              />
            )}
          </Snackbar>
        </Grid>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
