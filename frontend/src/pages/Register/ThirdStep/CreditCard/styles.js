import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  .input {
    background-color: ${colors.gray4};
    color: ${colors.gray2};
  }

  /* .date {
    width: 76%;
    padding: 10px;
  } */

  #date-picker-dialog {
    padding-left: 10px;
  }

  #date-picker-dialog-label {
    padding-left: 10px;
    padding-top: 8px;
  }

  Input::placeholder {
    color: ${colors.gray2};
  }

  .mainGrid {
    height: auto;
    position: relative;
    margin-top: 60px;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }

  button {
    margin-top: 140px;
  }

  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .input {
    width: 80%;
    margin-bottom: 5px;
    margin-top: 5px;
    min-width: 250px;
    /* background-color: white;
    color: ${colors.gray2}; */
  }
`;

export const Bill = styled.div`
  background-color: ${colors.lightyellow};
  padding: 5px;
  width: 78%;
  height: 80px;
  position: absolute;
  bottom: 50px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 8px 8px 0px 0px;
  transition: all 0.5s;

  h2 {
    color: ${colors.dark};
    font-size: 18px;
  }

  h3 {
    color: ${colors.primary};
    font-size: 18px;
  }

  p {
    color: ${colors.gray3};
    font-size: 14px;
    margin: 0;
  }

  ${(props) =>
    props.active
      ? `
            height: 350px;
            z-index: 2;
            button {
                margin: 5px;
            }
        `
      : `
            height 120px;

            :hover {
                height: 140px;  
            }
        `}
`;
