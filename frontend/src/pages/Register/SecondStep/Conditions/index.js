import React, { useState } from "react";
import { Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
  TextField,
} from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

export default function Conditions() {
  const [acceptedTerms, setAcceptedTerms] = useState(false);
  const [success, setSuccess] = useState(undefined);
  const history = useHistory();
  const location = useLocation();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerPlan");
  }

  // async function handleSubmit(e) {
  //   e.preventDefault();
  //   try {
  //     const res = await api.post("/", { name, duration, description });
  //     console.log(res.data);
  //     setSuccess(res.data.sucesso === "true");
  //     setEmail("");
  //     setLogin("");
  //     setPassword("");
  //     setTimeout(() => history.push("/home"), 2500);
  //   } catch {
  //     setSuccess(false);
  //   }
  // }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  return (
    <Container>
      <NavBar admin={false} logged={false} goBack="/registerGeneral" />
      <Grid
        container
        component="form"
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
        onSubmit={(e) => {
          e.preventDefault();
          history.push("/registerPayment", {
            data: {
              ...routeState,
              acceptedTerms,
            },
          });
        }}
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Cadastro</h2>

          <div className="terms">
            <h3>Termos e condições</h3>
            <div className="content">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing
                and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five centuries, but also
                the leap into electronic typesetting, remaining essentially
                unchanged. It was popularised in the 1960s with the release of
                Letraset sheets containing Lorem Ipsum passages, and more
                recently with desktop publishing software like Aldus PageMaker
                including versions of Lorem Ipsum.
              </p>
              <p>
                Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived
                not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised
                in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div className="accept">
              <input
                type="checkbox"
                value={acceptedTerms}
                onChange={(e) => setAcceptedTerms(e.target.checked)}
              />
              <p>Aceitar termos e condições</p>
            </div>
          </div>
          {acceptedTerms ? (
            <Button
              name="Confirmar"
              txtcolor={colors.blue}
              color={colors.aqua}
            />
          ) : (
            <>
              <p className="accept">
                Aceite os termos e condições de uso para prosseguir.
              </p>
              <Button
                name="Confirmar"
                txtcolor={colors.gray2}
                color={colors.gray}
              />
            </>
          )}
        </Grid>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={success !== undefined}
          autoHideDuration={1600}
          onClose={handleClose}
        >
          {success !== undefined && (
            <SnackMessageTC
              message={success ? "Sucesso" : "Erro"}
              handleClose={handleClose}
              type={success ? "success" : "error"}
            />
          )}
        </Snackbar>
      </Grid>
    </Container>
  );
}
