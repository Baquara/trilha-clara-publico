import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  .accept {
    font-size: 14px;
  }

  Input::placeholder {
    color: ${colors.gray2};
  }

  .mainGrid {
    height: auto;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }
  button {
    margin: 10px;
  }
  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .terms {
    background-color: white;
    border-radius: 8px;
    width: 400px;
    height: 350px;
    padding: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    .content {
      max-height: 250px;
      overflow-y: scroll;
    }

    h3,
    p {
      color: #000;
    }

    p {
      font-size: 12px;
    }

    .accept {
      display: flex;
      align-items: center;
    }
  }

  @media (max-width: 700px) {
    .terms {
      border-radius: 8px;
      width: 240px;

      h3 {
        font-size: 22px;
      }

      .content {
        max-height: 300px;
        overflow-y: scroll;
      }

      .accept {
        p {
          font-size: 14px;
        }
      }
    }
    .mainGrid {
      margin-top: 60px;
    }
  }
`;
