import React, { useState } from "react";
import { Container, PlanCard } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
  TextField,
} from "@material-ui/core";
import PremiumImg from "../../../../assets/img/premiumplan.jpg";
import FreeImg from "../../../../assets/img/freeplan.jpg";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";

export default function Plan() {
  const [plan, setPlan] = useState(undefined);
  const [success, setSuccess] = useState(undefined);
  const history = useHistory();
  const location = useLocation();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerAddress");
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  // async function handleSubmit(e) {
  //   e.preventDefault();
  //   try {
  //     const res = await api.post("/", { name, duration, description });
  //     console.log(res.data);
  //     setSuccess(res.data.sucesso === "true");
  //     setEmail("");
  //     setLogin("");
  //     setPassword("");
  //     setTimeout(() => history.push("/home"), 2500);
  //   } catch {
  //     setSuccess(false);
  //   }
  // }

  return (
    <Container>
      <NavBar admin={false} logged={false} goBack="/registerAddress" />
      <Grid
        container
        component="form"
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
        onSubmit={(e) => {
          e.preventDefault();
          history.push("/registerConditions", {
            data: {
              ...routeState,
              plan,
            },
          });
        }}
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Cadastro</h2>

          <h4>Escolha seu plano de preferência</h4>

          {plan === 0 || plan === undefined ? (
            <PlanCard
              img={PremiumImg}
              color="rgba(238, 75, 112, 0.4)"
              selected={plan === 0}
              onClick={() =>
                plan === undefined ? setPlan(0) : setPlan(undefined)
              }
            >
              <h3>PREMIUM</h3>
              {plan === 0 ? (
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Sodales eget cursus sit nunc. Dui feugiat at ac velit
                  dignissim purus. Eu aliquam leo imperdiet condimentum etiam in
                  facilisis. Viverra quis integer sed nisi. Diam neque quis
                  egestas cras.
                </p>
              ) : (
                <></>
              )}
            </PlanCard>
          ) : (
            <></>
          )}
          {plan === 1 || plan === undefined ? (
            <PlanCard
              img={FreeImg}
              color="rgba(60, 192, 204, 0.4)"
              selected={plan === 1}
              onClick={() =>
                plan === undefined ? setPlan(1) : setPlan(undefined)
              }
            >
              <h3>FREE</h3>
              {plan === 1 ? (
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Sodales eget cursus sit nunc. Dui feugiat at ac velit
                  dignissim purus. Eu aliquam leo imperdiet condimentum etiam in
                  facilisis. Viverra quis integer sed nisi. Diam neque quis
                  egestas cras.
                </p>
              ) : (
                <></>
              )}
            </PlanCard>
          ) : (
            <></>
          )}
          {plan != undefined ? (
            <>
              {plan == 0 ? (
                <Button //Esse botão deveria seguir o fluxo até o pagamento
                  name="Confirmar"
                  txtcolor={colors.blue}
                  color={colors.aqua}
                />
              ) : (
                <Button //Esse botão deveria evitar redirecionamento para o pagamento
                  name="Confirmar"
                  txtcolor={colors.blue}
                  color={colors.aqua}
                />
              )}
            </>
          ) : (
            <></>
          )}
        </Grid>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={success !== undefined}
          autoHideDuration={1600}
          onClose={handleClose}
        >
          {success !== undefined && (
            <SnackMessageTC
              message={success ? "Sucesso" : "Erro"}
              handleClose={handleClose}
              type={success ? "success" : "error"}
            />
          )}
        </Snackbar>
      </Grid>
    </Container>
  );
}
