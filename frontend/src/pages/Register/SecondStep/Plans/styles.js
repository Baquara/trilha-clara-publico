import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  Input::placeholder {
    color: ${colors.gray2};
  }

  .mainGrid {
    height: auto;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }

  h4 {
    font-size: 14px;
  }

  button {
    margin: 10px;
  }
  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .input {
    width: 80%;
    margin-bottom: 10px;
    background-color: white;
    color: ${colors.gray2};
  }

  @media (max-width: 700px) {
    .mainGrid {
      margin-top: 30px;
    }
  }
`;

export const PlanCard = styled.div`
  width: 80%;
  border-radius: 8px;
  background-image: linear-gradient(
      ${(props) => props.color},
      ${(props) => props.color}
    ),
    url("${(props) => props.img}");
  background-size: cover;
  background-position: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
  padding: 20px;
  transition: height 2s;
  cursor: pointer;

  ${(props) =>
    props.selected
      ? `
        height: 200px;
        p {
          animation-delay: 8s;
          animation: appear 4s ease-in-out;
        }
        `
      : `
        height: 100px
        `};

  p {
    font-size: 14px;
  }

  @keyframes appear {
    0% {
      visibility: hidden;
      opacity: 0;
    }

    100% {
      visibility: visible;
      opacity: 1;
    }
  }
`;
