import * as yup from "yup";

yup.setLocale({
  mixed: {
    default: "Não é válido",
  },
  string: {
    required: "O campo é obrigatório",
    min: "O campo deve ter mais que ${min} A caracteres",
    email: "O campo deve ser um email válido",
  },
});

export const schema = yup.object().shape({
  email: yup.string().email().required(),
  //   emailConfirmation: yup
  // .string()
  // .oneOf([yup.ref("email"), null], "O email deve ser igual"),
  password: yup.string().min(8).required(),
  login: yup.string().min(1).required(),
});
