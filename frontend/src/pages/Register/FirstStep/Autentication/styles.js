import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  input {
    width: 100%;
    padding-left: 10px;
  }

  Input::placeholder {
    color: ${colors.gray2};
  }

  .mainGrid {
    height: auto;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }
  button {
    margin: 10px;
  }
  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .password {
    width: 80%;
    min-width: 250px;
    background-color: ${colors.gray4};
    color: ${colors.gray2};
    margin-top: 10px;
  }

  @media (max-width: 345px) {
    .password {
      max-width: 250px;
    }
  }
`;
