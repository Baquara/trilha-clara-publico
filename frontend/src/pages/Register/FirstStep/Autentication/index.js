import React, { useEffect, useState } from "react";
import { Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
} from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";
import { Visibility, VisibilityOff } from "@material-ui/icons";
// import { debounce } from "debounce";
import { useDebounce } from "use-debounce";
import { schema } from "./validation";
import ErrorText from "../../../../components/ErrorText";
import { useUser } from "../../../../contexts/auth";

export default function Autentication() {
  const { user } = useUser();
  const [email, setEmail] = useState("");
  const [confirmation, setConfirmation] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState(undefined);
  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });

  const [errorsObj, setErrors] = useState({});
  const [errors] = useDebounce(errorsObj, 500);

  async function checkValidation(value, all = false) {
    try {
      setErrors({});
      const res = await schema.validate(value, { abortEarly: false });
      return res;
    } catch (err) {
      if (!all) {
        const key = Object.keys(value)[0];
        const target = err.inner.find((e) => e.path === key);
        setErrors({ [key]: target?.errors && target?.errors[0] });
      } else {
        const allErrors = err.inner.map((e) => [e.path, e.errors[0]]);
        console.log({ allErrors });
        setErrors(Object.fromEntries(allErrors));
        return false;
      }
    }
  }

  useEffect(() => {
    if (email) checkValidation({ email });
  }, [email]);
  useEffect(() => {
    if (login) checkValidation({ login });
  }, [login]);
  useEffect(() => {
    if (values.password) checkValidation({ password: values.password });
  }, [values]);

  const history = useHistory();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <Container>
      <NavBar
        admin={user?.cargo == "professor"}
        logged={false}
        goBack="/login"
      />
      <Grid
        container
        component="form"
        className="mainGrid"
        justify="center"
        alignItems="flex-start"
        onSubmit={async (e) => {
          e.preventDefault();
          const checked = await checkValidation(
            {
              email,
              login,
              ...values,
            },
            true
          );
          if (checked)
            history.push("/registerGeneral", {
              data: {
                email,
                login,
                ...values,
              },
            });
        }}
      >
        <Grid
          container
          item
          className="mainGrid subGrid"
          xs={11}
          sm={6}
          lg={4}
          direction="column"
          justify="space-around"
          alignItems="center"
        >
          <h2>Cadastro</h2>

          <InputTC
            className="mainInput"
            label="E-mail"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          {errors.email && <ErrorText>{errors.email}</ErrorText>}
          <InputTC
            className="mainInput"
            label="Confirmar E-mail"
            value={confirmation}
            onChange={(e) => setConfirmation(e.target.value)}
          />
          <InputTC
            label="Login"
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
          {errors.login && <ErrorText>A{errors.login}</ErrorText>}
          <FormControl className="password" variant="filled">
            <InputLabel htmlFor="filled-adornment-password">Senha</InputLabel>
            <Input
              id="filled-adornment-password"
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              onChange={handleChange("password")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          {errors.password && <ErrorText>{errors.password}</ErrorText>}
          {/* <Link to="/registerGeneral" > */}
          <Button
            name="Confirmar"
            txtcolor={colors.primary}
            color={colors.aqua}
            // color={colors.gray}
          />
          {/* </Link> */}
        </Grid>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={success !== undefined}
          autoHideDuration={1600}
          onClose={handleClose}
        >
          {success !== undefined && (
            <SnackMessageTC
              message={success ? "Sucesso" : "Erro"}
              handleClose={handleClose}
              type={success ? "success" : "error"}
            />
          )}
        </Snackbar>
      </Grid>
    </Container>
  );
}
