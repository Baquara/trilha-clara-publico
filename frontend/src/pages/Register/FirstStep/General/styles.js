import styled from "styled-components";
import colors from "../../../../utils/colors";

export const Container = styled.div`
  color: white;
  font-family: Roboto, sans-serif;
  background: ${colors.primary};
  font-size: 25px;
  height: 100vh;
  width: 100vw;
  overflow: auto;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  #date-picker-dialog {
    padding-left: 10px;
  }

  #date-picker-dialog-label {
    padding-left: 10px;
    padding-top: 8px;
  }
  Input::placeholder {
    color: ${colors.gray2};
  }

  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  input[type="number"] {
    -moz-appearance: textfield;
  }

  .mainGrid {
    height: auto;
  }

  h2 {
    margin: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: center;
  }
  button {
    margin: 10px;
  }
  .subGrid {
    width: 100%;
    @media (min-width: 700px) {
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }

  .input {
    width: 80%;
    min-width: 250px;
    margin-top: 5px;
    margin-bottom: 5px;
    background-color: ${colors.gray4};
    color: ${colors.gray2};
  }
`;
