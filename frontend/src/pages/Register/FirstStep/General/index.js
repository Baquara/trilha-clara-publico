import React, { useState } from "react";
import { Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
  TextField,
} from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { useEffect } from "react";

const types = [
  {
    label: "Pessoa Física",
    value: "PF",
  },
  {
    label: "Pessoa Jurídica",
    value: "PJ",
  },
];

export default function General() {
  const [name, setName] = useState("");
  const [type, setType] = useState("PF");
  const [cpf, setCpf] = useState("");
  const [cnpj, setCnpj] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [success, setSuccess] = useState(undefined);
  const history = useHistory();
  const location = useLocation();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerAutentication");
  }

  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  const handleChangeType = (event) => {
    setType(event.target.value);
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  // async function handleSubmit(e) {
  //   e.preventDefault();
  //   try {
  //     const res = await api.post("/", { name, duration, description });
  //     console.log(res.data);
  //     setSuccess(res.data.sucesso === "true");
  //     setEmail("");
  //     setLogin("");
  //     setPassword("");
  //     setTimeout(() => history.push("/home"), 2500);
  //   } catch {
  //     setSuccess(false);
  //   }
  // }

  const types = [
    {
      label: "Pessoa Física",
      value: "PF",
    },
    {
      label: "Pessoa Jurídica",
      value: "PJ",
    },
  ];

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar goBack="/home" />
        <Grid
          container
          component="form"
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
          onSubmit={(e) => {
            e.preventDefault();
            history.push("/registerAddress", {
              data: {
                ...routeState,
                name,
                type,
                cpf,
                cnpj,
                birthDate,
                success,
              },
            });
          }}
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <h2>Cadastro</h2>

            <TextField
              id="filled-select-currency-native"
              select
              label="Tipo de pessoa"
              value={type}
              onChange={handleChangeType}
              SelectProps={{
                native: true,
              }}
              variant="filled"
              className="input"
            >
              {types.map((option) => {
                return <option value={option.value}>{option.label}</option>;
              })}
            </TextField>
            <InputTC
              className="mainInput input"
              label="Nome completo"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            {type === "PF" ? (
              <>
                <InputTC
                  className="mainInput input"
                  label="CPF"
                  value={cpf}
                  onChange={(e) => setCpf(e.target.value)}
                  type="number"
                  onInput={(e) => {
                    e.target.value = Math.max(0, parseInt(e.target.value))
                      .toString()
                      .slice(0, 11);
                  }}
                />
                <KeyboardDatePicker
                  margin="normal"
                  id="date-picker-dialog"
                  className="input date"
                  label="Data de nascimento"
                  format="dd/MM/yyyy"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </>
            ) : (
              <InputTC
                className="mainInput input"
                label="CNPJ"
                value={cnpj}
                onChange={(e) => setCnpj(e.target.value)}
                type="number"
                onInput={(e) => {
                  e.target.value = Math.max(0, parseInt(e.target.value))
                    .toString()
                    .slice(0, 14);
                }}
              />
            )}

            {/* <Link to="/registerAddress"> */}
            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
            />
            {/* </Link> */}
          </Grid>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={success !== undefined}
            autoHideDuration={1600}
            onClose={handleClose}
          >
            {success !== undefined && (
              <SnackMessageTC
                message={success ? "Sucesso" : "Erro"}
                handleClose={handleClose}
                type={success ? "success" : "error"}
              />
            )}
          </Snackbar>
        </Grid>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
