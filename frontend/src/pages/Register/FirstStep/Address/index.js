import React, { useState, useEffect } from "react";
import { Container } from "./styles";
import Button from "../../../../components/Button";
import NavBar from "../../../../components/NavBar";
import InputTC from "../../../../components/InputTC";
import colors from "../../../../utils/colors";
import {
  FormControl,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar,
  TextField,
} from "@material-ui/core";

import SnackMessageTC from "../../../../components/SnackMessageTC";
import { useLocation, useHistory, Link } from "react-router-dom";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Axios from "axios";

export default function Address() {
  const [cep, setCep] = useState("");
  const [street, setStreet] = useState("");
  const [number, setNumber] = useState("");
  const [state, setState] = useState("");
  const [stateList, setStateList] = useState([]);
  const [cityList, setCityList] = useState([]);
  const [city, setCity] = useState("");
  const [phone, setPhone] = useState("");
  const [success, setSuccess] = useState(undefined);
  const history = useHistory();
  const location = useLocation();

  const routeState = location.state?.data;
  if (!routeState) {
    history.push("/registerGeneral");
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSuccess(undefined);
  };

  const handleChangeState = (event) => {
    const target = stateList.find(
      (currentState) => currentState.id == event.target.value
    );
    console.log(state.nome);
    setState(target);
  };

  const handleChangeCity = (event) => {
    setCity(event.target.value);
  };

  // async function handleSubmit(e) {
  //   e.preventDefault();
  //   try {
  //     const res = await api.post("/", { name, duration, description });
  //     console.log(res.data);
  //     setSuccess(res.data.sucesso === "true");
  //     setEmail("");
  //     setLogin("");
  //     setPassword("");
  //     setTimeout(() => history.push("/home"), 2500);
  //   } catch {
  //     setSuccess(false);
  //   }
  // }
  useEffect(() => {
    Axios.get(
      "https://servicodados.ibge.gov.br/api/v1/localidades/estados"
    ).then((res) => {
      setStateList(res.data);
    });
  }, []);
  useEffect(() => {
    Axios.get(
      `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${state.id}/municipios`
    ).then((res) => {
      setCityList(res.data);
      // console.log(res.data);
    });
    // console.log(cityList);
  }, [state]);
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <NavBar admin={false} logged={false} goBack="/registerGeneral" />
        <Grid
          container
          component="form"
          className="mainGrid"
          justify="center"
          alignItems="flex-start"
          onSubmit={(e) => {
            e.preventDefault();
            history.push("/registerPlan", {
              data: {
                ...routeState,
                cep,
                street,
                number,
                state,
                stateList,
                cityList,
                city,
                phone,
              },
            });
          }}
        >
          <Grid
            container
            item
            className="mainGrid subGrid"
            xs={11}
            sm={6}
            lg={4}
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            <h2>Cadastro</h2>

            <InputTC
              className="input"
              label="CEP"
              value={cep}
              onChange={(e) => setCep(e.target.value)}
              type="number"
              onInput={(e) => {
                e.target.value = Math.max(0, parseInt(e.target.value))
                  .toString()
                  .slice(0, 8);
              }}
            />
            <InputTC
              className="input"
              label="Rua"
              value={street}
              onChange={(e) => setStreet(e.target.value)}
            />
            <InputTC
              className="input"
              label="Número"
              value={number}
              onChange={(e) => setNumber(e.target.value)}
            />
            <TextField
              id="filled-select-currency-native"
              select
              label="Estado"
              value={state?.name}
              onChange={handleChangeState}
              SelectProps={{
                native: true,
              }}
              variant="filled"
              className="input"
            >
              {stateList.map((option) => (
                <option key={option.id} value={option.id}>
                  {option.nome}
                </option>
              ))}
            </TextField>
            <TextField
              id="filled-select-currency-native"
              select
              label="Cidade"
              value={city}
              onChange={handleChangeCity}
              SelectProps={{
                native: true,
              }}
              variant="filled"
              className="input"
            >
              {cityList.map((option) => (
                <option key={option.sigla} value={option.sigla}>
                  {option.nome}
                </option>
              ))}
            </TextField>
            <InputTC
              className="input"
              label="Telefone"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
            {/* <Link to="/registerPlan"> */}
            <Button
              name="Confirmar"
              txtcolor={colors.primary}
              color={colors.aqua}
            />
            {/* </Link> */}
          </Grid>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={success !== undefined}
            autoHideDuration={1600}
            onClose={handleClose}
          >
            {success !== undefined && (
              <SnackMessageTC
                message={success ? "Sucesso" : "Erro"}
                handleClose={handleClose}
                type={success ? "success" : "error"}
              />
            )}
          </Snackbar>
        </Grid>
      </Container>
    </MuiPickersUtilsProvider>
  );
}
