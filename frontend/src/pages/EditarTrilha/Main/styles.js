import styled from "styled-components";
import { Button } from "grommet";
import colors from "../../../utils/colors";

export const MainStyled = styled.div`
  display: flex;
  margin: 0 auto;
  justify-content: center;

  a {
    text-decoration: none;
    color: #000;
  }

  .flag {
    position: absolute;
    color: white;
    font-size: 70px;
    margin-top: -30px;
    margin-left: 160px;
  }

  .lamp {
    font-size: 100px;
    color: ${colors.pink};
  }

  .flagFinal {
    margin-top: 30px;
    display: flex;
    justify-content: center;
  }

  .flagFinal .flag {
    margin-top: -40px;
    margin-right: 80px;
    transform: rotatey(180deg);
  }

  .buttonFinal {
    height: 150px;
    width: 150px;
  }

  .settings {
    margin-left: 160px;
  }

  @media (max-width: 770px) {
    .settings {
      margin-left: 110px;
    }

    .flag {
      margin-left: 110px;
    }

    .flagFinal .flag {
      margin-right: 30px;
    }
  }
`;

export const Buttons = styled(Button)`
  left: -100px;
  width: 100px;
  height: 100px;
  border: none;
  border-radius: 50%;
  margin-left: 10px;

  margin: 20px;

  text-align: center;
  justify-content: center;

  @media (max-width: 770px) {
    width: 80px;
    height: 80px;
    margin-left: 100px;

    margin: 10px;
    /* padding:  */
  }
`;
