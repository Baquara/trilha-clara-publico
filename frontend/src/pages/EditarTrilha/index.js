import React from "react";
import { LoggedContainer } from "./styles";
import MainStyled from "./Main";
import UserInfos from "./User";
import NavBar from "../../components/NavBar";
import balls from "../../assets/img/Group 38.png";
import { Link } from "react-router-dom";
import Button from "../../components/Button";
import colors from "../../utils/colors";
import { useUser } from "../../contexts/auth";

// Container Progress/Barra de Progresso na pasta User
export default function EditarTrilha() {
  const { user } = useUser();
  return (
    <div style={{ overflowX: "hidden" }}>
      <LoggedContainer>
        {/* <img src={balls} alt="" style={{position: 'absolute'}}/> */}
        <NavBar
          admin={user?.cargo == "professor"}
          logged={true}
          goBack="/course"
          className="top"
        />
        <div className="page-logged-container">
          <div className="user">
            <UserInfos
              info="Seu Progresso Nessa Trilha:"
              title="Clara Ideia"
              hours="6 horas"
              points="50"
              // TODO: tornar esse id dinamico
              courseId="1"
              maxpoints="100"
            />
          </div>

          <div className="main">
            <MainStyled />
          </div>
        </div>
      </LoggedContainer>
    </div>
  );
}
