import styled from "styled-components";
import colors from "../../../utils/colors";

const UserStyled = styled.div`
  text-align: center;
  margin-left: 50px;
  img {
    height: 130px;
    border-radius: 50%;
    margin-top: 5px;
    margin-bottom: 10px;
  }

  h3 {
    color: ${colors.lightgray};
    margin: 0;
    font-weight: bold;
    font-size: 17px;
    text-align: center;
    margin-top: 25px;
  }

  h4 {
    margin-bottom: 20px;
    color: ${colors.lightgray};
    margin: 0;
    font-weight: bold;
    font-size: 25px;
    text-align: center;
  }

  p {
    color: ${colors.gold};
    margin: 30px;
    font-weight: bold;
    font-size: 25px;
    margin-left: 10px;
  }

  @media (max-width: 770px) {
    margin-left: 0px;

    h3 {
      margin-bottom: 50px;
    }

    P {
      display: none;
    }
  }
`;

export default UserStyled;
