import styled from "styled-components";
import colors from "../../utils/colors";
import LinearProgress from "@material-ui/core/LinearProgress";
import balls from "../../assets/img/Group 38.png";
import bgPic from "../../assets/img/Group 136.png";

export const ProgressContainer = styled.div`
  box-sizing: border-box;
  width: 450px;
  height: 110px;
  border-radius: 0 150px 150px 5px;
  background: ${colors.green};
  margin: 0 auto;

  .container {
    padding: 25px;
    font-size: 25px;
    color: ${colors.primary};
  }

  @media (max-width: 770px) {
    display: none;
  }
`;

export const Progress = styled(LinearProgress)`
  margin-top: -20px;
  width: 350px;
  padding: 10px;
  height: 20px;
  left: 40px;
  border-radius: 150px;
`;

export const LoggedContainer = styled.div`
  max-width: 100vw;
  height: 100vh;
  margin: 0 auto;
  /* overflow-x: hidden; */
  /* display: flex; */

  background: linear-gradient(
      180deg,
      ${colors.primary} 31.09%,
      ${colors.pink} 100%
    ),
    ${colors.primary};
  overflow: auto;

  .page-logged-container {
    display: grid;
    grid-template-columns: 300px 1fr;
    grid-template-areas: "user main";
    margin: 60px auto;
    max-width: 100vw;
    min-height: 120%;
    background-image: url("${balls}");
    background-repeat: no-repeat;
    background-position: top center;
  }

  .top {
    display: flex;
    justify-content: space-between;
  }

  Button {
    cursor: pointer;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  }

  Button {
    font-weight: bold;
    color: white;
    font-size: 16px;
    margin-top: 15px;
  }

  Button:hover {
    transition: 0.3s;
    background: ${colors.primary};
    color: white;
  }

  a {
    color: white;
    text-decoration: none;
    /* margin-left: 100px; */
    opacity: 100%;
  }

  a:hover {
    font-weight: bold;
    color: ${colors.green};
    transition: 0.1s;
  }

  .user {
    grid-area: user;
    display: flex;
    position: fixed;
  }

  .main {
    grid-area: main;
    display: flex;
    justify-content: right;
  }

  @media (max-width: 770px) {
    background-image: url("${bgPic}");
    background-repeat: no-repeat;
    background: linear-gradient(
        180deg,
        ${colors.primary} 31.09%,
        ${colors.pink} 100%
      ),
      ${colors.primary};

    .page-logged-container {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-template-areas:
        "user user"
        "main main";
      margin: 50px auto;
      max-width: 600px;
      background-image: none;
    }

    .user {
      grid-area: user;
      display: block;
      position: relative;
    }

    header {
      background: ${colors.green};
      opacity: 100%;
      margin: 0 auto;
      height: 40vh;
      font-size: 15px;
      border-radius: 0 0 10px 10px;
      color: ${colors.primary};
    }

    header .back a {
      color: ${colors.purple};
    }

    header .back {
      display: flex;
      justify-content: space-between;
      width: 95vw;
      margin: 20px auto;
    }
  }
`;
