import React, { useEffect } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./pages/Login";
import EsqueciSenha from "./pages/EsqueciSenha";
import RedefinirSenha from "./pages/RedefinirSenha";
import PageLogged from "./pages/PageLogged";
import AdicionarFase from "./pages/AdicionarFase";
import EditarInfo from "./pages/EditarInfo";
import CriarTrilha from "./pages/CriarTrilha";
import Etapas from "./pages/Etapas";
import AddUser from "./pages/AddUser";
import AddUserTrilha from "./pages/AddUserTrilha";
import EditUser from "./pages/EditUser";
import UserProfile from "./pages/UserProfile";
import CourseDetails from "./pages/CourseDetails";
import CreatAccount from "./pages/CreateAccount";
import Participantes from "./pages/Participantes";
import Materiais from "./pages/Materiais";
import ResCards from "./pages/RespondeCards";
import AddCard from "./pages/AddCard";
import TelaTrilha from "./pages/TelaTrilha";
import LandingPage from "./pages/LandingPage";
import Forum from "./pages/Forum";
import FavoriteCards from "./pages/FavoriteCards";
import Settings from "./pages/Settings";
import Autentication from "./pages/Register/FirstStep/Autentication";
import General from "./pages/Register/FirstStep/General";
import Address from "./pages/Register/FirstStep/Address";
import Conditions from "./pages/Register/SecondStep/Conditions";
import Plan from "./pages/Register/SecondStep/Plans";
import Payment from "./pages/Register/ThirdStep/Payment";
import CreditCard from "./pages/Register/ThirdStep/CreditCard";
import CreateTopic from "./pages/CreateTopic";
import Dashboard from "./pages/Professor";
import GerenciarCards from "./pages/GerenciarCards";
import EditarTrilha from "./pages/EditarTrilha";
import { useAuth } from "./contexts/auth";

function PrivateRoute(props) {
  const { auth } = useAuth();

  if (!auth) {
    return <Redirect to="/login" />;
  } else {
    return <Route {...props} />;
  }
}

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={LandingPage} />
        <Route path="/login" component={Login} />
        <Route path="/forgotpassword" component={EsqueciSenha} />
        <Route path="/createAccount" component={CreatAccount} />
        <Route path="/registerAutentication" component={Autentication} />
        <Route path="/registerGeneral" component={General} />
        <Route path="/registerAddress" component={Address} />
        <Route path="/registerConditions" component={Conditions} />
        <Route path="/registerPlan" component={Plan} />
        <Route path="/registerPayment" component={Payment} />
        <Route path="/creditCard" component={CreditCard} />
        <PrivateRoute path="/newpassword" component={RedefinirSenha} />
        <PrivateRoute path="/home" component={PageLogged} />
        <PrivateRoute path="/addstage/:courseId" component={AdicionarFase} />
        <PrivateRoute path="/edit/:courseId" component={EditarInfo} />
        <PrivateRoute path="/create" component={CriarTrilha} />
        <PrivateRoute path="/stages" component={Etapas} />
        <PrivateRoute path="/adduser/:courseId" component={AddUser} />
        <PrivateRoute path="/addusertrilha/" component={AddUserTrilha} />
        <PrivateRoute path="/edituser" component={EditUser} />
        <PrivateRoute path="/myprofile" component={UserProfile} />
        <PrivateRoute path="/course/:courseId" component={CourseDetails} />
        <PrivateRoute path="/members" component={Participantes} />
        <PrivateRoute path="/materials" component={Materiais} />
        <PrivateRoute path="/cards" component={ResCards} />
        <PrivateRoute path="/addcard" component={AddCard} />
        <PrivateRoute path="/trilha" component={TelaTrilha} />
        <PrivateRoute path="/forum" component={Forum} />
        <PrivateRoute path="/favoritesCards" component={FavoriteCards} />
        <PrivateRoute path="/settings" component={Settings} />
        <PrivateRoute path="/createTopic" component={CreateTopic} />
        <PrivateRoute path="/dashboard" component={Dashboard} />
        <PrivateRoute path="/gerenciarCards" component={GerenciarCards} />
        <PrivateRoute path="/editTrilha" component={EditarTrilha} />
      </Switch>
    </BrowserRouter>
  );
}
