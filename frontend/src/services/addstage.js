import axios from "axios";

const apiRoutePrefix = "/api/v1/addstage";
const endpoint = process.env.REACT_APP_API_URL || "http://localhost:5000";

const api = axios.create({
  baseURL: `${endpoint}${apiRoutePrefix}`,
});

export default api;
