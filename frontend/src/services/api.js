import axios from "axios";

const apiRoutePrefix = "/api/v1/";
const endpoint = process.env.REACT_APP_API_URL || "http://claralab.pythonanywhere.com";

const api = axios.create({
  baseURL: `${endpoint}${apiRoutePrefix}`,
  headers: {
    'Content-Type': 'application/json'
  }
});

export default api;
