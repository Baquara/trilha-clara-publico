import React, { createContext, useContext, useEffect, useState } from "react";

const AuthContext = createContext({});

export default function AuthProvider({ children }) {
  const [auth, setAuth] = useState();
  const [user, setUser] = useState();

  useEffect(() => {
    if (auth) {
      localStorage.setItem("auth", JSON.stringify(auth));
      // console.log(getAuthFromLocalStorage());
    }
    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
      // console.log(getAuthFromLocalStorage());
    }
  }, [auth, user]);

  function getFromLocalStorage(key) {
    if (
      localStorage.getItem(key) &&
      localStorage.getItem(key) !== "undefined"
    ) {
      // console.log("localStorage", localStorage.getItem("auth"));
      return JSON.parse(localStorage.getItem(key));
    }
  }

  return (
    <AuthContext.Provider
      value={{
        auth: auth ? auth : getFromLocalStorage("auth"),
        setAuth,
        user: user ? user : getFromLocalStorage("user"),
        setUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  const context = useContext(AuthContext);
  if (!context)
    throw new Error("useContext must be used within a AuthProvider");
  const { auth, setAuth } = context;
  return { auth, setAuth };
}

export function useRole() {
  const context = useContext(AuthContext);
  if (!context)
    throw new Error("useContext must be used within a AuthProvider");
  const { user } = context;
  return user?.cargo;
}

export function useUser() {
  const context = useContext(AuthContext);
  if (!context)
    throw new Error("useContext must be used within a AuthProvider");
  const { user, setUser } = context;
  return { user, setUser };
}
