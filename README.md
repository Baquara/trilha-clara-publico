# Trilha Clara

<img src="https://gitlab.com/uploads/-/system/project/avatar/20065043/trilha-clara.jpg" width="210px" style="margin: 0 auto;"/>

**Documentos:**
[
[Proposta](https://docs.google.com/document/d/1Cj3jyw8WkYdsXpXiw8s-MboEbxuERfQrrGbSQOAXjXI/edit?usp=sharing)
|
[Apresentação](https://www.canva.com/design/DAEAsV7ISVE/yPw45hBjW-XzPTns0tOsKg/view?utm_content=DAEAsV7ISVE&utm_campaign=designshare&utm_medium=link&utm_source=homepage_design_menu)
|
[Milestones com pesos](https://docs.google.com/document/d/1ws6x2UFx_-iQsMOwOIw3n6ZLcZn1muCNyTVlDfvrnSA/edit?usp=sharing)
|
[Protótipo no Figma](https://www.figma.com/file/mvfWKQaRDkSxPLBCae52X4/Trilha-Clara?node-id=10%3A17)
|
[Sitemap no Notion](https://www.notion.so/2eee20719db9484a8d9b93b5b89aace7?v=2a5f61ddc64f4d4e8d6816dc5458fa4a)
]

**Links úteis:**
[
[Sistema em Desenvolvimento](http://trilhaclara.infojr.com.br/)
|
[Backend - Documentação no GitLab](https://gitlab.com/Baquara/documentacao-backend-felipe/-/blob/master/README.md)
|
[Backend - Documento no Postman ](https://documenter.getpostman.com/view/13068940/TVeiEWZn)
]

Plataforma de aprendizagem Trilha Clara

**Gerentes do Projeto**: @caiovinisl e @Edson-G

**Time**: @joaovictor1, @mariottipedro14, @vanessasamira1996 e @wxavier08

**Suporte**: @caiovinisl

**Testes**: @wxavier08

Outras pessoas **PODEM** pegar issues abertas, porém, apenas o Time, o Suporte ou os Gerentes devem aceitar Merge Requests.

### Personagens

~"💳"
~"💼"
~"📓"

### Necessidades da Cliente

- **Front-end interativo, animado e customizável**: Há um forte foco em conceitos de UX e UI, e as animações são especialmente importantes na tela do tabuleiro.
- **Comunicação entre os usuários**: Os participantes se comunicam através da plataforma, com recursos como chat.
- **Tipos diferentes de usuários**: Administadores, jogadores e clientes em potencial interagem com a plataforma de formas diferentes.

### Padrão de branch

Use _{id_da_issue}-descricao-breve_ para nomear as branchs.  
**Exemplo**:

- Para a issue #18 use **18-visualizar-participantes** como nome da branch

Obs.: Evite mais de 3 palavras como descrição breve no nome da issue.

### Divisão de Milestones

O projeto terá o prazo de aproximadamente 8.5 milestones, indo de 2 de Agosto de 2020 até 30 de Novembro de 2020.

---

- **1° Milestone [2020 Ago [2 a 15]](#)**
  - [x] Página de log-in
    - [x] Esqueci minha senha
      - [x] Redefinir senha
  - [x] Página inicial (Logado)
    - [x] Opções
    - [x] Cursos
      - [x] Gerenciar Cursos (Adm)

---

- **2º Milestone [2020 Ago [16 a 29]](#)**
  - [x] Página do Curso
    - [x] Sobre
      - [x] Editar infos
    - [x] Participantes
      - [x] Gerenciar Participantes
    - [x] Materiais Disponíveis
      - [x] Gerenciar Material (Adm)

---

- **3° Milestone [2020 Ago [30 a 12]](#)**
  - [ ] Tela da Trilha
    - [x] Edição de Perguntas (Adm)
    - [x] Perguntas e Respostas (User)
    - [x] Edição de fases (Adm)
    - [x] Criar fases (Adm)

---

- **4° Milestone [2020 Set [13 a 26]](#)**
  - [x] Revisão e ajustes
  - [x] Integração com o backend das páginas anteriores

---

- **5° Milestone [2020 Set [27 a 10]](#)**
  - [x] Página de Cadastro 1.1
    - [x] Página de cadastro 1.2
      - [x] Página de cadastro 1.3
  - [ ] Gerenciar planos (Trocar planos)
    - [ ] Gerenciar Planos (Manager)
    - [ ] Editar Planos

---

- **6° Milestone [2020 Out [11 a 24]](#)**
  - [x] Revisão e ajustes

---

- **7° Milestone [2020 Nov [25 a 7]](#)**
  - [x] Fórum
    - [ ] Criar novo tópico
  - [ ] Cards FAV
  - [ ] Terminar as issues com atraso.

---

- **8° Milestone [2020 Nov [8 a 21]](#)**
  - [ ] Revisão e ajustes
  
---

- **9° "Milestone" [2020 Nov [22 a 30]](#)**
  - [ ] Revisão e ajustes
  
---